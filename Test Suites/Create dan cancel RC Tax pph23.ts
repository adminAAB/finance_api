<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Create dan cancel RC Tax pph23</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a00c99a2-a70b-496a-8be2-2825dd31270c</testSuiteGuid>
   <testCaseLink>
      <guid>8784ca3a-e808-4261-823c-b9100ed1c850</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/RC/05 - Create RC Tax (Fullpayment Yes incl PPH PPN)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1392ced-3545-4fe5-bf8b-88d1e0e09b5e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/CA/Cancel voucher RC Tax pph21</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
