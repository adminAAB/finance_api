<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Generate voucher payment instrument</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>865a8b91-ead6-43c0-899a-6e380a3ab3f8</testSuiteGuid>
   <testCaseLink>
      <guid>f26d4b15-8ffc-4663-9f12-6cf603326030</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Payment Instrument/GenerateVoucher Bulk Payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8243bf60-9d0a-480a-9c03-910d08ee7852</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Payment Instrument/GenerateVoucher Single Payment</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
