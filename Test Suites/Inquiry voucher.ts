<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Inquiry Voucher</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1feaffb8-0516-4866-9a7c-136d5eee7f26</testSuiteGuid>
   <testCaseLink>
      <guid>077a93ba-fe8a-46c3-ba6e-02c4531c8f21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/001 - Inquiry Voucher RC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5b396e8-aaa8-4068-a696-5557c9c25b6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/002 - Inquiry Voucher CA atas RC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>636e3137-97f1-4f04-bebf-24b19c021ada</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/003 - Inquiry Voucher RC Tax pph23</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a412bc35-02b8-43c6-8033-83b8d8ccf819</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/004 - Inquiry Voucher CA atas RC Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22d636b6-e889-4f08-95ab-c530c785ec76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/005 - Inquiry Voucher PY</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5759214-765d-4f3e-bb28-ea61c72ca855</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/006 - Inquiry Voucher CA Atas PY</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24e8fd29-daec-40bd-938c-06ba58c2a1d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/007 - Inquiry Voucher PY Tax pph23</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93ed0625-fb4f-4a73-962c-cd1dea288a76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/008 - Inquiry Voucher CA Atas PY Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5243aef3-e376-4d53-8701-fe0c41435079</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/009 - Inquiry Voucher RO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13702b62-b3b7-46a0-956a-a4a3be1779d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/010 - Inquiry Voucher PO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16bef2b8-eb5b-494e-a9d6-706278babd39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/011 - Inquiry Voucher PS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07bc88dc-ef51-4ba8-8f2a-a79d34420af5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/012 - Inquiry Voucher MR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67661b07-d5bb-4aa0-9ffc-62c5f1d4f54a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/013 - Inquiry Voucher MP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17569cd0-d421-4bf1-89f3-fcb78d8d0317</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/014 - Inquiry Voucher W1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d25eb680-7b7d-4618-b9dd-9eb403c27241</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/015 - Inquiry Voucher W2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>413e3b95-fdf4-4b70-8217-87725e0c7da4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/016 - Inquiry Voucher RC Tax Pph21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c9c68e7-a239-4bb0-9893-abc5802b87f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/017 - Inquiry Voucher PY Tax Pph21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>066cef37-e5cb-4238-8025-7442ad7d29fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/018 - Inquiry Voucher PS Tax Pph21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aed71302-1ed3-40db-aaed-01ec49478d89</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/019 - Inquiry Voucher W1 Tax Pph21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0babf3ee-0151-4333-a0d9-4ba2400cc5eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/021 - Inquiry voucher by Policy No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2b6fcd0-98ca-4e60-940c-317fb91d9925</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/020 - Inquiry Voucher W2 Tax Pph21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5735d7ea-dd33-497e-9e2d-82eb3b108d06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/022 - Inquiry voucher by AAB Account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2241e8ce-b02e-415d-9e86-273f9b2a23c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/023 - Inquiry voucher by Payer Name</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49e62cc7-5259-4e82-b3e3-6a022864f511</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/024 - Inquiry voucher by All</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21d2191d-1eed-4c63-8d6d-0ed14fae9322</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/025 - Inquiry voucher by Note No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>585d14ed-b795-479b-8c07-9712d20c9e5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/026 - Inquiry voucher by Claim No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab83e077-9ee3-4c4b-b9b4-26e122b39f2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/027 - Inquiry voucher by Grouping No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>226a437a-0d94-49d5-974f-17b1cff023ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/028 - Inquiry voucher by PI No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>563543c6-9015-4a41-8339-6fd2bb963dd5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/029 - Inquiry voucher by Voucher Type</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7f80159-be8d-49c3-bd6f-d4ba442daa97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/030 - Inquiry voucher by Voucher Status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c6ad736-48e0-426d-af40-1f6ca84d6044</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/031 - Inquiry voucher by Request Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b32f79d-43cd-48d9-85a6-05ab4d2ad0ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/032 - Inquiry voucher by Entry User</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
