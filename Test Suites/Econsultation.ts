<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Econsultation</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ee724480-32a3-4c63-9378-bc94a547a9d9</testSuiteGuid>
   <testCaseLink>
      <guid>8c94ba15-ce62-4471-aad7-654d9c3997b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Econsultation/Create Voucher PS 1x Bayar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3053a6af-2ba8-451a-bea1-c4c490185e9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Econsultation/Create Voucher RO 1x Bayar (CC)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>741a63fd-9095-4a7c-9519-737b706b4f99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Econsultation/Create Voucher RO 1x Bayar (GoPay)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ab88c04-dc36-4a95-aadb-af24163daf13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Econsultation/Create Voucher RO 1x Bayar (ShopeePay)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ac1f4c8-5f6e-4986-93ba-696eceb22f33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Econsultation/Create Voucher RO 1x Bayar (VA)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>081b50ac-e9b5-49a9-b21d-3873a997c4ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Econsultation/Create Voucher RO 3x Bayar (ShopeePay_VA_GoPay)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0af8abdc-0444-4620-8a83-d0fa4e25af41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Econsultation/Drop voucher RO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e646579-f1c7-431c-89c2-48b5f696efdc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Econsultation/Reject voucher RO</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
