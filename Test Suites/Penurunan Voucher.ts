<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Penurunan Voucher</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>50903dcf-17c0-41a1-a976-e7f223756df5</testSuiteGuid>
   <testCaseLink>
      <guid>ee76e67d-c564-4363-93d8-7b0dae96fd77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Voucher/Health/Voucher CA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4151b4e3-5442-4a44-bff1-f9dc7854a8a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Voucher/Health/Voucher PS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>436db88e-da48-4051-b9d1-bb147261834e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Voucher/Health/Voucher PY</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efcd48cf-3558-43e1-b3e2-348dc91a6c19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Voucher/Health/Voucher RC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be7bb7d0-f687-4d4e-b4b6-e3accc743f25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Voucher/Health/Voucher RO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9f59925-008b-4a48-a3c3-004af519c86e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Voucher/Health/Voucher W2</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
