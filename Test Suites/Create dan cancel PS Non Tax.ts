<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Create dan cancel PS Non Tax</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5a5e1074-0ff2-4035-9d0b-ece09a2a3cac</testSuiteGuid>
   <testCaseLink>
      <guid>8a6d6f7c-bf20-4e68-8ca4-327de0938b53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/PS/Create Voucher PS Non Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03129ff9-94d3-40f5-8dd4-5c3dd6a742c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/CA/Cancel voucher PS</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
