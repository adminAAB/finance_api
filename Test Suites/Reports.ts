<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Reports</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4d5221f5-a838-4b1f-bf51-ed7173b08caf</testSuiteGuid>
   <testCaseLink>
      <guid>6f419841-e666-445b-9977-8ffa1fd01053</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Reports/01 - Report Payment Instrument</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7e0ff97-09cf-41ad-a15d-dbf2a07008b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Reports/02 - Report Suspend by Company</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2687ba47-8b3c-4e68-bf51-d63cf57c76fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Reports/03 - Report Suspend by InternalDistribution</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d46e502-d5e9-473a-91e7-60de88acabea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Reports/04 - Report Suspend by AABAccount</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cafb951d-bea6-4c58-8429-185cc8f5defb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Reports/05 - Report Suspend by SuspendCategory</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
