<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Inquiry Nota, Voucher, Search and Add note - Penurunan Data</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c82fe3c6-c8ad-4d58-bfa8-f53930d29710</testSuiteGuid>
   <testCaseLink>
      <guid>03543b26-cdbc-4f00-9c9d-0c34c880dbd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Note/001 - Inquiry Note PRM Outstanding - Misc Retail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e89cc846-6233-47cb-93d2-00c52e615f2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Note/002 - Inquiry Note PRM Outsanding Partial Settled - Misc Retail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5104ceb3-2984-47dc-9416-a7855ee30d24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Note/004 - Inquiry Note PRM Outstanding - A2is Commercial</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>934b7314-f4a3-4674-af35-8e0ffd9b1b0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Note/003 - Inquiry Note PRM - Health</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cade1c2b-7e02-45a5-9c9a-7fcacd327226</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/033 - Inquiry Voucher Misc Retail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ffe75e82-410a-4250-9669-0cb6bbb8d6a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Voucher/034 - Inquiry Voucher Health</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99ec826d-de38-47ea-b152-224812cd0a7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/Search Nota/Search and Add Nota Health</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7377db40-1fea-4653-b3c0-f324447262ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/Search Nota/Search and Add Nota Misc Retail</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
