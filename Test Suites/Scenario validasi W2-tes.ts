<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Scenario validasi W2-tes</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>708010d6-2106-4f77-bf56-63e475e0fa99</testSuiteGuid>
   <testCaseLink>
      <guid>eb0b9e2f-57de-4494-8fa4-ea0f8342fd63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 01 - Cek validasi searchaddnote produksi full OS hold</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>538a86c6-a3da-44b7-8c3c-6ad77f149be4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 02 - Cek validasi searchandnote produksi partial OS unhold</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0f9103f-25b8-4dee-921d-b7f37f028ff2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 03 - Cek validasi searchandnote produksi full OS nempel voucher lain (drop)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36652ae5-deef-4182-9eb2-c0c3aa7c5a18</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 04 - Cek validasi searchandnote produksi full OS tdk nempel voucher lain</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>708a4b11-4778-4cc2-82ac-095a3a971710</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 05 - Cek validasi searchandnote suspend</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b80344cc-6ed6-4570-b60c-75bddf234f23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 06 - Cek validasi searchandnote komisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b048a8c8-8ad4-436c-92a7-3003e0e5f26b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 07 - Cek validasi searchandnote shariah</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c83bec3c-41aa-43f3-8330-4b7f1452504f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 08 - Cek validasi searchandnote produksi no polis kosong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee533cc9-8b35-48c8-ba5f-7dbce6a684d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 09 - Cek validasi saat validate beda PolicyNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2169ec81-fefb-473d-bd61-c7e8fd25356e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 10 - Cek validasi saat validate beda TPJournal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>698e6906-8924-4c5d-bdd2-344822857fff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/W2/W2 11 - Cek validasi saat validate PolicyNo TPJournal sama</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
