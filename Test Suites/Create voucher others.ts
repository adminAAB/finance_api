<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Create voucher others</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e77d2e84-0a7c-425d-a590-d430bf00f77d</testSuiteGuid>
   <testCaseLink>
      <guid>323e6028-a172-4ce4-9a44-bb49f6ef03d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/PO/Create voucher PO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8fc727ff-9cce-4845-984d-4f24860a2fa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/RO/Create voucher RO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>455752ca-201e-49ff-841c-63e89c3ecd8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/MP/Create MP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e54ac50-a68b-44b9-8c09-8196a4492d66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/MR/Create MR</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
