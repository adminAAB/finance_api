<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Export Voucher</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>80b523ee-c7dc-427e-b4c2-167d01b57fa6</testSuiteGuid>
   <testCaseLink>
      <guid>b2866a12-8c55-4664-88b5-7052ab9a35a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/01 - Export Voucher PS Non Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86e9efa5-7962-4ce6-a9b3-ea3fc2c84c0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/02 - Export Voucher PY Tax pph23</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9eca630f-0508-45c1-8035-2a4aa0c249a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/03 - Export Voucher RC Tax pph23</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e44bcf76-a9ac-4023-b2d7-3fb3c3467a95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/04 - Export Voucher W1 Non Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb0deddf-b9df-415f-92a4-c813d4be22b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/05 - Export Voucher W2 Non Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53b74ff4-24e7-4a7a-a369-ee0bc607729b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/06 - Export Voucher CA atas RC Tax pph23</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0d41f48-a270-49c0-9271-dcdac2febc27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/07 - Export Voucher CA atas PY Tax pph23</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f211670-a8d7-45f8-b885-c6aa8a83013b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/08 - Export Voucher CA atas RC Non Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad137515-f7fe-4908-a506-ebca6c7723cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/09 - Export Voucher CA atas PY Non Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d50f900c-f5e2-4459-801e-a68f8cd00989</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/10 - Export Voucher PY Tax pph21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>277dcfc3-8a73-4d1d-85bc-00d2365e7935</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/11 - Export Voucher RC Tax pph21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11c2a2e0-5ed2-4182-9631-93c3fb1ae599</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/12 - Export Voucher PS Tax pph21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45bf7b3e-648e-46a7-8fa7-300b50632d25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/13 - Export Voucher W1 Tax pph21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f243b0d-dbe8-4536-95a7-274032df526a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/14 - Export Voucher W2 Tax pph21</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3009c781-fd8a-4534-b19c-482d2455ec0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/15 - Export Voucher RC Non Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>258e1842-8230-4adf-9a66-feb17fb56335</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/16 - Export Voucher PY Non Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a9d4b31-1c5c-4413-b31d-dd218122d153</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/17 - Export Voucher MR Non Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0ddeef7-9d71-43da-afed-864ee8ffd42e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/18 - Export Voucher MP Non Tax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2447517-33f9-4eef-b27c-8b326ed2b00e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/19 - Export Voucher RO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>010cb22d-05c0-4f2c-bdb5-f258f8724802</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Export Voucher/20 - Export Voucher PO</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
