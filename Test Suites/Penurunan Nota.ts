<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Penurunan Nota</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>80093f96-e4d9-4583-92c8-aa7f736ffd12</testSuiteGuid>
   <testCaseLink>
      <guid>b62ada10-91dd-4561-af48-0a19d35f106d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Health/Nota CLM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d7e00ce-d259-4183-ba6a-e6268c315a31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Health/Nota COM - No IDCard NPWP Company</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29c1bf52-787c-43b1-b04c-7f7531203818</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Health/Nota COM - No IDCard NPWP Personal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f30340a-9569-4d4b-9723-78d77b63e897</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Health/Nota COM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4800000f-5587-4840-bbb7-53ca8844a9bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Health/Nota EXS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>479dacfe-9284-404c-9389-686426a4a925</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Health/Nota OTH</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53315973-a2fb-43b6-bdb8-23eb2afd5ea8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Health/Nota PRM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18352a04-26f4-4528-a14c-313593d925a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Health/Nota SCA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18c31ec9-8387-4bf5-b75d-f2a93c8c1700</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Health/Nota SOT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b303746a-4dd1-4041-81c3-3097bfe8cb12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Health/Nota SPR</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
