<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Inquiry Suspend</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5eb3a8c7-9cdc-4dc1-9504-e7e05c87e116</testSuiteGuid>
   <testCaseLink>
      <guid>a4f169c1-e2cb-4bc5-8ff1-6799c531d47c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Suspend/001 - Inquiry Suspend by Note Number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49eae84b-e4b9-42ba-a8e9-1d937ced1fec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Suspend/002 - Inquiry Suspend by Voucher Number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f1f2333-7013-4362-9459-b7d377457e16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Suspend/007 - Suspend History Nota Oustanding - Health</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8c350f2-e6bb-4811-86e7-b5cef7ac80d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Suspend/008 - Suspend History Nota Settled - Health</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>454666b6-64a2-440c-8165-778e627e77dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Inquiry Suspend/009 - Update Details nota suspend - Health</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
