<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Create dan cancel PS Overshortage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ae76f801-9ec1-4fde-83f6-36ea88e2b6a4</testSuiteGuid>
   <testCaseLink>
      <guid>09a3f1c7-dd29-4170-83c2-fbfa8c7fe8b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/PS/Create Voucher PS Overshortage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9095c24-3b3c-4835-931d-79024147c9cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Finance/API/Scenario/Create Voucher/CA/Cancel voucher PS overshortage</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
