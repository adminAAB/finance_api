package com.keyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date

import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable

public class REA extends UI {

	@Keyword
	public static void ComboBoxReact (TestObject Combo, String Value) {
		TestObject tObj = Combo
		String findXpath = "${tObj.findPropertyValue('xpath')}"
		String children = '(' +findXpath + '//parent::*//*[text()=\''+ Value +'\'])[1]'

		if (Combo != null || Combo != "") {
			Click(Combo)
		} else {
			KeywordUtil.markFailedAndStop('Button Open Combo Box is not found, Please check your Xpath')
		}

		if (newTestObject(children) != null || newTestObject(children) != "") {
			Click(newTestObject(children))
		} else {
			KeywordUtil.markFailedAndStop('Button ' + Value + ' is not found')
		}
	}

	@Keyword
	public static ArrayList getArrayIcon (TestObject Object) {
		WebDriver driver = DriverFactory.getWebDriver()
		TestObject tObj = Object
		String Xpath = "${tObj.findPropertyValue('xpath')}"

		WebElement drivers = driver.findElement(By.xpath(Xpath))
		List<WebElement> elementList=  drivers.findElements(By.className("style-card-view"))
		ArrayList getValue = new ArrayList()
		ArrayList finalValue = new ArrayList()

		int i
		for (i = 1 ; i <= elementList.size() ; i++) {
			getValue.add(WebUI.getAttribute(newTestObject("("+Xpath + "/div[" + i + "]/div)[1]"), "id"))
		}

		int a
		for (a = 0 ; a < getValue.size() ; a++) {
			String result = getValue[a]
			String[] separate = result.split('Type')d

			finalValue.add(separate[1])
		}
		return finalValue
	}

	@Keyword
	public static ArrayList getArrayButton (TestObject Object, String type ,String Class) {
		WebDriver driver = DriverFactory.getWebDriver()
		TestObject tObj = Object
		String Xpath = "${tObj.findPropertyValue('xpath')}"

		ArrayList getValue = new ArrayList()
		WebElement drivers = driver.findElement(By.xpath(Xpath))
		ArrayList ListElement = new ArrayList()

		if (type.toLowerCase() == "class") {
			List<WebElement> elementList=  drivers.findElements(By.className(Class))
			ListElement = elementList

			int i
			for (i = 1 ; i <= ListElement.size() ; i++) {
				getValue.add(WebUI.getText(newTestObject("("+Xpath + "/*[" + i + "]//*[text()])")))
			}
		} else if (type.toLowerCase() == "id") {
			List<WebElement> elementList=  drivers.findElements(By.id(Class))
			ListElement = elementList

			int i
			for (i = 1 ; i <= ListElement.size() ; i++) {
				getValue.add(WebUI.getText(newTestObject("("+Xpath + "/*[" + i + "]//*[text()])")))
			}
		} else if (type.toLowerCase() == "xpath"){
			List<WebElement> elementList=  drivers.findElements(By.xpath(Class))
			ListElement = elementList

			int i
			for (i = 2 ; i <= ListElement.size() + 1 ; i++) {
				getValue.add(WebUI.getText(newTestObject("("+Xpath + "/*[" + i + "]//*[text()])")))
			}
		} else if (type.toLowerCase() == "tagname") {
			List<WebElement> elementList = drivers.findElements(By.tagName(Class))
			ListElement = elementList

			int i
			for (i = 1 ; i <= ListElement.size() ; i++) {
				String value = WebUI.getAttribute(UI.newTestObject("("+Xpath + "/*)[" + i + "]"), "aria-label")
				String textValue = WebUI.getText(UI.newTestObject("("+Xpath + "/*)[" + i + "]"))

				if ((value != null || value != '' || value != 'null') && ((textValue == null || textValue == '' || textValue == 'null'))) {
					getValue.add(value)
				} else {
					getValue.add(textValue)
				}
			}
		}
		return getValue
	}

	@Keyword
	public static ArrayList getColumnDataTableUI (TestObject Table, int column, boolean displayEmpty) {
		WebDriver Driver = DriverFactory.getWebDriver()
		TestObject tObj = Table
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		WebElement table = Driver.findElement(By.xpath(XpathTable))
		List<WebElement> body = table.findElements(By.tagName("tr"))
		ArrayList result = new ArrayList()

		int index = column -1

		int i
		for (i = 0 ; i < body.size() ; i++) {
			List<WebElement> Colls = body[i].findElements(By.tagName("td"))



			if (Colls.size() == 0){
				continue
			} else {
				if (displayEmpty) {
					if (index < 1) {
						if (Colls.size() > 1) {
							result.add(Colls[index].getText())
						} else {
							result.add(Colls[0].getText())
						}
					} else {
						if (Colls.size() > 1) {
							result.add(Colls[index].getText())
						} else {
							continue
						}
					}
				} else {
					if (index < 1) {
						if (Colls.size() > 1) {
							if (Colls[index].getText().size() > 2) {
								result.add(Colls[index].getText())
							}
						} else {
							result.add(Colls[0].getText())
						}
					} else {
						if (Colls.size() > 1) {
							if (Colls[index].getText().size() > 2) {
								result.add(Colls[index].getText())
							}
						} else {
							continue
						}
					}
				}
			}
		}
		return result
	}

	@Keyword
	public static void datePicker (String date, TestObject FieldDate) {
		WebElement element = WebUiCommonHelper.findWebElement(FieldDate,10)
		WebUI.executeJavaScript("arguments[0].value=\'" + date + "\'", Arrays.asList(element))
	}

	@Keyword
	public static void datePicker2 (String date) {
		String[] separate = date.split(' ')

		String day = separate[0]
		String month = separate[1]
		String year = separate[2]

		String XpathDay = '//*[contains(@class,"week")]/*[not(contains(@class,"outside")) and text()="' + day + '"]'
		String XpathMonth = '//*[contains(@class,"month")]/option[text()="' + month + '"]'
		String XpathYear = '//*[contains(@class,"year")]/option[text()="' + year + '"]'

		Click(newTestObject(XpathMonth))
		Click(newTestObject(XpathYear))
		Click(newTestObject(XpathDay))
	}

	@Keyword
	public static String convertDate (String Date, boolean time) {
		String[] separate = Date.split(" ")
		String tanggal

		if (separate[1].contains("Jan")) {
			String date1 = Date.replace(separate[1],"01")
			tanggal = date1
		} else if (separate[1].contains("Feb")) {
			String date1 = Date.replace(separate[1],"02")
			tanggal = date1
		} else if (separate[1].contains("Mar")) {
			String date1 = Date.replace(separate[1],"03")
			tanggal = date1
		} else if (separate[1].contains("Apr")) {
			String date1 = Date.replace(separate[1],"04")
			tanggal = date1
		} else if (separate[1].contains("Mei") || separate[1].contains("May")) {
			String date1 = Date.replace(separate[1],"05")
			tanggal = date1
		} else if (separate[1].contains("Jun")) {
			String date1 = Date.replace(separate[1],"06")
			tanggal = date1
		} else if (separate[1].contains("Jul")) {
			String date1 = Date.replace(separate[1],"07")
			tanggal = date1
		} else if (separate[1].contains("Agu") || separate[1].contains("Aug")) {
			String date1 = Date.replace(separate[1],"08")
			tanggal = date1
		} else if (separate[1].contains("Sep")) {
			String date1 = Date.replace(separate[1],"09")
			tanggal = date1
		} else if (separate[1].contains("Okt") || separate[1].contains("Oct")) {
			String date1 = Date.replace(separate[1],"10")
			tanggal = date1
		} else if (separate[1].contains("Nov")) {
			String date1 = Date.replace(separate[1],"11")
			tanggal = date1
		} else if (separate[1].contains("Des") || separate[1].contains("Dec")) {
			String date1 = Date.replace(separate[1],"12")
			tanggal = date1
		}

		String getDate = tanggal.replace(" ","/")
		Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(getDate);
		String fixDate

		if (time == true) {
			String fixDate1 = date1.format('yyyy-MM-dd HH:mm:ss')
			fixDate = fixDate1
		} else {
			String fixDate1 = date1.format('yyyy-MM-dd')
			fixDate = fixDate1
		}
		return fixDate
	}

	@Keyword
	public static ArrayList getAllText (TestObject Cover, String tagName) {
		WebDriver Driver = DriverFactory.getWebDriver()
		TestObject tObj = Cover
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		WebElement table = Driver.findElement(By.xpath(XpathTable))
		List<WebElement> body = table.findElements(By.xpath(tagName))
		ArrayList result = new ArrayList()

		int i
		for (i = 0 ; i < body.size() ; i++) {
			List<WebElement> Colls = body[i].findElements(By.tagName("label"))

			if (Colls.size() == 0){
				continue
			} else {
				result.add(Colls[0].getText().trim())
			}
		}
		return result
	}

	@Keyword
	public static ArrayList getAllColumnValue (TestObject tableXpath, String gridColumn) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		String XpathTableRowBody = XpathTable + '/tbody'
		String XpathTableHead = XpathTable + '/thead//tr'
		String XpathTableBody = XpathTable + '/tbody//tr'

		WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
		WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
		WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))

		List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
		List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
		List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))

		List<String> collsName = new ArrayList()
		List<String> column = new ArrayList()
		int index = 0

		int i
		for (i = 0 ; i < rows.size() ; i++){
			List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				collsName.add('')
				//				index += 1
				//				continue
			} else {
				collsName.add(Colls[0].getText())
			}

			int a
			if (collsName[i] == gridColumn) {
				for (a = 0 ; a < rowBody.size() ; a++) {
					List<WebElement> Kolom = rowBody[a].findElements(By.tagName("td"))
					if (Kolom.size() == 0){
						continue
					} else {
						column.add(Kolom[i].getText())
					}
				}
			}
		}
		return column
	}

	@Keyword
	public static ArrayList getAllRowsValue (TestObject tableXpath, String columnHeader, String RowsValue) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		String XpathTableRowBody = XpathTable + '//tbody'
		String XpathTableHead = XpathTable + '//thead//tr'
		String XpathTableBody = XpathTable + '//tbody//tr'

		WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
		WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
		WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))
		WebElement BodyTable = Driver.findElement(By.xpath(XpathTable))


		List<WebElement> body = BodyTable.findElements(By.tagName("tbody"))
		List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
		List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
		List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))
		List<WebElement> spanBody = tableRowBody.findElements(By.tagName("span"))

		List<String> collsName = new ArrayList()
		List<String> column = new ArrayList()
		List<String> line = new ArrayList()
		//		int index = 0

		int i
		for (i = 0 ; i < rows.size() ; i++){
			List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				collsName.add('')
				//				index += 1
				continue
			} else {
				collsName.add(Colls[0].getText())
				println (collsName[i])
			}

			int a
			println (columnHeader)
			if (collsName[i] == columnHeader) {
				for (a = 0 ; a < rowBody.size() ; a++) {
					List<WebElement> Kolom = rowBody[a].findElements(By.tagName("td"))
					if (Kolom.size() == 0){
						continue
					} else {
						column.add(Kolom[i].getText())
					}

					int x
					if (column[a] == RowsValue) {
						for (x = 0 ; x < baris.size() ; x++) {
							List<WebElement> Lines = rowBody[a].findElements(By.tagName("td"))
							if (Lines.size() == 0) {
								continue
							} else {
								line.add(Lines[x].getText())
							}
						}
					}
				}
			}
		}
		return line
	}

	@Keyword
	public static void CompareRowsValue (TestObject tableXpath, String columnHeader, String RowsValue, ArrayList RowsCompare) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		String XpathTableRowBody = XpathTable + '/tbody'
		String XpathTableHead = XpathTable + '/thead//tr'
		String XpathTableBody = XpathTable + '/tbody//tr'

		WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
		WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
		WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))
		WebElement BodyTable = Driver.findElement(By.xpath(XpathTable))

		List<WebElement> body = BodyTable.findElements(By.tagName("tbody"))
		List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
		List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
		List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))
		List<WebElement> spanBody = tableRowBody.findElements(By.tagName("span"))

		List<String> collsName = new ArrayList()
		List<String> column = new ArrayList()
		List<String> line = new ArrayList()
		//		int index = 0

		int i
		for (i = 0 ; i < rows.size() ; i++){
			List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				collsName.add('')
				//				index += 1
				continue
			} else {
				collsName.add(Colls[0].getText())
			}

			int a
			if (collsName[i] == columnHeader) {
				for (a = 0 ; a < rowBody.size() ; a++) {
					List<WebElement> Kolom = rowBody[a].findElements(By.tagName("td"))
					if (Kolom.size() == 0){
						continue
					} else {
						column.add(Kolom[i].getText())
					}

					int x
					if (column[a] == RowsValue) {
						for (x = 0 ; x < baris.size() ; x++) {
							List<WebElement> Lines = rowBody[a].findElements(By.tagName("td"))
							if (Lines.size() == 0) {
								continue
							} else {
								line.add(Lines[x].getText())
							}
						}
					}
				}
			}
		}

		int r
		for (r = 0 ; r < line.size() ; r++) {
			if (line[r].trim() == RowsCompare[r]) {
				KeywordUtil.markPassed("Value of Column " + collsName[r] +" has same Value with array")
			} else {
				KeywordUtil.markWarning("Value of Column " + collsName[r] +" has different Value, Column Value = "+ line[r] + " Compare to Array = "+ RowsCompare[r])
			}
		}
	}

	@Keyword
	public static void CompareColumnsValue (TestObject tableXpath, String gridColumn, ArrayList CompareColumn) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		String XpathTableRowBody = XpathTable + '/tbody'
		String XpathTableHead = XpathTable + '/thead//tr'
		String XpathTableBody = XpathTable + '/tbody//tr'

		WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
		WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
		WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))

		List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
		List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
		List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))

		List<String> collsName = new ArrayList()
		List<String> column = new ArrayList()
		int index = 0

		int i
		for (i = 0 ; i < rows.size() ; i++){
			List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				collsName.add (' ')
				//				index += 1
				//				continue
			} else {
				collsName.add(Colls[0].getText())
			}

			int a
			if (collsName[i] == gridColumn) {
				for (a = 0 ; a < rowBody.size() ; a++) {
					List<WebElement> Kolom = rowBody[a].findElements(By.tagName("td"))
					if (Kolom.size() == 0){
						continue
					} else {
						column.add(Kolom[i-index].getText())
					}
				}
			}
		}

		int r
		for (r = 0 ; r < column.size() ; r++) {
			int sum = r + 1

			if (column[r].trim() == CompareColumn[r]) {
				KeywordUtil.markPassed("Value of Row " + sum +" has same Value with array")
			} else {
				KeywordUtil.markWarning("Value of Row " + sum +" has different Value, Column Value = "+ column[r] + " Compare to Array = "+ CompareColumn[r])
			}
		}
	}

	@Keyword
	public static void ClickExpectedRow (TestObject tableXpath, String gridColumn, String columnValue) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		String XpathTableRowBody = XpathTable + '/tbody'
		String XpathTableHead = XpathTable + '/thead//tr'
		String XpathTableBody = XpathTable + '/tbody//tr'

		WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
		WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
		WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))

		List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
		List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
		List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))

		List<String> collsName = new ArrayList()
		List<String> column = new ArrayList()
		//		int index = 0

		int i
		for (i = 0 ; i < rows.size() ; i++){
			List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				collsName.add('')
				//				index += 1
				continue
			} else {
				collsName.add(Colls[0].getText())
			}

			int a
			if (collsName[i] == gridColumn) {
				for (a = 0 ; a < rowBody.size() ; a++) {
					List<WebElement> Kolom = rowBody[a].findElements(By.tagName("td"))
					if (Kolom.size() == 0){
						continue
					} else {
						column.add(Kolom[i].getText())

						if (column[a] == columnValue) {
							String xpathButton = XpathTableBody + '//*[normalize-space()=\'' + column[a] + '\']'
							Click(newTestObject(xpathButton))
						}
					}
				}
			}
		}
	}

	@Keyword
	public static void ClickExpectedRowWithNext (TestObject tableXpath, String gridColumn, String columnValue, TestObject ButtonNext) {
		boolean found  = false

		while (!found) {
			WebDriver Driver = DriverFactory.getWebDriver()

			TestObject tObj = tableXpath
			String XpathTable = "${tObj.findPropertyValue('xpath')}"

			String XpathTableRowBody = XpathTable + '/tbody'
			String XpathTableHead = XpathTable + '/thead//tr'
			String XpathTableBody = XpathTable + '/tbody//tr'

			WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
			WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
			WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))

			List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
			List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
			List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))
			List<String> collsName = new ArrayList()
			List<String> column = new ArrayList()
			//			int index = 0

			int i = 0
			for (i = 0 ; i < rows.size() ; i++){
				ArrayList Colls = new ArrayList()
				Colls = rows[i].findElements(By.tagName("span"))
				if (Colls.size() == 0){
					collsName.add('')
					//					index += 1
					continue
				} else {
					collsName.add(Colls[0].getText())
				}

				int a = 0
				if (collsName[i] == gridColumn) {
					for (a = 0 ; a < rowBody.size() ; a++) {
						ArrayList Kolom = new ArrayList()
						Kolom = rowBody[a].findElements(By.tagName("td"))
						if (Kolom.size() == 0){
							continue
						} else {
							column.add(Kolom[i].getText())

							if (column[a].trim() == columnValue) {
								String xpathButton = XpathTableBody + '//td[normalize-space()=\'' + column[a] + '\']'
								Click(newTestObject(xpathButton))
								found  = true

								break
							}
						}
					}
				}
				if (found == true) {
					break
				}
			}
			if (!found) {
				WebUI.click(ButtonNext)
				WebUI.delay(2)
			}
		}
	}

	@Keyword
	public static void CompareColumnToDatabase (TestObject tableXpath, String gridColumn, String url, String dbname, String queryTable, String getColumn) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		String XpathTableRowBody = XpathTable + '/tbody'
		String XpathTableHead = XpathTable + '/thead//tr'
		String XpathTableBody = XpathTable + '/tbody//tr'

		WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
		WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
		WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))

		List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
		List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
		List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))

		List<String> collsName = new ArrayList()
		List<String> column = new ArrayList()
		int index = 0

		int i
		for (i = 0 ; i < rows.size() ; i++){
			List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				collsName.add (' ')
				//				index += 1
				//				continue
			} else {
				collsName.add(Colls[0].getText())
			}

			int a
			if (collsName[i] == gridColumn) {
				for (a = 0 ; a < rowBody.size() ; a++) {
					List<WebElement> Kolom = rowBody[a].findElements(By.tagName("td"))
					if (Kolom.size() == 0){
						continue
					} else {
						column.add(Kolom[i - index].getText())
					}
				}
			}
		}

		int r
		def database = getOneColumnDatabase(url, dbname, queryTable, getColumn)
		for (r = 0 ; r < column.size() ; r++) {
			int sum = r + 1

			if (column[r].trim() == database[r]) {
				KeywordUtil.markPassed("Value of Row " + sum +" has same Value with Database")
			} else {
				KeywordUtil.markWarning("Value of Row " + sum +" has different Value, Column Value = "+ column[r] + " Compare to Database Value = "+ database[r])
			}
		}
	}

	@Keyword
	public static void CompareRowToDatabase (TestObject tableXpath, String columnHeader, String RowsValue, String url, String dbname, String queryTable) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		String XpathTableRowBody = XpathTable + '/tbody'
		String XpathTableHead = XpathTable + '/thead//tr'
		String XpathTableBody = XpathTable + '/tbody//tr'

		WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
		WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
		WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))
		WebElement BodyTable = Driver.findElement(By.xpath(XpathTable))


		List<WebElement> body = BodyTable.findElements(By.tagName("tbody"))
		List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
		List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
		List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))
		List<WebElement> spanBody = tableRowBody.findElements(By.tagName("span"))

		List<String> collsName = new ArrayList()
		List<String> column = new ArrayList()
		List<String> line = new ArrayList()
		//		int index = 0

		int i
		for (i = 0 ; i < rows.size() ; i++){
			List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				collsName.add(' ')
				//				index += 1
				continue
			} else {
				collsName.add(Colls[0].getText())
			}

			int a
			if (collsName[i] == columnHeader) {
				for (a = 0 ; a < rowBody.size() ; a++) {
					List<WebElement> Kolom = rowBody[a].findElements(By.tagName("td"))
					if (Kolom.size() == 0){
						continue
					} else {
						column.add(Kolom[i].getText())
					}

					int x
					if (column[a] == RowsValue) {
						for (x = 0 ; x < baris.size() ; x++) {
							List<WebElement> Lines = rowBody[a].findElements(By.tagName("td"))
							if (Lines.size() == 0) {
								continue
							} else {
								line.add(Lines[x].getText())
							}
						}
					}
				}
			}
		}

		int r
		def database =  getOneRowDatabase(url, dbname, queryTable)
		for (r = 0 ; r < line.size() ; r++) {
			int sum = r + 1

			if (line[r].trim() == database[r]) {
				KeywordUtil.markPassed("Value of Row " + sum +" has same Value with Database")
			} else {
				KeywordUtil.markWarning("Value of Row " + sum +" has different Value, Column Value = "+ line[r] + " Compare to Database Value = "+ database[r])
			}
		}
	}

	@Keyword
	public static ArrayList getAllDataTable (TestObject tableXpath) {
		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"

		String XpathTableRowBody = XpathTable + '/tbody'
		String XpathTableHead = XpathTable + '/thead//tr'
		String XpathTableBody = XpathTable + '/tbody//tr'

		WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
		WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
		WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))

		List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
		List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
		List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))

		List<String> collsName = new ArrayList()
		List<String> column = new ArrayList()
		//		int index = 0

		int i
		for (i = 0 ; i < rows.size() ; i++){
			List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				collsName.add('')
				//				index += 1
				continue
			} else {
				collsName.add(Colls[0].getText())
			}

			int a

			if (collsName[i] == collsName[1]) {

				for (a = 0 ; a < rowBody.size() ; a++) {
					List<String> line = new ArrayList()
					List<WebElement> Kolom = rowBody[a].findElements(By.tagName("td"))
					if (Kolom.size() == 0){
						continue
					}

					int x
					for (x = 0 ; x < baris.size() ; x++) {
						List<WebElement> Lines = rowBody[a].findElements(By.tagName("td"))
						if (Lines.size() == 0) {
							continue
						} else {
							line.add(Lines[x].getText())
						}
					}
					column.add(line)
				}
			}
		}
		return column
	}

	@Keyword
	public static ArrayList getAllDataTableMultiPage (TestObject tableXpath, TestObject ButtonNext) {
		List<String> column = new ArrayList()
		String NextButton = "${ButtonNext.findPropertyValue('xpath')}"

		String buttonDisable = NextButton + "/*/parent::button[@disabled]"
		boolean button = false

		int v
		for (v = 0 ; !button ; v++) {
			button = WebUI.waitForElementPresent(newTestObject(buttonDisable), 1, FailureHandling.OPTIONAL)
			WebDriver Driver = DriverFactory.getWebDriver()

			TestObject tObj = tableXpath
			String XpathTable = "${tObj.findPropertyValue('xpath')}"

			String XpathTableRowBody = XpathTable + '/tbody'
			String XpathTableHead = XpathTable + '/thead//tr'
			String XpathTableBody = XpathTable + '/tbody//tr'

			WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
			WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
			WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))

			List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
			List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
			List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))

			//		int index = 0

			List<String> collsName = new ArrayList()
			int i
			for (i = 0 ; i < rows.size() ; i++){
				List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
				if (Colls.size() == 0){
					collsName.add('')
					//				index += 1
					continue
				} else {
					collsName.add(Colls[0].getText())
				}

				int a

				if (collsName[i] == collsName[1]) {

					for (a = 0 ; a < rowBody.size() ; a++) {
						List<String> line = new ArrayList()
						List<WebElement> Kolom = rowBody[a].findElements(By.tagName("td"))
						if (Kolom.size() == 0){
							continue
						}

						int x
						for (x = 0 ; x < baris.size() ; x++) {
							List<WebElement> Lines = rowBody[a].findElements(By.tagName("td"))
							if (Lines.size() == 0) {
								continue
							} else {
								line.add(Lines[x].getText())
							}
						}
						column.add(line)
					}
				}
			}
			WebUI.click(ButtonNext)
		}
		return column
	}

	@Keyword
	public static void compareAllTabletoDatabase (TestObject tableXpath, String IP, String dbname, String queryTable) {
		ArrayList table = getAllDataTable(tableXpath)

		ArrayList database = getAllDataDatabase(IP, dbname, queryTable)

		int i
		for (i = 0 ; i < table.size() ; i++) {

			int o
			for (o = 0 ; o < table[0].size() ; o++) {
				if ((table[i])[o] == (database[i])[o]) {
					KeywordUtil.markPassed("Value " + (table[i])[o] +" from Grid Table same with Database.")
				} else {
					KeywordUtil.markFailedAndStop("Value from Grid Table = " + (table[i])[o] + " has different Value from database = " + (database[i])[o])
				}
			}
		}

	}

	@Keyword
	public static void compareAllDatabasetoArray (String IP, String dbname, String queryTable, ArrayList ArrayName) {
		ArrayList database = getAllDataDatabase(IP, dbname, queryTable)

		int i
		for (i = 0 ; i < ArrayName.size() ; i++) {

			int o
			for (o = 0 ; o < ArrayName[0].size() ; o++) {
				if ((ArrayName[i])[o] == (database[i])[o]) {
					KeywordUtil.markPassed("Value " + (ArrayName[i])[o] +" from Grid Table same with Database.")
				} else {
					KeywordUtil.markFailedAndStop("Value from Grid Table = " + (ArrayName[i])[o] + " has different Value from database = " + (database[i])[o])
				}
			}
		}

	}
}
