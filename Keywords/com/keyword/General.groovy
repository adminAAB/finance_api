package com.keyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class General {

	@Keyword
	def static convertdate (String Date){

		ArrayList arrayDate = Date.split("-")

		String Month = ''

		String DateFormat = ''

		if (arrayDate[1].toString() == '01') {

			Month = "Jan"
		} else if (arrayDate[1].toString() == '02') {

			Month = "Feb"
		} else if (arrayDate[1].toString() == '03') {

			Month = "Mar"
		} else if (arrayDate[1].toString() == '04') {

			Month = "Apr"
		} else if (arrayDate[1].toString() == '05') {

			Month = "May"
		} else if (arrayDate[1].toString() == '06') {

			Month = "Jun"
		} else if (arrayDate[1].toString() == '07') {

			Month = "Jul"
		} else if (arrayDate[1].toString() == '08') {

			Month = "Aug"
		} else if (arrayDate[1].toString() == '09') {

			Month = "Sep"
		} else if (arrayDate[1].toString() == '10') {

			Month = "Oct"
		} else if (arrayDate[1].toString() == '11') {

			Month = "Nov"
		} else if (arrayDate[1].toString() == '12') {

			Month = "Dec"
		}

		DateFormat = arrayDate[0] + "/" + Month + "/" + arrayDate[2]

		return DateFormat
	}
}
