package com.keyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.sql.DriverManager
import com.keyword.UI
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.sql.Driver
import java.sql.Connection
import java.sql.CallableStatement
import internal.GlobalVariable
import java.sql.SQLException

public class JOB extends UI {
	
	static void runJob(String serverName, String jobName) {		
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String sqlSerDriver = "jdbc:sqlserver://%s;integratedSecurity=true;";
			String Url = String.format(sqlSerDriver, serverName);
			Connection conn = DriverManager.getConnection(Url);
			String jobExString = "EXEC msdb.dbo.sp_start_job N'%s'";
			String jobExcute = String.format(jobExString, jobName);
			CallableStatement cs = conn.prepareCall(jobExcute);
			cs.execute();
			KeywordUtil.markPassed("Job started..")
			
		} catch (ClassNotFoundException){
		
			KeywordUtil.markErrorAndStop("Job not found.")
		}
	}
	
	
//	public static void main(String[] args) {		
//		try {
//			String serverName = " "; /*pass your server name here*/
//			String jobName = " "; /*pass your job name here*/
//			runJob(serverName, jobName);
//		
//		} catch (ClassNotFoundException){
//		
//			KeywordUtil.markErrorAndStop("Job not found.")
//		
//		}
//	
//	}
//		

	
}
