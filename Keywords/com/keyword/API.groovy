package com.keyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.testobject.RestRequestObjectBuilder
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import groovy.json.JsonSlurper
import groovy.json.internal.LazyMap

class API extends UI {
	/**
	 * Send request and verify status code
	 * @param request request object, must be an instance of RequestObject
	 * @param expectedStatusCode
	 * @return a boolean to indicate whether the response status code equals the expected one
	 */
	@Keyword
	def verifyStatusCode(TestObject request, int expectedStatusCode) {
		if (request instanceof RequestObject) {
			RequestObject requestObject = (RequestObject) request
			ResponseObject response = WSBuiltInKeywords.sendRequest(requestObject)
			if (response.getStatusCode() == expectedStatusCode) {
				KeywordUtil.markPassed("Response status codes match")
			} else {
				KeywordUtil.markFailed("Response status code not match. Expected: " +
						expectedStatusCode + " - Actual: " + response.getStatusCode() )
			}
		} else {
			KeywordUtil.markFailed(request.getObjectId() + " is not a RequestObject")
		}
	}

	/**
	 * Add Header basic authorization field,
	 * this field value is Base64 encoded token from user name and password
	 * @param request object, must be an instance of RequestObject
	 * @param username username
	 * @param password password
	 * @return the original request object with basic authorization header field added
	 */
	@Keyword
	def addBasicAuthorizationProperty(TestObject request, String username, String password) {
		if (request instanceof RequestObject) {
			String authorizationValue = username + ":" + password
			authorizationValue = "Basic " + authorizationValue.bytes.encodeBase64().toString()

			// Find available basic authorization field and change its value to the new one, if any
			List<TestObjectProperty> headerProperties = request.getHttpHeaderProperties()
			boolean fieldExist = false
			for (int i = 0; i < headerProperties.size(); i++) {
				TestObjectProperty headerField = headerProperties.get(i)
				if (headerField.getName().equals('Authorization')) {
					KeywordUtil.logInfo("Found existent basic authorization field. Replacing its value.")
					headerField.setValue(authorizationValue)
					fieldExist = true
					break
				}
			}

			if (!fieldExist) {
				TestObjectProperty authorizationProperty = new TestObjectProperty("Authorization",
						ConditionType.EQUALS, authorizationValue, true)
				headerProperties.add(authorizationProperty)
			}
			KeywordUtil.markPassed("Basic authorization field has been added to request header")
		} else {
			KeywordUtil.markFailed(request.getObjectId() + "is not a RequestObject")
		}
		return request
	}
	/*
	 @Keyword
	 public static Object GetFullResponse (String EndPoint, String bodyParam, String contentType, String Authorization, String Cookie) {
	 String APIURL = EndPoint
	 def builder = new RestRequestObjectBuilder()
	 ArrayList listHeaders
	 TestObjectProperty header
	 TestObjectProperty header2
	 def body = (bodyParam)
	 if (contentType != null || contentType != "") {
	 TestObjectProperty head=new TestObjectProperty("Content-Type", ConditionType.EQUALS, contentType)
	 header = head
	 }
	 if (Authorization != null || Authorization != "") {
	 TestObjectProperty head2=new TestObjectProperty("Authorization", ConditionType.EQUALS, Authorization)
	 header2 = head2
	 }
	 if ((contentType != null || contentType != "") && (Authorization != null || Authorization != "")) {
	 ArrayList list=Arrays.asList(header, header2)
	 listHeaders = list
	 } else if ((contentType != null || contentType != "") && (Authorization == null || Authorization == "")) {
	 ArrayList list=Arrays.asList(header)
	 listHeaders = list
	 } else if ((contentType == null || contentType == "") && (Authorization != null || Authorization != "")) {
	 ArrayList list=Arrays.asList(header2)
	 listHeaders = list
	 } else {
	 KeywordUtil.markWarning("No header has been set for check API")
	 }
	 def request = builder
	 .withRestUrl(APIURL)
	 .withHttpHeaders(listHeaders)
	 .withRestRequestMethod("POST")
	 .withTextBodyContent(body)
	 .build()
	 ResponseObject response = WS.sendRequest(request)
	 if (response.getStatusCode ()!=200) {
	 KeywordUtil.markFailedAndStop('Failed, error code '+response.getStatusCode())
	 }
	 JsonSlurper jsonSlupper = new JsonSlurper()
	 Object getData = jsonSlupper.parseText(response.getResponseBodyContent())
	 return getData
	 }
	 */
	@Keyword
	public static Object GetFullResponse (String EndPoint, String bodyParam, String contentType, String Authorization, String Cookie) {
		String APIURL = EndPoint
		def builder = new RestRequestObjectBuilder()
		ArrayList defaultHeaders
		TestObjectProperty headerContent
		TestObjectProperty headerAuth
		TestObjectProperty headerCookie
		boolean cont = false
		boolean auth = false
		boolean cook = false

		if (contentType.size() > 0) {
			cont = true
		}

		if (Authorization.size() > 0) {
			auth = true
		}

		if (Cookie.size() > 0) {
			cook = true
		}

		if (cont) {
			headerContent = new TestObjectProperty("Content-Type", ConditionType.EQUALS, contentType)
		}

		if (auth) {
			headerAuth = new TestObjectProperty("Authorization", ConditionType.EQUALS, Authorization)
		}

		if (cook) {
			headerCookie = new TestObjectProperty("Cookie", ConditionType.EQUALS, Cookie)
		}

		if ((cont) && (auth) && (cook)) {
			defaultHeaders = Arrays.asList(headerContent, headerAuth, headerCookie)

		} else if ((cont) && (auth) && (!cook)) {
			defaultHeaders = Arrays.asList(headerContent, headerAuth)

		} else if ((cont) && (!auth) && (cook)) {
			defaultHeaders = Arrays.asList(headerContent, headerCookie)

		} else if ((!cont) && (auth) && (cook)) {
			defaultHeaders = Arrays.asList(headerAuth, headerCookie)

		} else if ((cont) && (!auth) && (!cook)) {
			defaultHeaders = Arrays.asList(headerContent)

		}

		RequestObject request = new RestRequestObjectBuilder()
				.withRestUrl(APIURL)
				.withHttpHeaders(defaultHeaders)
				.withRestRequestMethod("POST")
				.withTextBodyContent(bodyParam)
				.build()

		ResponseObject response = WS.sendRequest(request)

		if (response.getStatusCode ()!=200) {
			KeywordUtil.markFailedAndStop('Failed, error code '+response.getStatusCode())
		}

		return response.getResponseBodyContent()
	}
	/*
	 @Keyword
	 public static void CompareResponseData (String EndPoint, String bodyParam, String contentType, String Authorization, Object JSON) {
	 String APIURL = EndPoint
	 def builder = new RestRequestObjectBuilder()
	 ArrayList listHeaders
	 TestObjectProperty header
	 TestObjectProperty header2
	 def body = (bodyParam)
	 if (contentType != null || contentType != "") {
	 TestObjectProperty head=new TestObjectProperty("Content-Type", ConditionType.EQUALS, contentType)
	 header = head
	 }
	 if (Authorization != null || Authorization != "") {
	 TestObjectProperty head2=new TestObjectProperty("Authorization", ConditionType.EQUALS, Authorization)
	 header2 = head2
	 }
	 if ((contentType != null || contentType != "") && (Authorization != null || Authorization != "")) {
	 ArrayList list=Arrays.asList(header, header2)
	 listHeaders = list
	 } else if ((contentType != null || contentType != "") && (Authorization == null || Authorization == "")) {
	 ArrayList list=Arrays.asList(header)
	 listHeaders = list
	 } else if ((contentType == null || contentType == "") && (Authorization != null || Authorization != "")) {
	 ArrayList list=Arrays.asList(header2)
	 listHeaders = list
	 } else {
	 KeywordUtil.markWarning("No header has been set for check API")
	 }
	 def request = builder
	 .withRestUrl(APIURL)
	 .withHttpHeaders(listHeaders)
	 .withRestRequestMethod("POST")
	 .withTextBodyContent(body)
	 .build()
	 ResponseObject response = WS.sendRequest(request)
	 if (response.getStatusCode ()!=200) {
	 KeywordUtil.markFailedAndStop('Failed, error code '+response.getStatusCode())
	 }
	 Object result = response.getResponseBodyContent().trim()
	 if (result == JSON) {
	 KeywordUtil.markPassed("Value match")
	 } else {
	 KeywordUtil.markWarning('Value tidak sesuai')
	 }
	 }
	 */
	@Keyword
	public static Object getResponseData (ResponseObject Request) {
		JsonSlurper test = new JsonSlurper()

		if (Request.getStatusCode ()!=200) {
			KeywordUtil.markFailedAndStop('Failed, error code '+Request.getStatusCode())
		}

		Object result = test.parseText(Request.getResponseBodyContent())
		return result
	}

}