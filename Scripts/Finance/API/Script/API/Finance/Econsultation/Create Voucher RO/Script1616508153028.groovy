import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI
import com.keyword.GEN5

if (Step == "Get Member Valid"){
	
	//******************** QUERY UTK GET DATA MEMBER VALID ********************//
	
	GlobalVariable.DataMemberNo =  UI.getOneRowDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_GetValidMember)
	
	println (GlobalVariable.DataMemberNo)
	
	//******************** CEK ELIGIBILITY MEMBER - OP ********************//
	
	UI.connectDB('172.16.94.70', 'SEA')
	
	println (GlobalVariable.SEA_CekEligibilityMember.replace("GVMemberNo",GlobalVariable.DataMemberNo[4]).replace("GVBookDate",GlobalVariable.DataMemberNo[1]).replace("GVClaimNo",""))
	
	UI.execute(GlobalVariable.SEA_CekEligibilityMember.replace("GVMemberNo",GlobalVariable.DataMemberNo[4]).replace("GVBookDate",GlobalVariable.DataMemberNo[1]).replace("GVClaimNo",""))
	
} else if (Step == "Insert/Reschedule Biaya Dokter"){

	//******************** INSERT KE TABEL econsultation_claimdetail ********************//
	
	GlobalVariable.ParamPaymentGateway = ParamPG
	 	
	println (GlobalVariable.ParamPaymentGateway) //Seq,Booking,PaymentMethod(CC,VA,GoPay,ShopeePay),Nominal,Deskripsi
	
	def now = new Date()
	
	def formatdate = now.format('yyyyMMddHHmm')
	
	println(formatdate)
	
	if (GlobalVariable.BookingNo == ""){
	
		GlobalVariable.BookingNo = GlobalVariable.ParamPaymentGateway[1] + "-" + formatdate
	
		println (GlobalVariable.BookingNo)
	} 
	
	def Action
	
	def ClaimNo = ''
	
	def Deskripsi = GlobalVariable.ParamPaymentGateway[4]
		
	def GVSeq = ''
	
	ArrayList DataEconsul_ClaimDetail = new ArrayList()
	
	DataEconsul_ClaimDetail =  UI.getOneRowDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_GetDataEconsultation_claimdetail.replace("GVbookingno", GlobalVariable.BookingNo))

	println (DataEconsul_ClaimDetail)
	
	if (GlobalVariable.ParamAll.size() == 2 && GlobalVariable.ParamPaymentGateway[0] == "1"){ 
		
		Action = 'Save'
		
		GVSeq = '1'
		
	} else if (GlobalVariable.ParamAll.size() == 2 && GlobalVariable.ParamPaymentGateway[0] == "2"){ //utk skenario 1x bayar / tanpa obat
			
		Action = 'FinishWithoutMedicine'
		
		GVSeq = '2'
		
		ClaimNo = DataEconsul_ClaimDetail[0]
	
	} else if (GlobalVariable.ParamAll.size() > 2 && GlobalVariable.ParamPaymentGateway[0] == "1"){
	
		Action = 'Save'
		
		GVSeq = '1'
				
	} else if (GlobalVariable.ParamAll.size() > 2 && GlobalVariable.ParamPaymentGateway[0] == "2"){
	
		Action = 'Save'
		
		GVSeq = '1'
		
		ClaimNo = DataEconsul_ClaimDetail[0]
		
	} else if (GlobalVariable.ParamAll.size() > 2 && GlobalVariable.ParamPaymentGateway[0] == "3"){
	
		Action = 'FinishWithMedicine'
		
		GVSeq = '2'
		
		ClaimNo = DataEconsul_ClaimDetail[0]
		
	}
	
	if (GlobalVariable.ParamAll.size() > 2 || (GlobalVariable.ParamAll.size() == 2 && GlobalVariable.ParamPaymentGateway[0] == "1")){ //untuk proses data yg dg obat & tanpa obat tetapi byr dokter
	
		UI.connectDB('172.16.94.70', 'SEA')
		
		println (GlobalVariable.SEA_InsertToEconsultation_claimdetail.replace("GVBookDate", GlobalVariable.DataMemberNo[1]).replace("GVType",'1').replace("GVDeskripsi", '').replace("GVNominal", GlobalVariable.ParamPaymentGateway[3]).replace("GVBookingNo", GlobalVariable.BookingNo).replace("GVClaimNo","").replace("GVSeq", GVSeq).replace("GVMemberNo", GlobalVariable.DataMemberNo[4]))
		
		UI.execute(GlobalVariable.SEA_InsertToEconsultation_claimdetail.replace("GVBookDate", GlobalVariable.DataMemberNo[1]).replace("GVType",'1').replace("GVDeskripsi", '').replace("GVNominal", GlobalVariable.ParamPaymentGateway[3]).replace("GVBookingNo", GlobalVariable.BookingNo).replace("GVClaimNo","").replace("GVSeq", GVSeq).replace("GVMemberNo", GlobalVariable.DataMemberNo[4]))
	
		println (GlobalVariable.SEA_GetDataEconsultation_claimdetail.replace("GVbookingno", GlobalVariable.BookingNo))
			
		DataEconsul_ClaimDetail =  UI.getOneRowDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_GetDataEconsultation_claimdetail.replace("GVbookingno", GlobalVariable.BookingNo))
	
		println (DataEconsul_ClaimDetail)
	
		if (DataEconsul_ClaimDetail[9] == '0.0' && DataEconsul_ClaimDetail[10] == '0.0'){
					
			if (DataEconsul_ClaimDetail[6] == DataEconsul_ClaimDetail[8]){
				
				UI.connectDB('172.16.94.70', 'SEA')
				
				println (GlobalVariable.SEA_UpdateUnpaidEconsul.replace("GVNominalAccepted","0").replace("GVNominalUnpaid",DataEconsul_ClaimDetail[6]).replace("GVbookingno", GlobalVariable.BookingNo).replace("GVseqno",DataEconsul_ClaimDetail[2]))
				
				UI.execute(GlobalVariable.SEA_UpdateUnpaidEconsul.replace("GVNominalAccepted","0").replace("GVNominalUnpaid",DataEconsul_ClaimDetail[6]).replace("GVbookingno", GlobalVariable.BookingNo).replace("GVseqno",DataEconsul_ClaimDetail[2]))
				
			}
	
			DataEconsul_ClaimDetail =  UI.getOneRowDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_GetDataEconsultation_claimdetail.replace("GVbookingno", GlobalVariable.BookingNo))
			
			println (DataEconsul_ClaimDetail)
			
		} else if (DataEconsul_ClaimDetail[9] == '0.0' && (DataEconsul_ClaimDetail[10] > 0 || DataEconsul_ClaimDetail[12] > 0)){
		
			println (GlobalVariable.SEA_UpdateUnpaidEconsul.replace("GVbookingno", GlobalVariable.BookingNo).replace("GVseqno",DataEconsul_ClaimDetail[2].replace("accepted='GVNominalAccepted',", "").replace("GVNominalUnpaid", DataEconsul_ClaimDetail[10]).replace("where",",excess='0',excessemp='0' where")))
		
			UI.execute(GlobalVariable.SEA_UpdateUnpaidEconsul.replace("GVbookingno", GlobalVariable.BookingNo).replace("GVseqno",DataEconsul_ClaimDetail[2].replace("accepted='GVNominalAccepted',", "").replace("GVNominalUnpaid", DataEconsul_ClaimDetail[10]).replace("where",",excess='0',excessemp='0' where")))
			
			DataEconsul_ClaimDetail =  UI.getOneRowDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_GetDataEconsultation_claimdetail.replace("GVbookingno", GlobalVariable.BookingNo))
			
			println (DataEconsul_ClaimDetail)
		}
	}
	
	String Fee = ''
	
	String QueryCalc = ''
	
	String PaymentTypeCode = ''

	ArrayList ParamPGNominal = new ArrayList()
	
	// step ini khusus kl lewat payment gateway
	if (GlobalVariable.ParamPaymentGateway[3] != ""){
		
		if (GlobalVariable.ParamPaymentGateway[2] == "CC"){
			
			Fee = '0'
			
			QueryCalc = "Select '0' as Fee, '"+ DataEconsul_ClaimDetail[9] +"' as Nominal"
			
			PaymentTypeCode = UI.getValueDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_GetPaymentType.replace("GVName","Credit Card"),'Code')
			
			println (PaymentTypeCode)
			
			println (QueryCalc)
			
		} else if (GlobalVariable.ParamPaymentGateway[2] == "VA"){
		
			Fee = '4400'
			
			QueryCalc = 'Select ' + Fee + ' as Fee,' + DataEconsul_ClaimDetail[9] + '-' + Fee + ' as Nominal'
			
			PaymentTypeCode = UI.getValueDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_GetPaymentType.replace("GVName","Permata Bank Transfer"),'Code')
			
			println (PaymentTypeCode)
			
			println (QueryCalc)
			
		} else if (GlobalVariable.ParamPaymentGateway[2] == "GoPay"){
		
			Fee = '2/100'
			
			QueryCalc = 'Select ' + DataEconsul_ClaimDetail[9] + '*' + Fee + ' as Fee,' + DataEconsul_ClaimDetail[9] + '-(' + DataEconsul_ClaimDetail[9] + '*' + Fee + ') as Nominal'
			
			PaymentTypeCode = UI.getValueDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_GetPaymentType.replace("GVName","GOPAY"),'Code')
			
			println (PaymentTypeCode)
			
			println (QueryCalc)
			
		} else if (GlobalVariable.ParamPaymentGateway[2] == "ShopeePay"){
		
			Fee = '1.5/100'
			
			QueryCalc = 'Select ' + DataEconsul_ClaimDetail[9] + '*' + Fee + ' as Fee,' + DataEconsul_ClaimDetail[9] + '-(' + DataEconsul_ClaimDetail[9] + '*' + Fee + ') as Nominal'
			
			println (QueryCalc)
			
			PaymentTypeCode = UI.getValueDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_GetPaymentType.replace("GVName","Shopee Pay"),'Code')
			
			println (PaymentTypeCode)
		}
		
		ParamPGNominal =  UI.getOneRowDatabase('172.16.94.70', 'SEA', QueryCalc)
		
		println (ParamPGNominal)
	
	}
	
	//******************** INSERT KE TABEL EConsultationAutomaticVoucherQueue ********************//
				
	if (GlobalVariable.ParamAll.size() > 2 && GlobalVariable.ParamPaymentGateway[0] == "3"){ // untuk step biaya obat
		
		println (GlobalVariable.SEA_InsertToEConsultationAutomaticVoucherQueue.replace("--INSERT","INSERT").replace("GVSeq", GVSeq).replace("GVDeskripsi", Deskripsi).replace("GVFee", ParamPGNominal[0]).replace("GVNominal", ParamPGNominal[1]).replace("GVPaymentTypeCode", PaymentTypeCode).replace("GVTransactionReference", GlobalVariable.BookingNo + "-" + GlobalVariable.ParamPaymentGateway[0]).replace("GVAction", Action).replace("GVBookDate", GlobalVariable.DataMemberNo[1]).replace("GVMemberNo", GlobalVariable.DataMemberNo[4]).replace("GVClaimNo", ClaimNo).replace("GVBookingNo",GlobalVariable.BookingNo))
		
		GEN5.updateValueDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_InsertToEConsultationAutomaticVoucherQueue.replace("--INSERT","INSERT").replace("GVSeq", GVSeq).replace("GVDeskripsi", Deskripsi).replace("GVFee", ParamPGNominal[0]).replace("GVNominal", ParamPGNominal[1]).replace("GVPaymentTypeCode", PaymentTypeCode).replace("GVTransactionReference", GlobalVariable.BookingNo + "-" + GlobalVariable.ParamPaymentGateway[0]).replace("GVAction", Action).replace("GVBookDate", GlobalVariable.DataMemberNo[1]).replace("GVMemberNo", GlobalVariable.DataMemberNo[4]).replace("GVClaimNo", ClaimNo).replace("GVBookingNo",GlobalVariable.BookingNo))
	
	} else if (GlobalVariable.ParamPaymentGateway[0] != "3" && GlobalVariable.ParamPaymentGateway[3] != ""){ // untuk step biaya dokter
	
		println (GlobalVariable.SEA_InsertToEConsultationAutomaticVoucherQueue.replace("GVDeskripsi", Deskripsi).replace("GVFee", ParamPGNominal[0]).replace("GVSeq", GVSeq).replace("GVNominal", ParamPGNominal[1]).replace("GVPaymentTypeCode", PaymentTypeCode).replace("GVTransactionReference", GlobalVariable.BookingNo + "-" + GlobalVariable.ParamPaymentGateway[0]).replace("GVAction", Action).replace("GVBookDate", GlobalVariable.DataMemberNo[1]).replace("GVMemberNo", GlobalVariable.DataMemberNo[4]).replace("GVClaimNo", ClaimNo).replace("GVBookingNo",GlobalVariable.BookingNo))	
	
		GEN5.updateValueDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_InsertToEConsultationAutomaticVoucherQueue.replace("GVDeskripsi", Deskripsi).replace("GVFee", ParamPGNominal[0]).replace("GVNominal", ParamPGNominal[1]).replace("GVPaymentTypeCode", PaymentTypeCode).replace("GVTransactionReference", GlobalVariable.BookingNo + "-" + GlobalVariable.ParamPaymentGateway[0]).replace("GVAction", Action).replace("GVBookDate", GlobalVariable.DataMemberNo[1]).replace("GVMemberNo", GlobalVariable.DataMemberNo[4]).replace("GVClaimNo", ClaimNo).replace("GVBookingNo",GlobalVariable.BookingNo))
	
	} else if (GlobalVariable.ParamPaymentGateway[0] == "2" && GlobalVariable.ParamPaymentGateway[3] == ""){ // untuk step tanpa biaya obat
	
		println (GlobalVariable.SEA_InsertToEConsultationAutomaticVoucherQueue.replace("GVDeskripsi", Deskripsi).replace("GVFee", "0.0").replace("GVSeq", GVSeq).replace("GVNominal", "0.0").replace("GVPaymentTypeCode", "").replace("GVTransactionReference", "").replace("GVAction", Action).replace("GVBookDate", GlobalVariable.DataMemberNo[1]).replace("GVMemberNo", GlobalVariable.DataMemberNo[4]).replace("GVClaimNo", ClaimNo).replace("GVBookingNo",GlobalVariable.BookingNo))	
	
		GEN5.updateValueDatabase('172.16.94.70', 'SEA', GlobalVariable.SEA_InsertToEConsultationAutomaticVoucherQueue.replace("GVDeskripsi", Deskripsi).replace("GVFee", "0.0").replace("GVSeq", GVSeq).replace("GVNominal", "0.0").replace("GVPaymentTypeCode", "").replace("GVTransactionReference", "").replace("GVAction", Action).replace("GVBookDate", GlobalVariable.DataMemberNo[1]).replace("GVMemberNo", GlobalVariable.DataMemberNo[4]).replace("GVClaimNo", ClaimNo).replace("GVBookingNo",GlobalVariable.BookingNo))
	}
			
}
	


