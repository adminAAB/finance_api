import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper


def savePaymentInstrument = WS.sendRequest(findTestObject('Object Repository/Payment Instrument/SavePaymentInstrument'))

jsonSlurper = new JsonSlurper()

savePaymentInstrument = jsonSlurper.parseText(savePaymentInstrument.getResponseText())

println (savePaymentInstrument)

GlobalVariable.PaymentInstrumentNo  = savePaymentInstrument.data.PaymentInstrumentNo[0]

println (GlobalVariable.PaymentInstrumentNo)

if (savePaymentInstrument.data.status == false){
	
	KeywordUtil.markFailedAndStop("Failed "+ Action +" Save Payment Instrument with message : "+ savePaymentInstrument.data.message)
	
} else {

	KeywordUtil.markPassed("Save Payment Instrument success")
}







