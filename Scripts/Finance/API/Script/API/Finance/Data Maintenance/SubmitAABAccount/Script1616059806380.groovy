import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil
	
	String AABAccountCode = ''
	
	String AABAccountNo = ''
	
	String AABAccountDescription = ''
	
	def InternalDistributionID = 0
	
	def AccountName = ''
	
	def COACode = ''
	
	def CurrencyID = ''
	
	def BankBranchID = ''
	
	def CompanyID = ''
	
	def BankID = ''
	
	def IsDC = ''
	
	def ActiveStatusID = ''
	
	if (Action == 'Edit'){
		
		GlobalVariable.ParamList = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetParamSubmitAABAccount)
		
		GlobalVariable.ParamList1 = [GlobalVariable.ParamList[0],'Account 1 automate','1221114','3009199121','Account Name 1 automate','5','4','1','4','2393','1','2'] //AABAccountDescription, COACode, AABAccountNo, AccountName, CurrencyID, InternalDistributionID, IsDC, BankID, BankBranchID, CompanyID, ActiveStatusID     
		
		println (GlobalVariable.ParamList)
		
		println (GlobalVariable.ParamList1)
		
	} else {
	
		GEN5.updateValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_DeleteAABAccount)
		
		GlobalVariable.ParamList1 = ['AUTO01','Account 1 automate','1221114','3009199121','Account Name 1 automate','5','4','1','4','2393','1','2'] //AABAccountDescription, COACode, AABAccountNo, AccountName, CurrencyID, InternalDistributionID, IsDC, BankID, BankBranchID, CompanyID, ActiveStatusID
				
		println (GlobalVariable.ParamList1)

	}
	
	AABAccountCode = GlobalVariable.ParamList1[0]
	
	AABAccountDescription =  GlobalVariable.ParamList1[1]

	COACode = GlobalVariable.ParamList1[2]
	
	AABAccountNo = GlobalVariable.ParamList1[3]
	
	AccountName = GlobalVariable.ParamList1[4]
	
	CurrencyID = GlobalVariable.ParamList1[5]
	
	InternalDistributionID = GlobalVariable.ParamList1[6]
	
	IsDC = GlobalVariable.ParamList1[7]
	
	BankID = GlobalVariable.ParamList1[8]
	
	BankBranchID = GlobalVariable.ParamList1[9]
	
	CompanyID = GlobalVariable.ParamList1[10]
	
	ActiveStatusID = GlobalVariable.ParamList1[11]
			
	ArrayList SourceAABAccount = new ArrayList()
	
	def SubmitAABAccount= WS.sendRequest(findTestObject('Object Repository/Data Maintenance/SubmitAABAccount', 
		[("AABAccountCode"): AABAccountCode, ("AABAccountDescription"): AABAccountDescription, ("COACode"): COACode,  
		("AccountNo"): AABAccountNo , ("AccountName"): AccountName , ("CurrencyID"): CurrencyID,
		("InternalDistributionID"): InternalDistributionID , ("IsDC"): IsDC , ("BankID"): BankID,
		("BankBranchID"): BankBranchID , ("CompanyID"): CompanyID , ("ActiveStatusID"): ActiveStatusID, ("Action"): Action]))	
	
	def jsonSlurper = new JsonSlurper()
	
	SubmitAABAccount = jsonSlurper.parseText(SubmitAABAccount.getResponseText())
	
	println (SubmitAABAccount)	
	
	SourceAABAccount.add(GlobalVariable.ParamList1)
	
	if (SubmitAABAccount.status == true && SubmitAABAccount.message == 'Success'){
				
		ArrayList APIAABAccount = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetParamSubmitAABAccount.replace("aabaccountcode like 'EUM%'","aabaccountcode = '" + AABAccountCode + "'"))
		
		println (APIAABAccount)
		
		println (SourceAABAccount)
		
		WebUI.verifyMatch(APIAABAccount.size().toString(), SourceAABAccount.size().toString(), false)
		
		WebUI.verifyMatch(APIAABAccount[0].size().toString(), SourceAABAccount[0].size().toString(), false)
		
		for (a = 0; a < SourceAABAccount.size(); a++){
			
			for (b = 0; b < SourceAABAccount[0].size(); b++){
			
				if (APIAABAccount[a][b].toString() == SourceAABAccount[a][b].toString()) {
					
					KeywordUtil.markPassed("Value " + APIAABAccount[a][b] +" from API same with Database.")
					
				} else {
					
					KeywordUtil.markFailedAndStop("Value from API = " + APIAABAccount[a][b] + " has different Value from database = " + SourceAABAccount[a][b])
				}
			}
			
		}
		
	} else {
	
		KeywordUtil.markFailedAndStop("Process submit / edit failed.")
	}	


	

	




