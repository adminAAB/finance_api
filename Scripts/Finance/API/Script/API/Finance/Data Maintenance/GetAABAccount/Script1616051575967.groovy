import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil
	
	if (Search == "InternalDistribution"){
	
		GlobalVariable.ParamList = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetInternalDistributionForSearch)
	
	} else {
	
		GlobalVariable.ParamList = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetAABAccountForSearch)
		
	}
	
	
	println (GlobalVariable.ParamList)
	
	String AABAccountCode = ''
	
	String AABAccountNo = ''
	
	String AABAccountDescription = ''
	
	def InternalDistributionID = 0
	
	String param = ''
		
	if (Search == "AccountNo"){
		
		AABAccountNo = GlobalVariable.ParamList[0]
		
		param = "AccountNo = '" + AABAccountNo + "'"
		
	} else if (Search == "BankAccountCode"){
	
		AABAccountCode = GlobalVariable.ParamList[1]
		
		param = "AABAccountCode = '" + AABAccountCode + "'"
		
	} else if (Search == "BankAccountName"){
	
		AABAccountDescription = GlobalVariable.ParamList[2]
		
		param = "AABAccountDescription = '" + AABAccountDescription + "'"
		
	} else if (Search == "InternalDistribution"){
	
		InternalDistributionID = GlobalVariable.ParamList[1]
		
		println (InternalDistributionID + " - " + GlobalVariable.ParamList  + " - " + GlobalVariable.ParamList[1])
		
		param = "InternalDistributionID = '" + InternalDistributionID + "'"
		
	}
	
	ArrayList SourceAABAccount = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetAABAccount.replace("param", param))

	def GetAABAccount= WS.sendRequest(findTestObject('Object Repository/Data Maintenance/GetAABAccount', 
		[("AABAccountCode"): AABAccountCode, ("AABAccountNo"): AABAccountNo , 
		("AABAccountDescription"): AABAccountDescription, ("InternalDistributionID"): InternalDistributionID]))	
	
	def jsonSlurper = new JsonSlurper()
	
	GetAABAccount = jsonSlurper.parseText(GetAABAccount.getResponseText())
	
	println (GetAABAccount)	
	
	ArrayList APIAABAccount = new ArrayList()
	
	Integer Totalitems = GetAABAccount.data.totalItems
	
	Integer TotalPages = GetAABAccount.data.totalPages
	
	println (Totalitems + " : " + TotalPages)
	
	for (x = 0; x < Totalitems; x++){
		
		ArrayList RawAPIAABAccount = new ArrayList()
			
		RawAPIAABAccount.add(GetAABAccount.data.items[x].AABAccountID)
			
		RawAPIAABAccount.add(GetAABAccount.data.items[x].AABAccountCode)
			
		RawAPIAABAccount.add(GetAABAccount.data.items[x].AABAccountDescription)
			
		RawAPIAABAccount.add(GetAABAccount.data.items[x].InternalDistributionID)
		
		RawAPIAABAccount.add(GetAABAccount.data.items[x].AccountNo)
		
		RawAPIAABAccount.add(GetAABAccount.data.items[x].AccountName)
			
		RawAPIAABAccount.add(GetAABAccount.data.items[x].CurrencyID)
			
		RawAPIAABAccount.add(GetAABAccount.data.items[x].BankID)
				
		RawAPIAABAccount.add(GetAABAccount.data.items[x].BankBranchID)
					
		RawAPIAABAccount.add(GetAABAccount.data.items[x].COACode)
	
		RawAPIAABAccount.add(GetAABAccount.data.items[x].ActiveStatusID)
		
		APIAABAccount.add(RawAPIAABAccount)
		
	}
	
	println (APIAABAccount)
	
	println (SourceAABAccount)
	
	WebUI.verifyMatch(APIAABAccount.size().toString(), SourceAABAccount.size().toString(), false)
	
	WebUI.verifyMatch(APIAABAccount[0].size().toString(), SourceAABAccount[0].size().toString(), false)
	
	for (a = 0; a < SourceAABAccount.size(); a++){
		
		for (b = 0; b < SourceAABAccount[0].size(); b++){ 
		
			if (APIAABAccount[a][b].toString() == SourceAABAccount[a][b].toString()) {
				
				KeywordUtil.markPassed("Value " + APIAABAccount[a][b] +" from API same with Database.")
				
			} else {
				
				KeywordUtil.markFailedAndStop("Value from API = " + APIAABAccount[a][b] + " has different Value from database = " + SourceAABAccount[a][b])
			}
		}
		
	}
	

	




