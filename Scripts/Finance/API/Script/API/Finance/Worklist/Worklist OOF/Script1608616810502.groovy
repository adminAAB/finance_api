import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5
import com.keyword.REA

ArrayList RawUser = new ArrayList()

if (Skenario == "Cancel"){
	
	RawUser = REA.getAllDataDatabase("172.16.94.145", "a2isfinanceDB", GlobalVariable.FIN_GetNextUser.replace("GVVoucherNo",GlobalVariable.VoucherNoCA))
	
} else {

	RawUser = REA.getAllDataDatabase("172.16.94.145", "a2isfinanceDB", GlobalVariable.FIN_GetNextUser.replace("GVVoucherNo",GlobalVariable.VoucherNo))

}

println (RawUser)

GlobalVariable.Status = RawUser[0][1].toString()

String user = RawUser[0][0].replace(", ","','")

GlobalVariable.NextUser = REA.getValueDatabase("172.16.94.70", "BeyondDB_Apr2020", GlobalVariable.BEY_UserWorklist.replace("GVlogID",user), "LOGID")

println (GlobalVariable.NextUser)

if (GlobalVariable.NextUser == 'BYK'){
	
	GlobalVariable.EncriptUsername = 'QllL'
	
} else if (GlobalVariable.NextUser == 'DKU'){

	GlobalVariable.EncriptUsername = 'REtV'
	
} else if (GlobalVariable.NextUser == 'DVI'){

	GlobalVariable.EncriptUsername = 'RFZJ'

} else if (GlobalVariable.NextUser == 'EUS'){

	GlobalVariable.EncriptUsername = 'RVVT'
	
} else if (GlobalVariable.NextUser == 'HDW'){

	GlobalVariable.EncriptUsername = 'SERX'
	
} else if (GlobalVariable.NextUser == 'ILN'){

	GlobalVariable.EncriptUsername = 'SUxO'
	
} else if (GlobalVariable.NextUser == 'JTM'){

	GlobalVariable.EncriptUsername = 'SlRN'
	
} else if (GlobalVariable.NextUser == 'ADJ') {

	GlobalVariable.EncriptUsername = 'QURK'
	
} else if (GlobalVariable.NextUser == 'NRO'){

	GlobalVariable.EncriptUsername = 'TlJP'
	
} else if (GlobalVariable.NextUser == 'YSS'){

	GlobalVariable.EncriptUsername = 'WVNT'
	
} else if (GlobalVariable.NextUser == 'SRN'){

	GlobalVariable.EncriptUsername = 'U1JO'
	
} else if (GlobalVariable.NextUser == 'CPA'){

	GlobalVariable.EncriptUsername = 'Q1BB'
	
} else if (GlobalVariable.NextUser == 'RCH'){

	GlobalVariable.EncriptUsername = 'UkNI'
	
} else if (GlobalVariable.NextUser == 'DPU'){

	GlobalVariable.EncriptUsername = 'RFBV'
	
} else if (GlobalVariable.NextUser == 'SPM'){

	GlobalVariable.EncriptUsername = 'U1BN'

} else if (GlobalVariable.NextUser == 'AWS'){

	GlobalVariable.EncriptUsername = 'QVdT'
	
} else if (GlobalVariable.NextUser == 'HWP'){

	GlobalVariable.EncriptUsername = 'SFdQ'
	
}

