import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper

GlobalVariable.VoucherNo = Param

println (Param)

GlobalVariable.VoucherTypeCode = VoucherTypeCode

GlobalVariable.ExportType = ExportType

def ExportVoucher = WS.sendRequest(findTestObject('Object Repository/Voucher/GenerateVoucher'))

def jsonSlurper = new JsonSlurper()

ExportVoucher = jsonSlurper.parseText(ExportVoucher.getResponseText())

println (ExportVoucher)

StatusExportVoucher = ExportVoucher.status

FileExportVoucher = ExportVoucher.file

messageExportVoucher = ExportVoucher.message

if (StatusExportVoucher == true && FileExportVoucher != '' && messageExportVoucher == ''){
	
	KeywordUtil.markPassed("Value Status Export Voucher = " + StatusExportVoucher + "and File Export Voucher "+ FileExportVoucher + " and message Export Voucher "+ messageExportVoucher +" from API same with expected result.")

} else {

	KeywordUtil.markFailedAndStop("Value Status Export Voucher = " + StatusExportVoucher + " has different value.")

}