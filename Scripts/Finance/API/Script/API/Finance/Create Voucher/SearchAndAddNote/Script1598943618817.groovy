import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.keyword.GEN5
import com.keyword.REA

String PolicyNo = ''

ArrayList NoteNo = new ArrayList()

String NoteNumber = ''

if (DataNota == "RandomNota"){
	
	NoteNumber = GlobalVariable.NoteNumber
	
} else if (DataNota == "HarcodeNota"){ // untuk flow create voucher

	println (GlobalVariable.ParamNoteNumber)
		
	NoteNumber = GlobalVariable.ParamNoteNumber
	
}


def searchAndAddNote = WS.sendRequest(findTestObject('Voucher/SearchAndAddNote', [('NoteNumber') : NoteNumber , ('TransactionTypeID') : TransactionTypeID]))

def jsonSlurper = new JsonSlurper()

searchAndAddNote = jsonSlurper.parseText(searchAndAddNote.getResponseText())

println (NoteNumber)

println (searchAndAddNote)

if (Scenario == "InvalidSearchAndAddNote"){
	
	String CountNote = NoteNumber.split(",").size()
	
	StatusAddNoteFailed = searchAndAddNote.data.counter.failed
	
	StatusAddNoteSuccess = searchAndAddNote.data.counter.successfully
	
	if (StatusAddNoteFailed.toInteger() == CountNote.toInteger() || (StatusAddNoteFailed.toInteger() == 0 && StatusAddNoteSuccess.toInteger() == 0)){
		
		KeywordUtil.markPassed("Value StatusAddNoteFailed: " + StatusAddNoteFailed + " StatusAddNoteSuccess: "+ StatusAddNoteSuccess +" from API same with Database.")
		
	} else {
	
		KeywordUtil.markFailedAndStop("Value StatusAddNoteFailed = " + StatusAddNoteFailed + " has different Value countnote = " + CountNote.toInteger())
	
	}

} else if (Scenario == "ValidSearchAndNoteW2"){
		
	String CountNote = NoteNumber.split(",").size()
		
	StatusAddNoteFailed = searchAndAddNote.data.counter.failed
		
	StatusAddNoteSuccess = searchAndAddNote.data.counter.successfully
		
	if (StatusAddNoteFailed.toInteger() == 0 && StatusAddNoteSuccess.toInteger() == CountNote.toInteger()){
			
		KeywordUtil.markPassed("Value StatusAddNoteFailed: " + StatusAddNoteFailed + " StatusAddNoteSuccess: "+ StatusAddNoteSuccess +" from API same with Database.")
		
	} else {
	
		KeywordUtil.markFailedAndStop("Value StatusAddNoteSuccess = " + StatusAddNoteSuccess + " has different Value countnote = " + CountNote.toInteger())
	
	}
	
} else if (Scenario == "CreateVoucher" || Scenario == "ValidateW2"){ // kondisi nota hardcode
	
	StatusAddNote = searchAndAddNote.data.counter.successfully
	
	println (StatusAddNote)
	
	Integer CountNote = NoteNumber.split(",").size()

	GlobalVariable.WTVoucherDetailID = searchAndAddNote.data.wtVoucherDetail.items.WTVoucherDetailID
	
	println (GlobalVariable.WTVoucherDetailID)
	
	if (StatusAddNote < CountNote){
		
		KeywordUtil.markFailedAndStop("Value StatusAddNote = " + StatusAddNote + " must " + CountNote + ".")
		
	}
	
	GlobalVariable.FinanceNoteID = searchAndAddNote.data.wtVoucherDetail.items.FinanceNoteID
	
	GlobalVariable.Journal = searchAndAddNote.data.wtVoucherDetail.items.Journal
	
	println (GlobalVariable.FinanceNoteID)
	
	println (GlobalVariable.Journal)
	
}



















