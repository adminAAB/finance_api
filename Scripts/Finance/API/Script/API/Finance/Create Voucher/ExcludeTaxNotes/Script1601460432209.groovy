import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper

GlobalVariable.ExcludeWithholdingTax = ExcludeWithholdingTax

GlobalVariable.ExcludeVAT = ExcludeVAT

println (GlobalVariable.VoucherTypeCode)

println (GlobalVariable.WTVoucherID)

def wtVoucherDetail = WS.sendRequest(findTestObject('Object Repository/Voucher/ExcludeTaxNotes'))

def jsonSlurper = new JsonSlurper()

wtVoucherDetail = jsonSlurper.parseText(wtVoucherDetail.getResponseText())

println (wtVoucherDetail)

//GlobalVariable.VoucherID = getVoucher.data.items.VoucherID
//
//println (GlobalVariable.VoucherID)
//WTVoucherID":18978,"WTVoucherDetailID":1056066,"FinanceNoteID":139930