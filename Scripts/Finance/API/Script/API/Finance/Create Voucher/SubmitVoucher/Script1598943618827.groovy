import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.keyword.GEN5
import com.keyword.UI


def submitVoucher 

GlobalVariable.TransactionTypeID = TransactionTypeID

GlobalVariable.PaymentMethodID = PaymentMethodID

GlobalVariable.AABBankAccountID = AABBankAccountID

GlobalVariable.AABBankAccountNo = AABBankAccountNo

GlobalVariable.Remarks = Remarks

GlobalVariable.PayerID = PayerID

GlobalVariable.VoucherTypeCode = VoucherTypeCode

submitVoucher = WS.sendRequest(findTestObject('Voucher/SubmitVoucher'))

jsonSlurper = new JsonSlurper()

submitVoucher = jsonSlurper.parseText(submitVoucher.getResponseText())

println (submitVoucher)

if (submitVoucher.data.status == false) {
		
	KeywordUtil.markFailedAndStop("Submit voucher error with message " + submitVoucher.data.message)

} else {

	KeywordUtil.markPassed("Submit voucher success")

}

GlobalVariable.VoucherNo = submitVoucher.data.voucherNo

println (GlobalVariable.VoucherNo)

GlobalVariable.ListVoucher.add(GlobalVariable.VoucherNo)
















