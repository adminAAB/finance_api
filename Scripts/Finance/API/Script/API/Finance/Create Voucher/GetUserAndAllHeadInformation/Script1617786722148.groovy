import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import oracle.sql.utilpack
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil
	
	println (GlobalVariable.username[GlobalVariable.username.size()-1])
	
	def GetUserHeadInfo = WS.sendRequest(findTestObject('Object Repository/Voucher/GetUserAndAllHeadInformation', [("UserID") : GlobalVariable.username[GlobalVariable.username.size()-1]]))
	
	def jsonSlurper = new JsonSlurper()
	
	GetUserHeadInfo = jsonSlurper.parseText(GetUserHeadInfo.getResponseText())
	
	println (GetUserHeadInfo)
	
	if (GetUserHeadInfo.status == true){
		
		println (GetUserHeadInfo.data)
														
		for (x = 0; x < GetUserHeadInfo.data.size(); x++){
							
			if (x == GetUserHeadInfo.data.size()-1) {
				
				GlobalVariable.HeadOrganizationEntityUserList += ('{"logID":"' + GetUserHeadInfo.data[x].logID + '",' +
				
					'"actorOfLogID":"' + GetUserHeadInfo.data[x].actorOfLogID + '",' +
				
					'"cdEntity":"' + GetUserHeadInfo.data[x].cdEntity + '",' +
				
					'"level":' + GetUserHeadInfo.data[x].level + ',' +
				
					'"internalDistributionCode":"' + GetUserHeadInfo.data[x].internalDistributionCode + '"}')
						
			} else {
				
				GlobalVariable.HeadOrganizationEntityUserList += ('{"logID":"' + GetUserHeadInfo.data[x].logID + '",' +
				
					'"actorOfLogID":"' + GetUserHeadInfo.data[x].actorOfLogID + '",' +
				
					'"cdEntity":"' + GetUserHeadInfo.data[x].cdEntity + '",' +
				
					'"level":' + GetUserHeadInfo.data[x].level + ',' +
				
					'"internalDistributionCode":"' + GetUserHeadInfo.data[x].internalDistributionCode + '"}') + ","
		
			}
			
		}
		
		println (GlobalVariable.HeadOrganizationEntityUserList)
		
	} else {
	
		KeywordUtil.markFailed("API response failed.")
	}




