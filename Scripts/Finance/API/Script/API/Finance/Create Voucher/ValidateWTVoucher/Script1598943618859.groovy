import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.keyword.GEN5
import com.keyword.REA

GlobalVariable.VoucherTypeCode = VoucherTypeCode

def ValidateWTVoucher = WS.sendRequest(findTestObject('Voucher/ValidateWTVoucher', [('VoucherTypeCode') : VoucherTypeCode]))

def jsonSlurper = new JsonSlurper()

StatusValidateWTVoucher = jsonSlurper.parseText(ValidateWTVoucher.getResponseText())

println (StatusValidateWTVoucher)

MessageValidate0 = StatusValidateWTVoucher.data

MessageValidate = StatusValidateWTVoucher.data.message

StatusValidate = StatusValidateWTVoucher.data.status

if (scenario == "InvalidNote") {
	
	WebUI.verifyMatch(StatusValidate.toString(), "false", false)
	
	WebUI.verifyMatch(MessageValidate, "Voucher invalid. Please check Policy No & Journal Type.", false)
		
} else if (scenario == "ValidNote") {

	WebUI.verifyMatch(StatusValidate.toString(), "true", false)
	
	WebUI.verifyMatch(MessageValidate, "", false)

} else if (scenario == "FlowVoucher") {

	String FinNoteID = ''
	
	println(GlobalVariable.FinanceNoteID)

	for (xx = 0; xx < GlobalVariable.FinanceNoteID.size(); xx ++){
		
		if (xx == GlobalVariable.FinanceNoteID.size()-1) {
		
			FinNoteID += GlobalVariable.FinanceNoteID[xx]
				
		} else {
		
			FinNoteID += GlobalVariable.FinanceNoteID[xx] + ','
		}
			
	}
	
	if (StatusValidate == false){
		
		KeywordUtil.markFailedAndStop("Error = " + MessageValidate)
		
	} else {
	
		KeywordUtil.markPassed("Passed.")
		
	}
	
} 


	
	



 



























