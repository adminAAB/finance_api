import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.keyword.GEN5
import com.keyword.UI

ArrayList Code = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetRequestPSEconsul)

GlobalVariable.RequestCode = Code[0]

def ID = Code[1]

println (Code)

ArrayList NoteData = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherFromNote.replace("distinct VoucherNo","distinct FN.FinanceNoteID, FN.NoteNumber").replace("GVNotenumber", NoteNumber))

println (NoteData)

GlobalVariable.Remarks = GEN5.getValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetRemarkROEconsul.replace("GVnotenumber",NoteNumber), "remark1")

def FinanceNoteID1 = NoteData[0][0].toInteger()

def FinanceNoteID2 = NoteData[1][0].toInteger()

println (GlobalVariable.ParamList1)

def Notenumber1 = GlobalVariable.ParamList1[0][0]

def Notenumber2 = GlobalVariable.ParamList1[1][0]

def RequestAmount1 = GlobalVariable.ParamList1[0][1]

def RequestAmount2 = GlobalVariable.ParamList1[1][1]

def Overshortage1 = GlobalVariable.ParamList1[0][2]

def Overshortage2 = GlobalVariable.ParamList1[1][2]

GlobalVariable.RequestDate = GlobalVariable.ParamList1[0][3]

println (GlobalVariable.Remarks)

println (GlobalVariable.RequestDate)

GenerateEConsultationVoucher = WS.sendRequest(findTestObject('Voucher/GenerateEConsultationVoucher', 
		[('FinanceNoteID1') : FinanceNoteID1, ('FinanceNoteID2') : FinanceNoteID2,
		('Notenumber1') : Notenumber1, ('Notenumber2') : Notenumber2,
		('RequestAmount1') : RequestAmount1, ('RequestAmount2') : RequestAmount2,
		('Overshortage1') : Overshortage1, ('Overshortage2') : Overshortage2,
		('ID') : ID]))

jsonSlurper = new JsonSlurper()

GenerateEConsultationVoucher = jsonSlurper.parseText(GenerateEConsultationVoucher.getResponseText())

println (GenerateEConsultationVoucher)

if (GenerateEConsultationVoucher.status == false) {
		
	KeywordUtil.markFailedAndStop("Generate voucher error with message " + GenerateEConsultationVoucher.error)

} else {

	KeywordUtil.markPassed("Generate voucher success")

}

GlobalVariable.VoucherNo = GenerateEConsultationVoucher.data

println (GlobalVariable.VoucherNo)

println (GlobalVariable.SEA_DeleteVoucherRequest.replace("GVVoucherNo",GlobalVariable.ParamList1[0][4]))

//GEN5.updateValueDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_DeleteVoucherRequest.replace("GVVoucherNo",GlobalVariable.ParamList1[0][4]))











