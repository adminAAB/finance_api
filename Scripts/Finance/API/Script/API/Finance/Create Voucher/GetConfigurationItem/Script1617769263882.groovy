import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import oracle.sql.utilpack
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil
	
	def GetConfigurationItem= WS.sendRequest(findTestObject('Object Repository/Voucher/GetConfigurationItem'))
	
	def jsonSlurper = new JsonSlurper()
	
	GetConfigurationItem = jsonSlurper.parseText(GetConfigurationItem.getResponseText())
	
	println (GetConfigurationItem)
	
	if (GetConfigurationItem.status == true){
		
		println (GetConfigurationItem.data)
		
		GlobalVariable.IterasiGetOrganizationEntityUserList = GetConfigurationItem.data.size()
		
		println (GlobalVariable.IterasiGetOrganizationEntityUserList) 
		
		for (aa = 0; aa < GetConfigurationItem.data.size(); aa++){
			
			ArrayList RawGroupCdEntity = new ArrayList()
			
			RawGroupCdEntity.add(GetConfigurationItem.data[aa].paramName)
			
			RawGroupCdEntity.add(GetConfigurationItem.data[aa].paramValue)
						
			GlobalVariable.GroupCdEntity.add(RawGroupCdEntity)
		}
		
		println (GlobalVariable.GroupCdEntity)
		
	} else {
	
		KeywordUtil.markFailed("API response failed.")
	}




