import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper

def NoteData = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetSuspendEconsul.replace("GVVoucherNo", GlobalVariable.VoucherNo))

GlobalVariable.NoteNo = NoteData[1]

def GetNoteOthers = WS.sendRequest(findTestObject('Voucher/GetNoteOthers'))

jsonSlurper = new JsonSlurper()

GetNoteOthers = jsonSlurper.parseText(GetNoteOthers.getResponseText())

println (GetNoteOthers)

if (GetNoteOthers.status == true){
		
	GlobalVariable.ParamAddWTVoucherDetailOthersEconsul.add(GetNoteOthers.data.financeNoteID)
	
	GlobalVariable.ParamAddWTVoucherDetailOthersEconsul.add(GetNoteOthers.data.noteNumber)
	
	GlobalVariable.ParamAddWTVoucherDetailOthersEconsul.add(GetNoteOthers.data.journalTypeCode)
	
	GlobalVariable.ParamAddWTVoucherDetailOthersEconsul.add(GetNoteOthers.data.subJournalTypeCode)
	
	GlobalVariable.ParamAddWTVoucherDetailOthersEconsul.add(GetNoteOthers.data.taxTypeCode)
	
	GlobalVariable.ParamAddWTVoucherDetailOthersEconsul.add(GetNoteOthers.data.currencyCode)
	
	GlobalVariable.ParamAddWTVoucherDetailOthersEconsul.add(GetNoteOthers.data.amount)
	
	GlobalVariable.ParamAddWTVoucherDetailOthersEconsul.add(GetNoteOthers.data.suspendResultCategoryCode)
	
	GlobalVariable.ParamAddWTVoucherDetailOthersEconsul.add(GetNoteOthers.data.referenceAABAccountID)
	
	GlobalVariable.ParamAddWTVoucherDetailOthersEconsul.add(GetNoteOthers.data.referencePaymentMethodID)
	
	println (GlobalVariable.ParamAddWTVoucherDetailOthersEconsul)
	
	GlobalVariable.ReferenceFinanceNoteID = GetNoteOthers.data.financeNoteID
	
	GlobalVariable.ReferenceNoteNumber = GetNoteOthers.data.noteNumber

} else {

	KeywordUtil.markFailedAndStop("Get note others failed.")
	
}










