import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper

if (Skenario == "CancelVoucher"){
	
	//GlobalVariable.VoucherID = GlobalVariable.VoucherIDCA.toString().replace("[","").replace("]","")
	
	GlobalVariable.VoucherID = GlobalVariable.VoucherIDCA
	
} else if (Skenario == "CreateVoucher PaymentInstrument"){

	GlobalVariable.VoucherID = "["+ GlobalVariable.VoucherID.toString() + "]"
	
}

GlobalVariable.Action = Action

def submitWorklist = WS.sendRequest(findTestObject('Worklist/SubmitWorklist'))

jsonSlurper = new JsonSlurper()

submitWorklist = jsonSlurper.parseText(submitWorklist.getResponseText())

println (submitWorklist)

if (submitWorklist.data.status == false && submitWorklist.status == false){
	
	KeywordUtil.markFailedAndStop("Failed "+ Action +" voucher with message : "+ submitWorklist.data.message)
	
} else if (submitWorklist.status == true){

	KeywordUtil.markPassed("Submit worklist success")
}








