import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil
	
	for (i = 0; i < GlobalVariable.IterasiGetOrganizationEntityUserList; i++){
		
		String StrGroupCdEntity = ""
		
		if (GlobalVariable.GroupCdEntity[i][0] == "FinanceGroupCdEntity"){
			
			StrGroupCdEntity = GlobalVariable.GroupCdEntity[i][0]
			
			GlobalVariable.FinanceGroupCdEntity = GlobalVariable.GroupCdEntity[i][1].split(",")
			
			println (GlobalVariable.FinanceGroupCdEntity)
			
			String StrFinanceGroupCdEntity = ''
			
			for (xx = 0; xx < GlobalVariable.FinanceGroupCdEntity.size(); xx ++){
				
				if (xx == GlobalVariable.FinanceGroupCdEntity.size()-1) {
					
					StrFinanceGroupCdEntity += '"' + GlobalVariable.FinanceGroupCdEntity[xx] + '"'
							
				} else {
					
					StrFinanceGroupCdEntity += '"' + GlobalVariable.FinanceGroupCdEntity[xx] + '",'
			
				}
					
			}
			
			GlobalVariable.CdEntity = StrFinanceGroupCdEntity
			
		} else if (GlobalVariable.GroupCdEntity[i][0] == "TaxGroupCdEntity"){
		
			StrGroupCdEntity = GlobalVariable.GroupCdEntity[i][0]
		
			GlobalVariable.TaxGroupCdEntity = GlobalVariable.GroupCdEntity[i][1].split(",")
			
			println (GlobalVariable.TaxGroupCdEntity)
			
			String StrTaxGroupCdEntity = ''
			
			for (xx = 0; xx < GlobalVariable.TaxGroupCdEntity.size(); xx ++){
				
				if (xx == GlobalVariable.TaxGroupCdEntity.size()-1) {
					
					StrTaxGroupCdEntity += '"' + GlobalVariable.TaxGroupCdEntity[xx] + '"'
							
				} else {
					
					StrTaxGroupCdEntity += '"' + GlobalVariable.TaxGroupCdEntity[xx] + '",'
			
				}
					
			}
			
			GlobalVariable.CdEntity = StrTaxGroupCdEntity
		
		}
	
		def GetOrganizationEntityUserList= WS.sendRequest(findTestObject('Object Repository/Voucher/GetOrganizationEntityUserList'))
		
		def jsonSlurper = new JsonSlurper()
		
		GetOrganizationEntityUserList = jsonSlurper.parseText(GetOrganizationEntityUserList.getResponseText())
		
		println (GetOrganizationEntityUserList)
		
		if (GetOrganizationEntityUserList.status == true){
			
			println (GetOrganizationEntityUserList.data)
												
			for (x = 0; x < GetOrganizationEntityUserList.data.size(); x++){
								
				if (StrGroupCdEntity == "FinanceGroupCdEntity"){
					
					if (GetOrganizationEntityUserList.data[x].actorOfLogID == null){								
						
						if (x == GetOrganizationEntityUserList.data.size()-1) {
							
							GlobalVariable.FinOrganizationEntityUserList += ('{"logID":"' + GetOrganizationEntityUserList.data[x].logID + '",' +
							
								'"actorOfLogID":' + GetOrganizationEntityUserList.data[x].actorOfLogID + ',' +
							
								'"cdEntity":"' + GetOrganizationEntityUserList.data[x].cdEntity + '",' +
							
								'"level":' + GetOrganizationEntityUserList.data[x].level + ',' +
							
								'"internalDistributionCode":"' + GetOrganizationEntityUserList.data[x].internalDistributionCode + '"}')
									
						} else {
							
							GlobalVariable.HeadOrganizationEntityUserList += ('{"logID":"' + GetOrganizationEntityUserList.data[x].logID + '",' +
							
								'"actorOfLogID":"' + GetOrganizationEntityUserList.data[x].actorOfLogID + '",' +
							
								'"cdEntity":"' + GetOrganizationEntityUserList.data[x].cdEntity + '",' +
							
								'"level":' + GetOrganizationEntityUserList.data[x].level + ',' +
							
								'"internalDistributionCode":"' + GetOrganizationEntityUserList.data[x].internalDistributionCode + '"}') + ","
					
						}
						
					} else {

						if (x == GetOrganizationEntityUserList.data.size()-1) {
							
							GlobalVariable.FinOrganizationEntityUserList += ('{"logID":"' + GetOrganizationEntityUserList.data[x].logID + '",' +
							
								'"actorOfLogID":"' + GetOrganizationEntityUserList.data[x].actorOfLogID + '",' +
							
								'"cdEntity":"' + GetOrganizationEntityUserList.data[x].cdEntity + '",' +
							
								'"level":' + GetOrganizationEntityUserList.data[x].level + ',' +
							
								'"internalDistributionCode":"' + GetOrganizationEntityUserList.data[x].internalDistributionCode + '"}')
									
						} else {
							
							GlobalVariable.HeadOrganizationEntityUserList += ('{"logID":"' + GetOrganizationEntityUserList.data[x].logID + '",' +
							
								'"actorOfLogID":"' + GetOrganizationEntityUserList.data[x].actorOfLogID + '",' +
							
								'"cdEntity":"' + GetOrganizationEntityUserList.data[x].cdEntity + '",' +
							
								'"level":' + GetOrganizationEntityUserList.data[x].level + ',' +
							
								'"internalDistributionCode":"' + GetOrganizationEntityUserList.data[x].internalDistributionCode + '"}') + ","
					
						}
						
					
					}
					
				} else if (StrGroupCdEntity == "TaxGroupCdEntity"){
					
					if (GetOrganizationEntityUserList.data[x].actorOfLogID == null){			
							
					
					
					} else {
					
						
					
					}
					
				}
								
			}
			
			println (GlobalVariable.HeadOrganizationEntityUserList)
			
			println (GlobalVariable.FinOrganizationEntityUserList)
			
			println (GlobalVariable.TaxOrganizationEntityUserList)
			
			
		} else {
	
			KeywordUtil.markFailed("API response failed.")
			
		}

	}

