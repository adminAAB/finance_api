import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper



def NoteInfo = WS.sendRequest(findTestObject('Object Repository/Voucher/GetEditNoteInfo'))

def jsonSlurper = new JsonSlurper()

NoteInfo = jsonSlurper.parseText(NoteInfo.getResponseText())

println (NoteInfo)

DataNote = NoteInfo.data

GlobalVariable.OriginalAmount = NoteInfo.data.OriginalAmount[0]

println (GlobalVariable.OriginalAmount)

GlobalVariable.PPNAmount = NoteInfo.data.PPNAmount[0]

println (GlobalVariable.PPNAmount)

GlobalVariable.ARAPAmount = NoteInfo.data.ARAPAmount[0]

println (GlobalVariable.ARAPAmount)




