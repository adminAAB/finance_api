import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.keyword.GEN5
import com.keyword.UI


def UploadFiles 

def now = new Date()

def formatdate = now.format('yyyy/MMM/dd')

println(formatdate)

GlobalVariable.DocumentDate = formatdate

GlobalVariable.DocumentTypeCode = DocumentTypeCode

UploadFiles = WS.sendRequest(findTestObject('Object Repository/Voucher/UploadAttachmentVoucher'))

jsonSlurper = new JsonSlurper()

UploadFiles = jsonSlurper.parseText(UploadFiles.getResponseText())

println (UploadFiles)

if (UploadFiles.data.status == false) {
		
	KeywordUtil.markFailedAndStop(UploadFiles.data.message)

} else {

	KeywordUtil.markPassed("Upload success")
}

















