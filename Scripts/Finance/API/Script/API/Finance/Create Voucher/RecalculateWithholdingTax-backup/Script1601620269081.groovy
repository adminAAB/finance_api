import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import java.text.NumberFormat
import java.math.RoundingMode
import java.text.DecimalFormat


GlobalVariable.IsVATExclude = IsVATExclude

GlobalVariable.IsWitholdingTaxExclude = IsWitholdingTaxExclude

if (Skenario == "Fullpayament"){
	
	GlobalVariable.IsFullPayment = true
	
	GlobalVariable.RequestAmount = GlobalVariable.OriginalAmount
	
} else if (Skenario == "Full Sisa OS"){

	GlobalVariable.RequestAmount = GlobalVariable.ARAPAmount

} else if (Skenario == "Kurang Bayar Overshort"){

	GlobalVariable.IsFullPayment = true
	
	GlobalVariable.FPOverShortage = 10000

	GlobalVariable.RequestAmount = GlobalVariable.ARAPAmount.toDouble() - 10000.0000

} else if (Skenario == "Lebih Bayar Overshort"){

	GlobalVariable.IsFullPayment = true
	
	GlobalVariable.FPOverShortage = -15000

	GlobalVariable.RequestAmount = (GlobalVariable.ARAPAmount.toDouble() + 15000).toDouble()

} else if (Skenario == "Partial"){

	GlobalVariable.RequestAmount = GlobalVariable.ARAPAmount.toFloat() - 230000

} else if (Skenario == "Lebih Bayar"){

	GlobalVariable.RequestAmount = GlobalVariable.ARAPAmount.toFloat() + 560000

}

println (GlobalVariable.RequestAmount)

println (GlobalVariable.OriginalAmount)

println (GlobalVariable.PPNAmount)

if (GlobalVariable.PPNAmount > 0){
	
	DecimalFormat df = new DecimalFormat("#.####")
	
	df.setRoundingMode(RoundingMode.CEILING);
	
	Double d = new Double(GlobalVariable.RequestAmount / 11)
	
	GlobalVariable.VATAmount = df.format(d)
	
	println (GlobalVariable.VATAmount)
	
}

def RecalculatePPH = WS.sendRequest(findTestObject('Object Repository/Voucher/RecalculateWithholdingTax'))

def jsonSlurper = new JsonSlurper()

RecalculatePPH = jsonSlurper.parseText(RecalculatePPH.getResponseText())

println (RecalculatePPH)

PPH = RecalculatePPH.data

GlobalVariable.calculateWitholdingTax = RecalculatePPH.data.calculateWitholdingTax




