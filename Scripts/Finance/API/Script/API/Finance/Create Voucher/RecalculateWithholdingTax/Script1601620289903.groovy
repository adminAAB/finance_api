import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import java.text.NumberFormat
import java.math.RoundingMode
import java.text.DecimalFormat

DecimalFormat df = new DecimalFormat("#.####")

df.setRoundingMode(RoundingMode.CEILING);

GlobalVariable.IsVATExclude = IsVATExclude

GlobalVariable.IsWitholdingTaxExclude = IsWitholdingTaxExclude

if (Skenario == "PartialFullY"){
	
	GlobalVariable.IsFullPayment = true
	
	GlobalVariable.FPOverShortage = 10000

	if (GlobalVariable.ARAPAmount.toDouble() > 0){//untuk hitung requestamount yg akomodir ARAP plus dan minus
		
		GlobalVariable.RequestAmount = GlobalVariable.ARAPAmount.toDouble() - GlobalVariable.FPOverShortage
		
		GlobalVariable.VATAmount =  GlobalVariable.PPNAmount
		
	} else {
	
		GlobalVariable.RequestAmount = GlobalVariable.ARAPAmount.toDouble() + GlobalVariable.FPOverShortage
		
		GlobalVariable.VATAmount =  GlobalVariable.PPNAmount * -1
	
	}
	
} else if (Skenario == "Custom"){
		
		GlobalVariable.IsFullPayment = true
		
		//GlobalVariable.FPOverShortage = 10000
	
		GlobalVariable.ARAPAmount = ARAPAmount
			
		GlobalVariable.RequestAmount = RequestAmount
			
		GlobalVariable.BankCharges = 0
			
			
	
} else if (Skenario == "PartialFullN"){

	if (GlobalVariable.ARAPAmount.toDouble() > 0){//untuk hitung requestamount yg akomodir ARAP plus dan minus
		
		GlobalVariable.RequestAmount = GlobalVariable.ARAPAmount.toFloat() - 23000
		
		GlobalVariable.VATAmount =  GlobalVariable.PPNAmount
		
	} else {
	
		GlobalVariable.RequestAmount = GlobalVariable.ARAPAmount.toFloat() + 23000
		
		GlobalVariable.VATAmount =  GlobalVariable.PPNAmount * -1
	}

}

println (GlobalVariable.RequestAmount)

println (GlobalVariable.OriginalAmount)

println (GlobalVariable.PPNAmount)

println (GlobalVariable.VATAmount)

//if (GlobalVariable.PPNAmount > 0){
//	
//	DecimalFormat df = new DecimalFormat("#.####")
//	
//	df.setRoundingMode(RoundingMode.CEILING);
//	
//	Double d = new Double(GlobalVariable.RequestAmount / 11)
//	
//	GlobalVariable.VATAmount = df.format(d)
//	
//	println (GlobalVariable.VATAmount)
//	
//}

GlobalVariable.WTVoucherDetailID = GlobalVariable.WTVoucherDetailID[0]

println (GlobalVariable.WTVoucherDetailID)

GlobalVariable.FinanceNotesID = GlobalVariable.FinanceNoteID[0]

println (GlobalVariable.WTVoucherDetailID)

def RecalculatePPH = WS.sendRequest(findTestObject('Object Repository/Voucher/RecalculateWithholdingTax'))

def jsonSlurper = new JsonSlurper()

RecalculatePPH = jsonSlurper.parseText(RecalculatePPH.getResponseText())

println (RecalculatePPH)

GlobalVariable.calculateWitholdingTax = RecalculatePPH.data.calculateWitholdingTax




