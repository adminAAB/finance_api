import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil

String Query = ''

	if (SearchBy == "NoteNo"){
		
		GlobalVariable.NoteNo = DataParam

		Query = "where FN.NoteNumber = '" + DataParam + "'"
		
	} else if (SearchBy == "PolicyNo"){
	
		GlobalVariable.PolicyNo = DataParam
	
		Query = "where FN.NoteNumber = '" + DataParam + "'"
	
	} else if (SearchBy == "ClaimNo"){
	
		GlobalVariable.ClaimNo = DataParam	
	
	} else if (SearchBy == "GroupNo"){
	
		GlobalVariable.GroupingNo = DataParam
		
		
	
	} else if (SearchBy == "CustomerName"){
	
		GlobalVariable.CustomerName = DataParam
		
		
	
	} else if (SearchBy == "Journal"){
	
		GlobalVariable.Journal = DataParam
		
		
		
	} else if (SearchBy == "OtherReference"){
	
		GlobalVariable.OtherReferenceValue = DataParam
		
	} else if (SearchBy == "NoteStatus"){
	
		GlobalVariable.NoteStatus = DataParam
	
	
	} else if (SearchBy == "PayerName"){
	
		GlobalVariable.PayerName = DataParam
	
	} else if (SearchBy == "SubJournal"){
	
		GlobalVariable.SubJournalType = DataParam
		
	} else if (SearchBy == "HoldStatus"){
	
		GlobalVariable.isHoldFlag = DataParam
	
	} else if (SearchBy == "ProductionDateFrom"){
	
		GlobalVariable.ProductionDateFrom = DataParam
	
	} else if (SearchBy == "ProductionDateTo"){
	
		GlobalVariable.ProductionDateTo = DataParam
		
	}
		
	ArrayList NoteData = GEN5.getOneColumnDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataInquiryNote.replace("where ", Query),'NoteNumber')

	ArrayList APINoteData = new ArrayList()
	
	Integer TotalItems = NoteData.size()
	
	println (TotalItems)
	
	def getNoteData = WS.sendRequest(findTestObject('Object Repository/Inquiry/GetFinanceNotes'))
	
	def jsonSlurper = new JsonSlurper()
	
	getNoteData = jsonSlurper.parseText(getNoteData.getResponseText())
	
	println (getNoteData)
	
	Integer TotalPages = getNoteData.data.totalPages
	
	ArrayList APINoteData0 = new ArrayList()
	
	if (TotalItems >= 1 && TotalItems <= 10) {
		
		for (x = 0; x < TotalItems; x++){
							
			APINoteData0.add(getNoteData.data.Items[x].FinanceNoteID)
			
			APINoteData0.add(getNoteData.data.Items[x].Journal)
			
			APINoteData0.add(getNoteData.data.Items[x].SubJournal)
			
			APINoteData0.add(getNoteData.data.Items[x].PolicyNo.trim())
			
			APINoteData0.add(getNoteData.data.Items[x].EndNo)
			
			APINoteData0.add(getNoteData.data.Items[x].NoteNo)
			
			APINoteData0.add(getNoteData.data.Items[x].ClaimNo)
			
			APINoteData0.add(getNoteData.data.Items[x].CustomerName.trim())
			
			APINoteData0.add(getNoteData.data.Items[x].PayerName.trim())
			
			APINoteData0.add(getNoteData.data.Items[x].Currency)
			
			APINoteData0.add(getNoteData.data.Items[x].NoteAmount)
			
			APINoteData0.add(getNoteData.data.Items[x].SettledAmount)
			
			APINoteData0.add(getNoteData.data.Items[x].ARAPAmount)
			
			APINoteData0.add(getNoteData.data.Items[x].PPN)
			
			APINoteData0.add(getNoteData.data.Items[x].PPH)
			
			APINoteData0.add(getNoteData.data.Items[x].GroupNo.trim())
			
			APINoteData0.add(getNoteData.data.Items[x].RefType)
			
			APINoteData0.add(getNoteData.data.Items[x].BatchNo)
			
			APINoteData0.add(getNoteData.data.Items[x].IsHoldFlag)			
			
			APINoteData0.add(getNoteData.data.Items[x].ChassisNo.trim())
			
			APINoteData0.add(getNoteData.data.Items[x].EngineNo.trim())
			
			APINoteData0.add(getNoteData.data.Items[x].OurReferenceNo)
			
			APINoteData0.add(getNoteData.data.Items[x].RegistrationNo)
			
			APINoteData0.add(getNoteData.data.Items[x].YourReferenceNo)
			
			APINoteData.add(APINoteData0)
			
		}
		
		ArrayList SourceNoteData = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataInquiryNote.replace("where ", Query))
	
		println (APINoteData)
		
		println (SourceNoteData)
		
		WebUI.verifyMatch(APINoteData.size().toString(), SourceNoteData.size().toString(), false)
		
		WebUI.verifyMatch(APINoteData[0].size().toString(), SourceNoteData[0].size().toString(), false)
		
		for (a = 0; a < SourceNoteData.size(); a++){
			
			for (b = 0; b < SourceNoteData[0].size(); b++){ 
				
				if (APINoteData[a][b].toString() == SourceNoteData[a][b].toString()) {
					
					KeywordUtil.markPassed("Value " + APINoteData[a][b] +" from API same with Database.")
					
				} else {
					
					KeywordUtil.markFailedAndStop("Value from API = " + APINoteData[a][b] + " has different Value from database = " + SourceNoteData[a][b])
				}
			}
			
		}
		
	} else if (TotalItems >= 10) { // untuk yg data banyak hanya compare by no nota dan countdata		
	
		for (b = 0; b < TotalPages; b++){
			
			GlobalVariable.PageNo = b + 1
			
			getNoteData = WS.sendRequest(findTestObject('Object Repository/Inquiry/GetFinanceNotes'))
			
			def parsedJson = new JsonSlurper()
			
			getNoteData = parsedJson.parseText(getNoteData.getResponseText())
			
			Integer currentPage = getNoteData.data.currentPage
			
			Integer ItemsPerPage = getNoteData.data.ItemsPerPage
			
			println (currentPage + " - " +  TotalPages)
			
			if (currentPage < TotalPages){
				
				for (x = 0; x < ItemsPerPage; x++){
												
					APINoteData0.add(getNoteData.data.Items[x].NoteNo)
						
				}
				
			} else { // untuk get data last page , jumlah itemnya tidak full 10
			
				Integer ItemsLastPage = TotalItems - (ItemsPerPage*(currentPage-1))
				
				println (ItemsLastPage)
			
				for (x = 0; x < ItemsLastPage; x++){
								
					APINoteData0.add(getNoteData.data.Items[x].NoteNo)
						
				}
			
			}
			
		}
		
		APINoteData.add(APINoteData0)
		
		ArrayList SourceNoteData = GEN5.getOneColumnDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataInquiryNote.replace("where ", Query),'NoteNumber')
		
		println (APINoteData[0])
		
		println (SourceNoteData)
		
		WebUI.verifyMatch(APINoteData[0].size().toString(), SourceNoteData.size().toString(), false)
			
		for (b = 0; b < SourceNoteData.size(); b++){
						
			if (APINoteData[0][b] == SourceNoteData[b]) {
				
				KeywordUtil.markPassed("Value " + APINoteData[0][b] +" from API same with Database.")
				
			} else {
				
				KeywordUtil.markFailedAndStop("Value from API = " + APINoteData[0][b] + " has different Value from database = " + SourceNoteData[b])
			}
				
		}
		
	} else if (TotalItems == 0 && getNoteData.data.TotalItems == 0 && getNoteData.status == true){
	
		KeywordUtil.markPassed("Value " + TotalItems +" from API same with Database.")
		
	}
	
	//reset param globalvariable
	GlobalVariable.NoteNo = ''

	GlobalVariable.PolicyNo = ''
	
	GlobalVariable.ClaimNo = ''

	GlobalVariable.GroupingNo = ''

	GlobalVariable.CustomerName = ''

	GlobalVariable.Journal = ''

	GlobalVariable.OtherReferenceValue = ''

	GlobalVariable.NoteStatus = ''

	GlobalVariable.PayerName = ''
	
	GlobalVariable.SubJournalType = ''
		
	GlobalVariable.isHoldFlag = ''
	
	GlobalVariable.ProductionDateFrom = ''
	
	GlobalVariable.ProductionDateTo = ''
	
	GlobalVariable.PageNo = 1
	





