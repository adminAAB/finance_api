import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.keyword.GEN5
import com.keyword.UI

def addCIFPersonal = WS.sendRequest(findTestObject('Object Repository/Penurunan Data/CIF/AddCIF - Personal', [
	('CustomerCode'): 'CJKPJ0005',
	('CustomerName'): 'PT Jardine Lloyd Thompson',
	('CustomerTypeCode'): 'C',
	('CustomerStatusTypeCode'): 'AST',
	('CustomerCategoryCode'): 'C',
	('IDCardTypeCode'): 'KTPP',
	('IDCardNumber'): '1234',
	('NameOnNPWP'): '',
	('NPWPNumber'): '',
	('NPWPAddress'): '',
	('IsPKP'): '1',
	('PKPNumber'): '',
	('CustomerUsageTypeCode'): 'Broker',
	('AddressTypeCodeIden'): 'IDEN',
	('AddressIden'): 'jalan sugus com shr',
	('AddressTypeCodeOffc'): 'OFFC',
	('AddressOffc'): 'World Trade Cebter, 10th Floor Jl. Jend. Sudirman Kav 29-31 Jakarta 12920',
	('CustomerAccountTypeCode'): 'S',
	('AccountNo'): '0200191161',
	('SwiftCode'): 'BBBAIDJA',
	('ClearingCode'): '0130307',
	('BankBranchDescription'): 'GEDUNG WTC II LT 21 JL JEND SUDIRMAN KAV 29-31',
	('AccountHolder'): 'PT. FEDERAL INTERNATIONAL FINANCE'
	]))


jsonSlurper = new JsonSlurper()

addCIFPersonal = jsonSlurper.parseText(addCIFPersonal.getResponseText())

println (addCIFPersonal)

if (addCIFPersonal.data.status == false) {
		
	KeywordUtil.markFailedAndStop("Add CIF Personal error with message " + addCIFPersonal.data.message)

} else {

	KeywordUtil.markPassed("Add CIF Personal success")

}