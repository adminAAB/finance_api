import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.keyword.GEN5
import com.keyword.UI


def addCIFCompany = WS.sendRequest(findTestObject('Object Repository/Penurunan Data/CIF/AddCIF - Company', [
	('CustomerCode'): 'CJKBA01',
	('CustomerName'): 'Budihardjo Adimuljo',
	('CustomerTypeCode'): 'P',
	('CustomerStatusTypeCode'): 'NAST',
	('CustomerCategoryCode'): 'C',
	('IDCardTypeCode'): 'PASS',
	('IDCardNumber'): '55555',
	('NameOnNPWP'): '',
	('NPWPNumber'): '',
	('NPWPAddress'): '',
	('IsPKP'): '0',
	('PKPNumber'): '',
	('CustomerUsageTypeCode'): 'Agent',
	('AddressTypeCode'): 'HOME',
	('Address'): 'an CV Anugrah Perdana ',
	('CustomerAccountTypeCode'): 'S',
	('AccountNo'): '0200191161',
	('SwiftCode'): 'BBBAIDJA',
	('ClearingCode'): '0130307',
	('BankBranchDescription'): 'GEDUNG WTC II LT 21 JL JEND SUDIRMAN KAV 29-31',
	('AccountHolder'): 'PT. FEDERAL INTERNATIONAL FINANCE'
	]))


jsonSlurper = new JsonSlurper()

addCIFCompany = jsonSlurper.parseText(addCIFCompany.getResponseText())

println (addCIFCompany)

if (addCIFCompany.data.status == false) {
		
	KeywordUtil.markFailedAndStop("Add CIF Company error with message " + addCIFCompany.data.message)

} else {

	KeywordUtil.markPassed("Add CIF Company success")

}













