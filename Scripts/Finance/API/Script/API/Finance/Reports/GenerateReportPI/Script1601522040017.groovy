import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.keyword.GEN5
import com.keyword.REA



def ReportPI = WS.sendRequest(findTestObject('Object Repository/Reports/Payment Instrument/GenerateReportPI'))

def jsonSlurper = new JsonSlurper()

ReportPI = jsonSlurper.parseText(ReportPI.getResponseText())

println (ReportPI)

StatusReportPI = ReportPI.status

FileReportPI = ReportPI.file

messageReportPI = ReportPI.message

if (StatusReportPI == true && FileReportPI != '' && messageReportPI == ''){
	
	KeywordUtil.markPassed("Value Status Report PI = " + StatusReportPI + "and File Report PI "+ FileReportPI + " and message Report PI "+ messageReportPI +" from API same with expected result.")

} else {

	KeywordUtil.markFailedAndStop("Value Status Report PI = " + StatusReportPI + " has different value.")

} 


	
