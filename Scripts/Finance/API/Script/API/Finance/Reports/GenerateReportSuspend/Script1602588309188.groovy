import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.keyword.GEN5
import com.keyword.REA


def now = new Date()

def getdate = now.format('yyyy-MM-dd')

String SendDate = getdate

def ReportSuspend = WS.sendRequest(findTestObject('Object Repository/Reports/Suspend/GenerateReportSuspend' ,
	[('Company') : Company, ('InternalDistributionCode') : InternalDistributionCode , ('AABAccountCode') : AABAccountCode
	,('SuspendCategoryCode') : SuspendCategoryCode, ('SendDate') : SendDate]))

def jsonSlurper = new JsonSlurper()

ReportSuspend = jsonSlurper.parseText(ReportSuspend.getResponseText())

println (ReportSuspend)

StatusReportSuspend = ReportSuspend.status

FileReportSuspend = ReportSuspend.file

messageReportSuspend = ReportSuspend.message

if (StatusReportSuspend == true && FileReportSuspend != '' && messageReportSuspend == ''){
	
	KeywordUtil.markPassed("Value Status Report Suspend = " + StatusReportSuspend + " and File Report Suspend = "+ FileReportSuspend + " and message Report Suspend "+ messageReportSuspend +" from API same with expected result.")

} else {

	KeywordUtil.markFailedAndStop("Value Status Report Suspend = " + StatusReportSuspend + " has different value.")

} 


	
