import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil
	

	GlobalVariable.Remarks = Remarks
	
	GlobalVariable.SuspendResultCategoryID = SuspendResultCategoryId
			
	GlobalVariable.PICCode  = PICCode
	
	GlobalVariable.PICName  = PICName
	
	ArrayList SourceSuspendUpdateDetails = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToUD_SuspendMonitoring.replace("GVnotenumber", GlobalVariable.NoteNumber))

	def UpdateDetailsSuspend= WS.sendRequest(findTestObject('Object Repository/Inquiry/UpdateDetailsSuspend'))
	
	def jsonSlurper = new JsonSlurper()
	
	UpdateDetailsSuspend = jsonSlurper.parseText(UpdateDetailsSuspend.getResponseText())
	
	println (UpdateDetailsSuspend)	
	
	println (GlobalVariable.FIN_GetDataCompareToUD_SuspendMonitoring.replace("GVnotenumber", GlobalVariable.NoteNumber))
	
	ArrayList APISuspendUpdateDetails = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToUD_SuspendMonitoring.replace("GVnotenumber", GlobalVariable.NoteNumber).replace("asc","desc"))
	
	SourceSuspendUpdateDetails[0][1] = GlobalVariable.PICName
	
	SourceSuspendUpdateDetails[0][3] = GlobalVariable.SuspendResultCategoryID
	
	SourceSuspendUpdateDetails[0][4] = '0'
	
	SourceSuspendUpdateDetails[0][7] = '0'
	
	if (APISuspendUpdateDetails[0][6] == 'null'){APISuspendUpdateDetails[0][6] = ''}
	
	if (APISuspendUpdateDetails[0][15] == 'null'){APISuspendUpdateDetails[0][15] = ''}
	
	if (SourceSuspendUpdateDetails[0][16] == ''){
		
		SourceSuspendUpdateDetails[0][16] = GlobalVariable.Remarks
		
	} else {
	
		SourceSuspendUpdateDetails[0][16] = GlobalVariable.Remarks + " - " + SourceSuspendUpdateDetails[0][16]
	
	}
	
//	SourceSuspendUpdateDetails[0][18] = GlobalVariable.username
	
	SourceSuspendUpdateDetails[0][19] = GlobalVariable.PICCode
		
	println (APISuspendUpdateDetails)
	
	println (SourceSuspendUpdateDetails)
	
	WebUI.verifyMatch(APISuspendUpdateDetails.size().toString(), SourceSuspendUpdateDetails.size().toString(), false)
	
	WebUI.verifyMatch(APISuspendUpdateDetails[0].size().toString(), SourceSuspendUpdateDetails[0].size().toString(), false)
	
	for (a = 0; a < SourceSuspendUpdateDetails.size(); a++){
		
		for (b = 0; b < SourceSuspendUpdateDetails[0].size(); b++){ 
		
			if (APISuspendUpdateDetails[a][b].toString() == SourceSuspendUpdateDetails[a][b].toString()) {
				
				KeywordUtil.markPassed("Value " + APISuspendUpdateDetails[a][b] +" from API same with Database.")
				
			} else {
				
				KeywordUtil.markFailedAndStop("Value from API = " + APISuspendUpdateDetails[a][b] + " has different Value from database = " + SourceSuspendUpdateDetails[a][b])
			}
		}
		
	}
	

	




