import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil
	
	def GetSuspendSettlementHistory= WS.sendRequest(findTestObject('Object Repository/Inquiry/GetSuspendSettlementHistory'))
	
	def jsonSlurper = new JsonSlurper()
	
	GetSuspendSettlementHistory = jsonSlurper.parseText(GetSuspendSettlementHistory.getResponseText())
	
	println (GetSuspendSettlementHistory)
	
	ArrayList RawSuspendSettlementHistory = new ArrayList()
	
	Integer TotalItems = GetSuspendSettlementHistory.data.TotalItems
	
	ArrayList APISuspendSettlementHistory = new ArrayList()
			
	ArrayList SourceSuspendSettlementHistory = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToSuspendSettlementHistory.replace("GVnotenumber", GlobalVariable.NoteNumber))

	if (SourceSuspendSettlementHistory != null){
			
		for (x = 0; x < TotalItems; x++){
							
			RawSuspendSettlementHistory.add(GetSuspendSettlementHistory.data.Items[x].VoucherNo)
			
//			RawSuspendSettlementHistory.add(GetSuspendSettlementHistory.data.Items[x].DtSettlement)			
			
			RawSuspendSettlementHistory.add(GetSuspendSettlementHistory.data.Items[x].Currency)
			
			RawSuspendSettlementHistory.add(GetSuspendSettlementHistory.data.Items[x].Nominal)
			
			RawSuspendSettlementHistory.add(GetSuspendSettlementHistory.data.Items[x].SettlementAmount)
			
			RawSuspendSettlementHistory.add(GetSuspendSettlementHistory.data.Items[x].ARAPAmount)
			
			RawSuspendSettlementHistory.add(GetSuspendSettlementHistory.data.Items[x].AABAccountDesc)
			
			RawSuspendSettlementHistory.add(GetSuspendSettlementHistory.data.Items[x].Remarks)
			
			APISuspendSettlementHistory.add(RawSuspendSettlementHistory)
			
		}
		
		println (APISuspendSettlementHistory)
		
		println (SourceSuspendSettlementHistory)
		
		WebUI.verifyMatch(APISuspendSettlementHistory.size().toString(), SourceSuspendSettlementHistory.size().toString(), false)
		
		WebUI.verifyMatch(APISuspendSettlementHistory[0].size().toString(), SourceSuspendSettlementHistory[0].size().toString(), false)
		
		for (a = 0; a < SourceSuspendSettlementHistory.size(); a++){
			
			for (b = 0; b < SourceSuspendSettlementHistory[0].size(); b++){
			
				if (APISuspendSettlementHistory[a][b].toString() == SourceSuspendSettlementHistory[a][b].toString()) {
					
					KeywordUtil.markPassed("Value " + APISuspendSettlementHistory[a][b] +" from API same with Database.")
					
				} else {
					
					KeywordUtil.markFailedAndStop("Value from API = " + APISuspendSettlementHistory[a][b] + " has different Value from database = " + SourceSuspendSettlementHistory[a][b])
				}
			}
			
		}
		
	} else {
	
		if (SourceSuspendSettlementHistory == null & TotalItems == 0) {
			
			KeywordUtil.markPassed("Value " + APISuspendSettlementHistory +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APISuspendSettlementHistory + " has different Value from database null")
		}
	
	}
			
	//reset param globalvariable
	GlobalVariable.NoteNumber = ''

	GlobalVariable.VoucherNo = ''

	




