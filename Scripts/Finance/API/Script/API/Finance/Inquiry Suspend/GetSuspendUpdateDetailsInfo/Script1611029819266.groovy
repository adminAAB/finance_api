import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil
	
	def GetSuspendUpdateDetailsInfo= WS.sendRequest(findTestObject('Object Repository/Inquiry/GetSuspendUpdateDetailsInfo'))
	
	def jsonSlurper = new JsonSlurper()
	
	GetSuspendUpdateDetailsInfo = jsonSlurper.parseText(GetSuspendUpdateDetailsInfo.getResponseText())
	
	println (GetSuspendUpdateDetailsInfo)	
												
	GlobalVariable.UnitCode = GetSuspendUpdateDetailsInfo.data[0].UnitCode
		
	String Unit = GEN5.getValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetUnit.replace("GVunitcode",GlobalVariable.UnitCode),"unitdescription")
	
	GlobalVariable.UnitDescription = Unit
	
	ArrayList Payer = (GetSuspendUpdateDetailsInfo.data[0].Payer).toString().split(" - ")
	
	GlobalVariable.PayerCode = Payer[0]
	
	GlobalVariable.PayerName = Payer[1]
	
	GlobalVariable.InternalDistributionID = (GetSuspendUpdateDetailsInfo.data[0].InternalDistributionId).toString()
	
	ArrayList InternalDistribution = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetInternalDistribution.replace("GVinternaldistributionID",GlobalVariable.InternalDistributionID))
				
	GlobalVariable.InternalDistributionCode = InternalDistribution[0]
		
	GlobalVariable.InternalDistributionDescription = InternalDistribution[1]
		
	ArrayList Salesman = GetSuspendUpdateDetailsInfo.data[0].Salesman.toString().split(" - ")
		
	GlobalVariable.SalesmanCode  = Salesman[0]
		
	GlobalVariable.SalesmanName  = Salesman[1]
			
	
	
		
	
		

	




