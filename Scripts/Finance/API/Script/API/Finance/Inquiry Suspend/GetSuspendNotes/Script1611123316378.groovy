import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil

	String QueryParam = ''
	
	if (SearchBy == "NoteNumber"){
		
		QueryParam = "fn.notenumber = '" + GlobalVariable.NoteNumber + "'"
		
	} else if (SearchBy == "VoucherNo"){
		
		QueryParam = "v.VoucherNo = '" + GlobalVariable.VoucherNo + "'"
		
	}
	
	def GetSuspendNotes= WS.sendRequest(findTestObject('Object Repository/Inquiry/GetSuspendNotes'))
	
	ArrayList SourceSuspendNotes = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToInquirySuspend.replace("FNA.rowstatus ='0'", "FNA.rowstatus ='0' and " + QueryParam))
	
	def jsonSlurper = new JsonSlurper()
	
	GetSuspendNotes = jsonSlurper.parseText(GetSuspendNotes.getResponseText())
	
	println (GetSuspendNotes)
	
	ArrayList RawSuspendNotes = new ArrayList()
	
	ArrayList APISuspendNotes = new ArrayList()
			
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].NoteNumber)
		
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].VoucherNo)
		
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].Currency)
			
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].NoteAmount)
	
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].SettledAmount)
		
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].ARAPAmount)
	
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].SuspendStatus)
	
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].SuspendCategoryDesc)
		
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].TpJournal)
		
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].TpSubJournal)
			
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].TaxTypeCode)		
			
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].AABAccountDesc)
		
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].NoteRemarks)
		
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].VoucherRemarks)
		
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].CreatedBy)
	
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].InternalDistribution)

	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].DirectorateDesc)
	
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].PICName)
	
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].Payer)
	
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].Salesman)
	
	RawSuspendNotes.add(GetSuspendNotes.data.Items[0].Unit)
	
	APISuspendNotes.add(RawSuspendNotes)
	
	println (APISuspendNotes)
	
	println (SourceSuspendNotes)
	
	WebUI.verifyMatch(APISuspendNotes.size().toString(), SourceSuspendNotes.size().toString(), false)
	
	WebUI.verifyMatch(APISuspendNotes[0].size().toString(), SourceSuspendNotes[0].size().toString(), false)
	
	for (a = 0; a < SourceSuspendNotes.size(); a++){
		
		for (b = 0; b < SourceSuspendNotes[0].size(); b++){ 
		
			if (APISuspendNotes[a][b].toString() == SourceSuspendNotes[a][b].toString()) {
				
				KeywordUtil.markPassed("Value " + APISuspendNotes[a][b] +" from API same with Database.")
				
			} else {
				
				KeywordUtil.markFailedAndStop("Value from API = " + APISuspendNotes[a][b] + " has different Value from database = " + SourceSuspendNotes[a][b])
			}
		}
		
	}
	

	




