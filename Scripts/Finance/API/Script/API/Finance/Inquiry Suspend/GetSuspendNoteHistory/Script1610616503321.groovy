import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil
	
	def getSuspendNoteHistory= WS.sendRequest(findTestObject('Object Repository/Inquiry/GetSuspendNoteHistory'))
	
	def jsonSlurper = new JsonSlurper()
	
	getSuspendNoteHistory = jsonSlurper.parseText(getSuspendNoteHistory.getResponseText())
	
	println (getSuspendNoteHistory)
	
	ArrayList RawSuspendNote = new ArrayList()
	
	ArrayList APISuspendNote = new ArrayList()
			
	RawSuspendNote.add(getSuspendNoteHistory.data[0].NoteNumber)
		
	//RawSuspendNote.add(getSuspendNoteHistory.data[0].NoteDate)
		
	RawSuspendNote.add(getSuspendNoteHistory.data[0].VoucherNo)
		
	RawSuspendNote.add(getSuspendNoteHistory.data[0].Currency)
		
	RawSuspendNote.add(getSuspendNoteHistory.data[0].SuspendStatus)
		
	RawSuspendNote.add(getSuspendNoteHistory.data[0].SuspendCategoryDesc)
	
	RawSuspendNote.add(getSuspendNoteHistory.data[0].NoteAmount)
	
	RawSuspendNote.add(getSuspendNoteHistory.data[0].SettledAmount)
		
	RawSuspendNote.add(getSuspendNoteHistory.data[0].ARAPAmount)
		
	RawSuspendNote.add(getSuspendNoteHistory.data[0].TpJournal)
		
	RawSuspendNote.add(getSuspendNoteHistory.data[0].TpSubJournal)
			
	RawSuspendNote.add(getSuspendNoteHistory.data[0].TaxTypeCode)		
			
	RawSuspendNote.add(getSuspendNoteHistory.data[0].AABAccountDesc)
		
	RawSuspendNote.add(getSuspendNoteHistory.data[0].NoteRemarks)
		
	RawSuspendNote.add(getSuspendNoteHistory.data[0].VoucherRemarks)
		
	RawSuspendNote.add(getSuspendNoteHistory.data[0].CreatedBy)
	
	RawSuspendNote.add(getSuspendNoteHistory.data[0].InternalDistribution)

	RawSuspendNote.add(getSuspendNoteHistory.data[0].DirectorateDesc)
	
	RawSuspendNote.add(getSuspendNoteHistory.data[0].PICName)
	
	RawSuspendNote.add(getSuspendNoteHistory.data[0].Payer)
	
	RawSuspendNote.add(getSuspendNoteHistory.data[0].Salesman)
	
	RawSuspendNote.add(getSuspendNoteHistory.data[0].Unit)
	
	APISuspendNote.add(RawSuspendNote)
	
//	println (GlobalVariable.FIN_GetDataCompareToSuspendNote.replace("GVnotenumber", GlobalVariable.NoteNumber).replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	ArrayList SourceSuspendNote = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToSuspendNote.replace("GVnotenumber", GlobalVariable.NoteNumber).replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APISuspendNote)
	
	println (SourceSuspendNote)
	
	WebUI.verifyMatch(APISuspendNote.size().toString(), SourceSuspendNote.size().toString(), false)
	
	WebUI.verifyMatch(APISuspendNote[0].size().toString(), SourceSuspendNote[0].size().toString(), false)
	
	for (a = 0; a < SourceSuspendNote.size(); a++){
		
		for (b = 0; b < SourceSuspendNote[0].size(); b++){ 
		
			if (APISuspendNote[a][b].toString() == SourceSuspendNote[a][b].toString()) {
				
				KeywordUtil.markPassed("Value " + APISuspendNote[a][b] +" from API same with Database.")
				
			} else {
				
				KeywordUtil.markFailedAndStop("Value from API = " + APISuspendNote[a][b] + " has different Value from database = " + SourceSuspendNote[a][b])
			}
		}
		
	}
	

	




