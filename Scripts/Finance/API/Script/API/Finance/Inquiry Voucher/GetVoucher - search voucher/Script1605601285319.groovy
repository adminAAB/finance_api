import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable
import com.keyword.API
import com.keyword.GEN5
import groovy.json.JsonSlurper as JsonSlurper

GlobalVariable.VoucherNo = VoucherNo
println (GlobalVariable.VoucherNo)

def VoucherNo = WS.sendRequest(findTestObject('Object Repository/Voucher/GetVoucher'))

if (VoucherNo.getStatusCode() == 200) 
	{ 
		//pengecekkan VoucherNo sukses
		if (API.getResponseData(VoucherNo).status == true && API.getResponseData(VoucherNo).error == '') 
			{
				API.Note('VoucherNo Sukses Input')
				def OutputInquiry = API.getResponseData(VoucherNo)
				API.Note(OutputInquiry.data.items[0].VoucherNo)
					//Compare API Vs Table Voucher,Create Arraylist API
					ArrayList OutputAPI = new ArrayList()
						//Array Compare Voucher ID convert ke type data string
						OutputAPI.add(OutputInquiry.data.items[0].VoucherID.toString())
						API.Note(OutputInquiry.data.items[0].VoucherID)
						
						//Array Compare Voucher Type
						OutputAPI.add(OutputInquiry.data.items[0].VoucherType)
						API.Note(OutputInquiry.data.items[0].VoucherType)
						
						//Array Compare Voucher No
						OutputAPI.add(OutputInquiry.data.items[0].VoucherNo)
						API.Note(OutputInquiry.data.items[0].VoucherNo)
						
						//Array Compare Voucher Type Code
						OutputAPI.add(OutputInquiry.data.items[0].VoucherTypeCode)
						API.Note(OutputInquiry.data.items[0].VoucherTypeCode)
						
						//Array Compare Payment Method
						OutputAPI.add(OutputInquiry.data.items[0].PaymentMethod)
						API.Note(OutputInquiry.data.items[0].PaymentMethod)
						
						//Array Compare Aab Bank Account
						OutputAPI.add(OutputInquiry.data.items[0].AABBankAccount)
						API.Note(OutputInquiry.data.items[0].AABBankAccount)
						
						//Array Compare CurrId
						OutputAPI.add(OutputInquiry.data.items[0].CurrID)
						API.Note(OutputInquiry.data.items[0].CurrID)
						
						//Array Compare Voucher Amount
						OutputAPI.add(OutputInquiry.data.items[0].VoucherAmount)
						API.Note(OutputInquiry.data.items[0].VoucherAmount)
						
						//Array Compare Payer Name
						OutputAPI.add(OutputInquiry.data.items[0].PayerName)
						API.Note(OutputInquiry.data.items[0].PayerName)
						
						//Array Compare Request Date
						OutputAPI.add(OutputInquiry.data.items[0].RequestDate)
						API.Note(OutputInquiry.data.items[0].RequestDate)
						
						//Array Compare Entry User
						OutputAPI.add(OutputInquiry.data.items[0].EntryUser)
						API.Note(OutputInquiry.data.items[0].EntryUser)
						
						//Array Compare Status
						OutputAPI.add(OutputInquiry.data.items[0].Status)
						API.Note(OutputInquiry.data.items[0].Status)
						
						//Array Compare Payment Instrument No
						OutputAPI.add(OutputInquiry.data.items[0].PaymentInstrumentNo)
						API.Note(OutputInquiry.data.items[0].PaymentInstrumentNo)
						
						//Array Compare Settled date
						OutputAPI.add(OutputInquiry.data.items[0].SettledDate)
						API.Note(OutputInquiry.data.items[0].SettledDate)
						
						//Array Compare Remarks
						OutputAPI.add(OutputInquiry.data.items[0].Remarks)
						API.Note(OutputInquiry.data.items[0].Remarks)
						
						//Compare Output API (Array) dan DB		
						ArrayList SourceDataVoucher = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetInquiryVoucher2.replace("'VoucherNo'", "'"+ GlobalVariable.VoucherNo + "'"))
						//Print Atau Cetak variable atas array Output APInya
						API.Note(OutputAPI)
						//Print Atau Cetak output dari databasenya
						API.Note(SourceDataVoucher)
						//Check Type Data Output API
						API.Note (OutputAPI[0].getClass().getSimpleName())
						//Check Type Data Output Database
						API.Note (SourceDataVoucher[0].getClass().getSimpleName())
						//Verifikasi hasil ouput API dan Database diconvert ke type data string
						WebUI.verifyMatch(OutputAPI.size().toString(), SourceDataVoucher.size().toString(), false)
		
								for (a = 0; a < SourceDataVoucher.size(); a++)
										{
					
											if (OutputAPI[a] == SourceDataVoucher[a]) 
												{
			
													KeywordUtil.markPassed("Value " + OutputAPI[a] +" Congratulations, API same with Database.")
			
												} else {
			
															KeywordUtil.markFailedAndStop("Value from API = " + OutputAPI[a] + " Failed,has different Value from database = " + SourceDataVoucher[a])
														}
			
										 }

			} else if (API.getResponseData(VoucherNo).status == false && API.getResponseData(VoucherNo).error != '') 
				{
					API.Note('VoucherNo gagal Input')
		
				} else {
						API.Note("Mohon Maaf Ada Error Di Skenario Anda !")
						}
	} else {
			API.Note("Website Error, Silakan Hub Dev Terkait!!! " + VoucherNo.getStatusCode())
			}





