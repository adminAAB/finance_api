import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil

println (Param)

String filter = ''

if (Skenario == "Inquiry Voucher"){
	
	if (Filter == "VoucherNo"){
		
		filter = "WHERE V.VoucherNo = '" + Param + "'"
		
		GlobalVariable.VoucherNo = Param
		
	} else if (Filter == "PolicyNo"){
	
		filter = "WHERE PolicyNo = '" + Param + "'"
		
		GlobalVariable.PolicyNo = Param
		
	} else if (Filter == "AABAccount"){
	
		filter = "WHERE V.AABAccountID = " + Param
		
		GlobalVariable.AABAccount = Param
		
	} else if (Filter == "PayerName"){
	
		filter = "WHERE C.CustomerCode = '" + Param + "'"
		
		GlobalVariable.PayerId = Param
		
	} else if (Filter == "NoteNo"){
	
		filter = "WHERE FN.NoteNumber = '" + Param + "'"
		
		GlobalVariable.NoteNo = Param
	
	} else if (Filter == "ClaimNo"){
	
		filter = "WHERE FNI.ClaimNo = '" + Param + "'"
		
		GlobalVariable.ClaimNo = Param
		
	} else if (Filter == "GroupingNo"){
	
		filter = "WHERE FNA.GroupingNo = '" + Param + "'"
		
		GlobalVariable.GroupingNo = Param
		
	} else if (Filter == "PINo"){
	
		filter = "WHERE PI.PaymentInstrumentNo = '" + Param + "'"
		
		GlobalVariable.PINo = Param
		
	} else if (Filter == "VoucherType"){
	
		filter = "WHERE V.VoucherTypeID = " + Param
		
		GlobalVariable.VoucherType = Param
	
	} else if (Filter == "VoucherStatus"){
	
		filter = "WHERE V.VoucherStatusID = " + Param
		
		GlobalVariable.VoucherStatus = Param
		
	} else if (Filter == "RequestDate"){
	
		filter = "WHERE CAST(RequestDate AS DATE) >= '" + Param +"' AND CAST(RequestDate AS DATE) <= '" + Param +"'"
		
		GlobalVariable.RequestDateFrom = CustomKeywords.'com.keyword.General.convertdate'(Param)
		
		GlobalVariable.RequestDateTo = CustomKeywords.'com.keyword.General.convertdate'(Param)
			
	} else if (Filter == "EntryUser"){
	
		filter = "WHERE V.CreatedBy = '" + Param +"'"
		
		GlobalVariable.EntryUser = Param
		
	} else if (Filter == "All"){
	
		filter = "WHERE V.VoucherNo = '" + ParamV + "' and PolicyNo = '" + ParamPo + "' and V.AABAccountID = " + ParamA + " and C.CustomerCode = '" + ParamPa + "'"
		
		GlobalVariable.VoucherNo = ParamV
		
		GlobalVariable.PolicyNo = ParamPo
		
		GlobalVariable.AABAccount = ParamA
		
		GlobalVariable.PayerId = ParamPa
		
	}
	
	def getVoucherID = WS.sendRequest(findTestObject('Object Repository/Voucher/GetVoucher'))
	
	def jsonSlurper = new JsonSlurper()
	
	getVoucherID = jsonSlurper.parseText(getVoucherID.getResponseText())
	
	println (getVoucherID)
	
	ArrayList DataGridAPI = new ArrayList()
	
	Integer TotalItems = getVoucherID.data.totalItems
	
	Integer TotalPages = getVoucherID.data.totalPages
	
	println (TotalItems + " : " + TotalPages)
	
	if (TotalItems >= 1 && TotalItems <= 10) {
		
		for (x = 0; x < TotalItems; x++){
		
			ArrayList DataGridAPI0 = new ArrayList()
					
			DataGridAPI0.add(getVoucherID.data.items[x].VoucherType)
			
			DataGridAPI0.add(getVoucherID.data.items[x].VoucherNo)
			
			DataGridAPI0.add(getVoucherID.data.items[x].PaymentMethod)
			
			DataGridAPI0.add(getVoucherID.data.items[x].AABBankAccount)
			
			DataGridAPI0.add(getVoucherID.data.items[x].CurrID)
			
			DataGridAPI0.add(getVoucherID.data.items[x].VoucherAmount)
			
			DataGridAPI0.add(getVoucherID.data.items[x].PayerName)
			
			DataGridAPI0.add(getVoucherID.data.items[x].RequestDate)
			
			DataGridAPI0.add(getVoucherID.data.items[x].EntryUser)
			
			DataGridAPI0.add(getVoucherID.data.items[x].Status)
			
			DataGridAPI0.add(getVoucherID.data.items[x].PaymentInstrumentNo)
			
			DataGridAPI0.add(getVoucherID.data.items[x].SettledDate)
			
			DataGridAPI0.add(getVoucherID.data.items[x].Remarks)
			
			DataGridAPI.add(DataGridAPI0)
			
		}
		
		ArrayList SourceDataVoucher = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetInquiryVoucher.replace("WHERE ", filter))
		
		println (GlobalVariable.FIN_GetInquiryVoucher.replace("WHERE ", filter))
		
		for (ii = 0; ii < DataGridAPI.size(); ii++){
					
			if (DataGridAPI[ii][12] != null || DataGridAPI[ii][12].toString().length() > 0){ // akomodir yg null
				
				DataGridAPI[ii][12] = DataGridAPI[ii][12].toString().trim() // replace string kosong , krn getdata ketika query di trim
	
			}
			
		}
		
		println (DataGridAPI)
		
		println (SourceDataVoucher)
		
		WebUI.verifyMatch(DataGridAPI.size().toString(), SourceDataVoucher.size().toString(), false)
		
		for (a = 0; a < SourceDataVoucher.size(); a++){
			
			for (b = 1; b < SourceDataVoucher[0].size(); b++){ //tambahan data voucherID untuk sorting
				
				if (DataGridAPI[a][b-1] == SourceDataVoucher[a][b]) {//tambahan data voucherID untuk sorting
					
					KeywordUtil.markPassed("Value " + DataGridAPI[a][b-1] +" from API same with Database.")
					
				} else {
					
					KeywordUtil.markFailedAndStop("Value from API = " + DataGridAPI[a][b-1] + " has different Value from database = " + SourceDataVoucher[a][b])
				}
			}
			
		}
		
	} else if (TotalItems >= 10){ // untuk yg data banyak hanya compare by no voucher dan countdata
	
		ArrayList DataGridAPI0 = new ArrayList()
	
		for (b = 0; b < TotalPages; b++){
			
			GlobalVariable.PageNo = b + 1
			
			getVoucherID = WS.sendRequest(findTestObject('Object Repository/Voucher/GetVoucher'))
			
			def parsedJson = new JsonSlurper()
			
			getVoucherID = parsedJson.parseText(getVoucherID.getResponseText())
			
			Integer currentPage = getVoucherID.data.currentPage
			
			Integer itemsPerPage = getVoucherID.data.itemsPerPage
			
			println (currentPage + " - " +  TotalPages)
			
			if (currentPage < TotalPages){
				
				for (x = 0; x < itemsPerPage; x++){
												
					DataGridAPI0.add(getVoucherID.data.items[x].VoucherNo)
						
				}
				
			} else { // untuk get data last page , jumlah itemnya tidak full 10
			
				Integer itemsLastPage = TotalItems - (itemsPerPage*(currentPage-1))
				
				println (itemsLastPage)
			
				for (x = 0; x < itemsLastPage; x++){
								
					DataGridAPI0.add(getVoucherID.data.items[x].VoucherNo)
						
				}
			
			}		
		
		
			
		}
		
		DataGridAPI.add(DataGridAPI0)
		
		ArrayList SourceDataVoucher = GEN5.getOneColumnDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetInquiryVoucher.replace("WHERE ", filter),'VoucherNo')
		
		println (DataGridAPI[0])
		
		println (SourceDataVoucher)
		
		WebUI.verifyMatch(DataGridAPI[0].size().toString(), SourceDataVoucher.size().toString(), false)
			
		for (b = 0; b < SourceDataVoucher.size(); b++){
						
			if (DataGridAPI[0][b] == SourceDataVoucher[b]) {
				
				KeywordUtil.markPassed("Value " + DataGridAPI[0][b] +" from API same with Database.")
				
			} else {
				
				KeywordUtil.markFailedAndStop("Value from API = " + DataGridAPI[0][b] + " has different Value from database = " + SourceDataVoucher[b])
			}
				
		}
		
	} else if (TotalItems == 0 && getVoucherID.status == true){
		
		KeywordUtil.markPassed("Value " + TotalItems +" from API same with Database.")
		
	}
	
	//reset param globalvariable
	GlobalVariable.VoucherNo = ''
	
	GlobalVariable.PolicyNo = ''
	
	GlobalVariable.AABAccount = 0
	
	GlobalVariable.PayerId = ''
	
	GlobalVariable.NoteNo = ''
	
	GlobalVariable.ClaimNo = ''
	
	GlobalVariable.GroupingNo = ''
	
	GlobalVariable.PINo = ''
	
	GlobalVariable.VoucherType = 0
	
	GlobalVariable.VoucherStatus = 0
	
	GlobalVariable.RequestDateFrom = ''
	
	GlobalVariable.RequestDateTo = ''
	
	GlobalVariable.EntryUser = ''
	
	GlobalVariable.PageNo = 1
	
}





