import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
import java.text.DecimalFormat
import java.text.NumberFormat
import java.math.RoundingMode

DecimalFormat df = new DecimalFormat("#.####")

DecimalFormat de = new DecimalFormat("#.##")

DecimalFormat dg = new DecimalFormat("#.###")

df.setRoundingMode(RoundingMode.HALF_UP);

de.setRoundingMode(RoundingMode.HALF_UP);

dg.setRoundingMode(RoundingMode.DOWN);

if (Proses == "GetNoteData"){
	
	String Query = ""

	Query = "CAST(RequestAmount as decimal (20,4)) "
			
	GlobalVariable.GetDataCompareToVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataTaxCompareToVoucherDetail.replace("GVNoteNumber",GlobalVariable.ParamNoteNumber.toString().replace('"', "'")).replace("'GVCashAmount'",Query).replace("'0' IsFullPayment","'1' IsFullPayment"))
	
	println (GlobalVariable.GetDataCompareToVD)

} else if (Proses == "CreateVoucher.RO"){

	/* untuk cek pembentukan data FinanceNotes */
	
	println (GlobalVariable.FIN_DataSuspendCategoryEconsul)
	
	println (GlobalVariable.FIN_GetDataCompareToFinanceNotesSuspend)
	
	println (GlobalVariable.EConsulAutomaticVoucherRO)
			
	ArrayList SourceDataFN = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesSuspend.replace("GVVoucherNo",GlobalVariable.VoucherNo))
					
	def OurRefNo = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetAABAccount.replace("param","aabaccountcode ='" + GlobalVariable.EConsulAutomaticVoucherRO[1] + "'"))
	
	SourceDataFN[0].set(6 , GlobalVariable.EConsulAutomaticVoucherRO[15]) //set nominal
	
	SourceDataFN[0].set(8 , "0") //set IsDebtor	
	
	SourceDataFN[0].set(9 , "1") //set IsCreditor
					
	SourceDataFN[0].set(12 , GlobalVariable.FIN_DataSuspendCategoryEconsul[0]) //set TpJournal
	
	SourceDataFN[0].set(13 , GlobalVariable.EConsulAutomaticVoucherRO[21]) //set Remarks
	
	SourceDataFN[0].set(24 , OurRefNo[4]) //set OurRefNo berisi accountno dari aabaccountcode
					
	SourceDataFN[0].set(39 , GlobalVariable.FIN_DataSuspendCategoryEconsul[1])
	
	SourceDataFN[0].set(46 , GlobalVariable.username[0]) //set CreatedBy
		
	ArrayList APIDataFN = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNotesSuspend.replace("GVVoucherNo", GlobalVariable.VoucherNo))
	
	println (SourceDataFN[0].size() +  " - " + APIDataFN[0].size())
	
	println (SourceDataFN)
	
	println (APIDataFN)
	
	WebUI.verifyMatch(APIDataFN[0].size().toString(), SourceDataFN[0].size().toString(), false)
	
	WebUI.verifyMatch(APIDataFN.size().toString(), SourceDataFN.size().toString(), false)
	
	for (j = 0; j < SourceDataFN.size(); j++){
		
		for (k = 0; k < SourceDataFN[0].size(); k++){
					
			if ((APIDataFN[j])[k] == (SourceDataFN[j])[k]) {
					
				KeywordUtil.markPassed("Value " + (APIDataFN[j])[k] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFN[j])[k] + " has different Value from database = " + (SourceDataFN[j])[k])
			}
				
		}
			
	}

	/* untuk cek pembentukan data FinanceSettlements */
	
	ArrayList APIDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceSettlements.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataFS)
	
	if (APIDataFS == null) {
	
		KeywordUtil.markPassed("Value " + APIDataFS +" from API same with Database.")
	
	} else {
		
		KeywordUtil.markFailedAndStop("Value from API = " + APIDataFS + " has different Value from database = null")
	}
	
	/* untuk cek pembentukan data SuspendMonitoring */
	
	ArrayList SourceDataSM = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToSuspendMonitoring.replace("GVSuspendResultCategoryCode", GlobalVariable.FIN_DataSuspendCategoryEconsul[2]))
			
	SourceDataSM[0][16] = GlobalVariable.EConsulAutomaticVoucherRO[21] //set remarks
	
	SourceDataSM[0][17] = GlobalVariable.username[GlobalVariable.username.size()-1] //set createdby
	
	ArrayList APIDataSM = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetSuspendMonitoring.replace("GVVoucherNo",GlobalVariable.VoucherNo))
			
	println (SourceDataSM[0].size() +  " - " + APIDataSM[0].size())
			
	println (SourceDataSM)
			
	println (APIDataSM)
			
	WebUI.verifyMatch(APIDataSM.size().toString(), SourceDataSM.size().toString(), false)
			
	WebUI.verifyMatch(APIDataSM[0].size().toString(), SourceDataSM[0].size().toString(), false)
			
	for (a = 0; a < SourceDataSM.size(); a++){
				
		for (b = 0; b < SourceDataSM[0].size(); b++){
							
			if ((APIDataSM[a])[b] == (SourceDataSM[a])[b]) {
							
				KeywordUtil.markPassed("Value " + (APIDataSM[a])[b] +" from API same with Database.")
							
			} else {
							
				KeywordUtil.markFailedAndStop("Value from API = " + (APIDataSM[a])[b] + " has different Value from database = " + (SourceDataSM[a])[b])
				
			}
						
		}
					
	}
	
	/* untuk cek pembentukan data Voucher */
	
	ArrayList SourceDataV = new ArrayList()
	
	println (GlobalVariable.FIN_GetDataCompareToVoucher.replace("'GVAABAccountID'", "(select aabaccountid from general.aabaccount where aabaccountcode ='"+ GlobalVariable.EConsulAutomaticVoucherRO[1] +"')"))
	
	SourceDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVoucher.replace("'GVAABAccountID'", "(select aabaccountid from general.aabaccount where aabaccountcode ='"+ GlobalVariable.EConsulAutomaticVoucherRO[1] +"')"))
	
	println (GlobalVariable.FIN_PaymentMethod.replace("GVpaymentmethodecode", GlobalVariable.EConsulAutomaticVoucherRO[5]))
	
	def PaymentMethod = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_PaymentMethod.replace("GVpaymentmethodecode", GlobalVariable.EConsulAutomaticVoucherRO[5]))
	 
	GlobalVariable.CdEntity = GEN5.getValueDatabase("172.16.94.70", "BeyondDB_Apr2020", GlobalVariable.BEY_GetCdEntity.replace("GVlogid",GlobalVariable.username[1]),"cdEntity")
	
	ArrayList APIDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	SourceDataV[2] = '1' //set IsGeneratedBySUNGL
	
	SourceDataV[3] = '9' //set VoucherTypeID	

	SourceDataV[5] = '3' //set ExpectedVoucherStatusID
	
	SourceDataV[6] = '13' //set VoucherCreatedType
	
	SourceDataV[10] = PaymentMethod[0] //set PaymentMethod
	
	SourceDataV[12] = GlobalVariable.EConsulAutomaticVoucherRO[21] //set Remark
			
	SourceDataV[13] = GlobalVariable.EConsulAutomaticVoucherRO[15].toString().replace(".0000",".00") //set Nominal
	
	SourceDataV[14] = '0.00' //set Diff
	
	SourceDataV[15] = '1' //set IsDebtor
	
	SourceDataV[16] = '0' //set IsCreditor
	
	SourceDataV[15] = '1' //set IsDebtor
	
	SourceDataV[16] = '0' //set IsCreditor
	
	SourceDataV[25] = GlobalVariable.EConsulAutomaticVoucherRO[4] //set SecondaryPayerBankBranch
	
	SourceDataV[27] = GlobalVariable.EConsulAutomaticVoucherRO[2] //set SecondaryPayerAccountNo
		
	SourceDataV[28] = GlobalVariable.EConsulAutomaticVoucherRO[3] //set SecondaryPayerAccountHolder
	
	SourceDataV[29] = GlobalVariable.EConsulAutomaticVoucherRO[4] //set SecondaryBankAddress
	
	SourceDataV[38] = '0' //set DirectorateID
	
	SourceDataV[45] = GlobalVariable.username[0] //set CreatedBy
	
	SourceDataV[47] = GlobalVariable.username[GlobalVariable.username.size()-1] //set CreatedBy
	
	SourceDataV[51] = '' //set EntryUserOrganizationEntityCode
					
	println (APIDataV)
	
	println (SourceDataV)
	
	println (APIDataV.size().toString() +" - "+  SourceDataV.size().toString())
	
	WebUI.verifyMatch(APIDataV.size().toString(), SourceDataV.size().toString(), false)
	
	for (c = 0; c < SourceDataV.size(); c++){
		
		if (APIDataV[c] == SourceDataV[c]) {
			
			KeywordUtil.markPassed("Value " + APIDataV[c] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataV[c] + " has different Value from database = " + SourceDataV[c])
		}
	}
	
	/* untuk cek pembentukan data VoucherDetail */
	
	ArrayList APIDataVD = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherDetail.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	ArrayList SourceDataVD = new ArrayList()
	
	SourceDataVD = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVoucherDetailOthers.replace("GVSubJournalTypeCode",GlobalVariable.FIN_DataSuspendCategoryEconsul[1]).replace("GVJournalTypeCode",GlobalVariable.FIN_DataSuspendCategoryEconsul[0]).replace("GVTaxTypeCode","").replace("GVNominal",GlobalVariable.FIN_DataSuspendCategoryEconsul[3]).replace("GVSuspendResultCategoryCode",GlobalVariable.FIN_DataSuspendCategoryEconsul[2]))
	
	SourceDataVD[4] = "1" // set IsDebtor
	
	SourceDataVD[5] = "0" // set IsCreditor
	
	SourceDataVD[6] = GlobalVariable.EConsulAutomaticVoucherRO[15] // set OutstandingAmount
		
	SourceDataVD[27] = GlobalVariable.EConsulAutomaticVoucherRO[21] //set remark
	
	SourceDataVD[37] = GlobalVariable.username[0] //set CreatedBy
	
	println (APIDataVD)
	
	println (SourceDataVD)
	
	println (APIDataVD.size().toString() +" - "+  SourceDataVD.size().toString())
	
	WebUI.verifyMatch(APIDataV.size().toString(), SourceDataV.size().toString(), false)
	
	for (f = 0; f < SourceDataVD.size(); f++){
					
		if (APIDataVD[f] == SourceDataVD[f]) {
					
			KeywordUtil.markPassed("Value " + APIDataVD[f] +" from API same with Database.")
					
		} else {
					
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataVD[f] + " has different Value from database = " + SourceDataVD[f])
		}
		
	}
	
	/* untuk cek pembentukan data vouchersettlementbackhistory */
	
	ArrayList APIDataVSH = new ArrayList()
	
	APIDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherSettlementBackHistory.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataVSH)
	
	if (APIDataVSH == null) {
	
		KeywordUtil.markPassed("Value " + APIDataVSH +" from API same with Database.")
	
	} else {
		
		KeywordUtil.markFailedAndStop("Value from API = " + APIDataVSH + " has different Value from database = null")
	}
	
} else if (Proses == "DropVoucher"){

	/* untuk cek pembentukan data FinanceSettlements */
	
	ArrayList APIDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceSettlements.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataFS)
	
	if (APIDataFS == null) {
	
		KeywordUtil.markPassed("Value " + APIDataFS +" from API same with Database.")
	
	} else {
	
		KeywordUtil.markFailedAndStop("Value from API = " + APIDataFS + " has different Value from database = null")
	}

	/* untuk cek pembentukan data Voucher */
	
	ArrayList SourceDataV = GlobalVariable.ParamList
	
	SourceDataV[6] = '9'
	
	SourceDataV[50] = GlobalVariable.username[GlobalVariable.username.size()-1]

	ArrayList APIDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", FIN_GetDataVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataV)
	
	println (SourceDataV)
	
	println (APIDataV.size().toString() +" - "+  SourceDataV.size().toString())
	
	WebUI.verifyMatch(APIDataV.size().toString(), SourceDataV.size().toString(), false)
	
	for (c = 0; c < SourceDataV.size(); c++){
	
		if (APIDataV[c] == SourceDataV[c]) {
			
			KeywordUtil.markPassed("Value " + APIDataV[c] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataV[c] + " has different Value from database = " + SourceDataV[c])
		}
	}
	
	/* untuk cek pembentukan data vouchersettlementbackhistory */
	
	ArrayList APIDataVSH = new ArrayList()
	
	APIDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherSettlementBackHistory.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataVSH)
	
	if (APIDataVSH == null) {
	
		KeywordUtil.markPassed("Value " + APIDataVSH +" from API same with Database.")
	
	} else {
		
		KeywordUtil.markFailedAndStop("Value from API = " + APIDataVSH + " has different Value from database = null")
	}
	
} else if (Proses == "RejectVoucher"){

	/* untuk cek pembentukan data FinanceSettlements */
	
	ArrayList APIDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceSettlements.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataFS)
	
	if (APIDataFS == null) {
	
		KeywordUtil.markPassed("Value " + APIDataFS +" from API same with Database.")
	
	} else {
	
		KeywordUtil.markFailedAndStop("Value from API = " + APIDataFS + " has different Value from database = null")
	}
	
	/* untuk cek pembentukan data Voucher */
	
	GlobalVariable.ParamList[51] = 'null'
	
	ArrayList SourceDataV = GlobalVariable.ParamList
	
	ArrayList APIDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", FIN_GetDataVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataV)
	
	println (SourceDataV)
	
	println (APIDataV.size().toString() +" - "+  SourceDataV.size().toString())
	
	WebUI.verifyMatch(APIDataV.size().toString(), SourceDataV.size().toString(), false)
	
	for (c = 0; c < SourceDataV.size(); c++){
	
		if (APIDataV[c] == SourceDataV[c]) {
			
			KeywordUtil.markPassed("Value " + APIDataV[c] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataV[c] + " has different Value from database = " + SourceDataV[c])
		}
	}
	
	/* untuk cek pembentukan data vouchersettlementbackhistory */
	
	ArrayList APIDataVSH = new ArrayList()
	
	APIDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherSettlementBackHistory.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataVSH)
	
	if (APIDataVSH == null) {
	
		KeywordUtil.markPassed("Value " + APIDataVSH +" from API same with Database.")
	
	} else {
		
		KeywordUtil.markFailedAndStop("Value from API = " + APIDataVSH + " has different Value from database = null")
	}

}
		
