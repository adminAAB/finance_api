import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.text.DecimalFormat
import java.text.NumberFormat
import java.math.RoundingMode
	
	
DecimalFormat df = new DecimalFormat("#.####")
	
DecimalFormat de = new DecimalFormat("#.##")
	
df.setRoundingMode(RoundingMode.HALF_UP);
	
de.setRoundingMode(RoundingMode.HALF_UP);

GlobalVariable.VoucherNo = GlobalVariable.ListVoucher[0]

GlobalVariable.VoucherNoCA = GlobalVariable.ListVoucher[1]
		
/* untuk cek pembentukan data VoucherDetail */
							
ArrayList SourceDataVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherDetail.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
println (GlobalVariable.VoucherNo + " : " + GlobalVariable.VoucherNoCA)

ArrayList APIDataVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherDetail.replace("GVvoucherno",GlobalVariable.VoucherNoCA))
		
for (h = 0; h < SourceDataVD.size(); h++){
						
	// untuk update status isdebtor, iscreditor karena nilainya kebalikan dari VD voucher RC
	if (SourceDataVD[h][4] == '1'){
				
		SourceDataVD[h][4] = '0'
				
		SourceDataVD[h][5] = '1'
				 
	} else {
			
		SourceDataVD[h][4] = '1'
				
		SourceDataVD[h][5] = '0'
			
	}
						
	SourceDataVD[h][37] = GlobalVariable.username[GlobalVariable.username.size()-1] // set CreatedBy
	
	SourceDataVD[h][28] = GlobalVariable.VoucherNo // set VoucherReference

}
				
println (APIDataVD)
		
println (SourceDataVD)
		
println (APIDataVD.size().toString() +" - "+  SourceDataVD.size().toString())
		
WebUI.verifyMatch(APIDataVD.size().toString(), SourceDataVD.size().toString(), false)
		
for (f = 0; f < SourceDataVD.size(); f++){
			
	for (g = 0; g < SourceDataVD[0].size(); g++){
						
		if ((APIDataVD[f])[g] == (SourceDataVD[f])[g]) {
						
			KeywordUtil.markPassed("Value " + (APIDataVD[f])[g] +" from API same with Database.")
						
		} else {
						
			KeywordUtil.markFailedAndStop("Value from API = " + (APIDataVD[f])[g] + " has different Value from database = " + (SourceDataVD[f])[g])
		}
					
	}
				
}
		
/* untuk cek pembentukan data Voucher */
			
ArrayList SourceDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo).replace("FORMAT(SettlementDate","FORMAT(GetDate()"))
		
ArrayList APIDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucher.replace("GVvoucherno",GlobalVariable.VoucherNoCA))
		
if (GlobalVariable.VoucherTypeCodeBefore == 'PS'){
			
	SourceDataV[15] = '1' //set IsDebtor
			
	SourceDataV[16] = '0' //set IsCreditor		
			
} else if (GlobalVariable.VoucherTypeCodeBefore == 'RO'){
		
	SourceDataV[15] = '0' //set IsDebtor
			
	SourceDataV[16] = '1' //set IsCreditor						
						
} else if (GlobalVariable.VoucherTypeCodeBefore == 'PO'){
		
	SourceDataV[15] = '0' //set IsDebtor
			
	SourceDataV[16] = '1' //set IsCreditor						

} else if (GlobalVariable.VoucherTypeCodeBefore == 'PY'){

	SourceDataV[10] = '0' //set PaymentMethodID
	
	SourceDataV[15] = '1' //set IsDebtor
	
	SourceDataV[16] = '0' //set IsCreditor

} else if (GlobalVariable.VoucherTypeCodeBefore == 'RC'){
		
	SourceDataV[10] = '0' //set PaymentMethodID
		
	SourceDataV[15] = '0' //set IsDebtor
			
	SourceDataV[16] = '1' //set IsCreditor	
						
} else if (GlobalVariable.VoucherTypeCodeBefore == 'W1'){
		
	SourceDataV[15] = '1' //set IsDebtor
			
	SourceDataV[16] = '0' //set IsCreditor						
						
} else if (GlobalVariable.VoucherTypeCodeBefore == 'W2'){
							
	SourceDataV[15] = '1' //set IsDebtor
			
	SourceDataV[16] = '0' //set IsCreditor
			
}
						
SourceDataV[3] = '4' //set VoucherTypeID
		
SourceDataV[5] = '0' //set ExpectedVoucherStatusID
		
SourceDataV[12] = GlobalVariable.Remarks //set Remark

println (GlobalVariable.username)
	
SourceDataV[45] = GlobalVariable.username[GlobalVariable.username.size()-1] // set CreatedBy

GlobalVariable.CdEntity = GEN5.getValueDatabase("172.16.94.70", "BeyondDB_Apr2020", GlobalVariable.BEY_GetCdEntity.replace("GVlogid",GlobalVariable.username[GlobalVariable.username.size()-1]),"cdEntity")

SourceDataV[51] = GlobalVariable.CdEntity //set EntryUserOrganizationEntityCode
					
println (APIDataV)
		
println (SourceDataV)
		
println (APIDataV.size().toString() +" - "+  SourceDataV.size().toString())
		
WebUI.verifyMatch(APIDataV.size().toString(), SourceDataV.size().toString(), false)
		
for (c = 0; c < SourceDataV.size(); c++){
		
	if (APIDataV[c] == SourceDataV[c]) {
				
		KeywordUtil.markPassed("Value " + APIDataV[c] +" from API same with Database.")
				
	} else {
				
		KeywordUtil.markFailedAndStop("Value from API = " + APIDataV[c] + " has different Value from database = " + SourceDataV[c])
	}
}
		
/* untuk cek pembentukan data FinanceNoteAdditional */
		
ArrayList SourceDataFNA = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNoteAdditional.replace("GVvoucherno",GlobalVariable.VoucherNo))
				
for (i = 0; i < SourceDataFNA.size(); i++){
		
	SourceDataFNA[i][0] = '0.0000'
			
	SourceDataFNA[i][1] = '0'
		
}
		
ArrayList APIDataFNA = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNoteAdditional.replace("GVvoucherno",GlobalVariable.VoucherNoCA))
				
println (APIDataFNA)
		
println (SourceDataFNA)
		
WebUI.verifyMatch(APIDataFNA.size().toString(), SourceDataFNA.size().toString(), false)
		
for (j = 0; j < SourceDataFNA.size(); j++){
			
	for (k = 0; k < SourceDataFNA[0].size(); k++){
						
		if ((APIDataFNA[j])[k] == (SourceDataFNA[j])[k]) {
						
			KeywordUtil.markPassed("Value " + (APIDataFNA[j])[k] +" from API same with Database.")
						
		} else {
						
			KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFNA[j])[k] + " has different Value from database = " + (SourceDataFNA[j])[k])
		}
					
	}
				
}
		
/* untuk cek pembentukan data nota SST dan CMT */
		
ArrayList APIDataSSTCMT = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNotesSSTCMT.replace("GVVoucherNo",GlobalVariable.VoucherNoCA))
		
ArrayList SourceDataCMT = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesCMT.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
if (SourceDataCMT != null){
			
	ArrayList SourceDataSSTPPH = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesSSTPPH.replace("GVvoucherno",GlobalVariable.VoucherNo))
			
	ArrayList SourceDataSSTPPN = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesSSTPPN.replace("GVvoucherno",GlobalVariable.VoucherNo))
			
	ArrayList SourceDataSSTCMT = new ArrayList()
				
	// add array nota CMT
	for (T = 0; T < SourceDataCMT.size(); T++){
					
		SourceDataSSTCMT.add(SourceDataCMT[T])
					
	}		
			
	if (SourceDataSSTPPH != null){
				
		// add array nota SST PPH
		for (H = 0; H < SourceDataSSTPPH.size(); H++){
					
			SourceDataSSTCMT.add(SourceDataSSTPPH[H])
					
		}
	}
			
	if (SourceDataSSTPPN != null){
				
		// add array nota SST PPN
		for (N = 0; N < SourceDataSSTPPN.size(); N++){
					
			SourceDataSSTCMT.add(SourceDataSSTPPN[N])
					
		}
	}
			
	// update isdebtor iscreditor kebalikan dari hasil RC/PY
	for (o = 0; o < SourceDataSSTCMT.size(); o++){
				
		if (SourceDataSSTCMT[o][12] == '1'){
					
			SourceDataSSTCMT[o][12] = '0'
					
			SourceDataSSTCMT[o][13] = '1'
					
		} else {
				
			SourceDataSSTCMT[o][12] = '1'
					
			SourceDataSSTCMT[o][13] = '0'
		}
				
	}
			
	println (APIDataSSTCMT)
			
	println (SourceDataSSTCMT)
			
	WebUI.verifyMatch(APIDataSSTCMT.size().toString(), SourceDataSSTCMT.size().toString(), false)
			
			for (ij = 0; ij < SourceDataSSTCMT.size(); ij++){
				
				for (ik = 0; ik < SourceDataSSTCMT[0].size(); ik++){
							
					if ((APIDataSSTCMT[ij])[ik] == (SourceDataSSTCMT[ij])[ik]) {
							
						KeywordUtil.markPassed("Value " + (APIDataSSTCMT[ij])[ik] +" from API same with Database.")
							
					} else {
							
						KeywordUtil.markFailedAndStop("Value from API = " + (APIDataSSTCMT[ij])[ik] + " has different Value from database = " + (SourceDataSSTCMT[ij])[ik])
					}
						
				}
					
			}
		}
		
		/* untuk cek pembentukan data FinanceSettlements */
			
		ArrayList SourceDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceSettlements.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
		ArrayList APIDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceSettlements.replace("GVvoucherno",GlobalVariable.VoucherNoCA))
					
		// untuk update status isdebtor, iscreditor karena nilainya kebalikan dari nota
		for (hi = 0; hi < SourceDataFS.size(); hi++){
			
			if (SourceDataFS[hi][6] == '1'){
				
				SourceDataFS[hi][6] = '0'
				
				SourceDataFS[hi][7] = '1'
				 
			} else {
			
				SourceDataFS[hi][6] = '1'
				
				SourceDataFS[hi][7] = '0'
			
			}
			
			for (hh = 0; hh < GlobalVariable.FinanceNoteID.size(); hh++){
				
				if (GlobalVariable.Journal[hh] == 'COM'){
								
					SourceDataFS[hh][3] = SourceDataVD[hh][30]
				}
			}

		}
		
		println (APIDataFS)
		
		println (SourceDataFS)
		
		WebUI.verifyMatch(APIDataFS.size().toString(), SourceDataFS.size().toString(), false)
		
		for (d = 0; d < SourceDataFS.size(); d++){
			
			for (e = 0; e < SourceDataFS[0].size(); e++){
						
				if ((APIDataFS[d])[e] == (SourceDataFS[d])[e]) {
						
					KeywordUtil.markPassed("Value " + (APIDataFS[d])[e] +" from API same with Database.")
						
				} else {
						
					KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFS[d])[e] + " has different Value from database = " + (SourceDataFS[d])[e])
				}
					
			}
				
		}
		
		/* untuk cek pembentukan data vouchersettlementbackhistory */
			
		ArrayList SourceDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherSettlementBackHistory.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
		SourceDataVSH[1] = '4' //set vouchertypeid
		
		ArrayList APIDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherSettlementBackHistory.replace("GVvoucherno",GlobalVariable.VoucherNoCA))
		
		println (APIDataVSH)
		
		println (SourceDataVSH)
		
		println (APIDataVSH.size().toString() +" - "+  SourceDataVSH.size().toString())
		
		WebUI.verifyMatch(APIDataVSH.size().toString(), SourceDataVSH.size().toString(), false)
		
		for (k = 0; k < SourceDataVSH.size(); k++){
			
			if (APIDataVSH[k] == SourceDataVSH[k]) {
				
				KeywordUtil.markPassed("Value " + APIDataVSH[k] +" from API same with Database.")
				
			} else {
				
				KeywordUtil.markFailedAndStop("Value from API = " + APIDataVSH[k] + " has different Value from database = " + SourceDataVSH[k])
			}
		}
		

