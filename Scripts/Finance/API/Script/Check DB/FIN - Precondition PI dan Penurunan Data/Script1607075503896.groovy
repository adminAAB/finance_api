import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI
import com.keyword.GEN5

if (Proses == 'Initial Select FIN'){
	
	//******************** Select Finance.Voucher ********************//
	
	def queryVoucher = ("select top 1 VoucherNo from Finance.Voucher where VoucherID in "
		+"(select VoucherID from Finance.VoucherDetail where FinanceNoteID in "
		+"(select FinanceNoteID from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"'))")
	
	def StrVoucherNo = UI.getValueDatabase('172.16.94.145', 'a2isFinanceDB', queryVoucher, 'VoucherNo')
	
	println (StrVoucherNo)
	
	GlobalVariable.VoucherNo=StrVoucherNo
	
	println (GlobalVariable.VoucherNo)

	//******************** Select Finance.PaymentInstrument ********************//

	def queryPaymentInstrumentNo = ("select PaymentInstrumentNo from Finance.PaymentInstrument where PaymentInstrumentID in "
		+"(select PaymentInstrumentID from Finance.PaymentInstrumentDetail where VoucherNo='"+ GlobalVariable.VoucherNo +"')")
	
	def StrPaymentInstrumentNo = UI.getValueDatabase('172.16.94.145', 'a2isFinanceDB', queryPaymentInstrumentNo, 'PaymentInstrumentNo')
	
	println (StrPaymentInstrumentNo)
	
	GlobalVariable.PaymentInstrumentNo=StrPaymentInstrumentNo
	
	println (GlobalVariable.PaymentInstrumentNo)
	
} else if (Proses == 'Initial Delete FIN'){

	UI.connectDB('172.16.94.145', 'a2isFinanceDB')

	UI.execute("delete from Finance.FinanceSettlements where voucherno='"+ GlobalVariable.VoucherNo +"'")
	
	UI.execute("delete Finance.VoucherDetail where voucherid in (select voucherid from finance.voucher where voucherno='"+ GlobalVariable.VoucherNo +"')")
	
	UI.execute("delete from Finance.PaymentInstrumentDetail where voucherno='"+ GlobalVariable.VoucherNo +"'")
	
	UI.execute("delete from Finance.PaymentInstrument where paymentinstrumentno='"+ GlobalVariable.PaymentInstrumentNo +"'")
	
	UI.execute("delete Finance.Voucher where VoucherNo='"+ GlobalVariable.VoucherNo +"'")
	
} else if (Proses == 'Initial Delete FIN - PenurunanNota'){

	UI.connectDB('172.16.94.145', 'a2isFinanceDB')

	UI.execute("delete from Finance.FinanceSettlements where voucherno='"+ GlobalVariable.VoucherNo +"'")
	
	UI.execute("delete Finance.VoucherDetail where voucherid in (select voucherid from finance.voucher where voucherno='"+ GlobalVariable.VoucherNo +"')")
	
	UI.execute("delete Finance.Voucher where VoucherNo='"+ GlobalVariable.VoucherNo +"'")
	
} else if (Proses == 'Initial Delete FIN.VoucherRO'){

	UI.connectDB('172.16.94.145', 'a2isFinanceDB')
	
	UI.execute("delete from Finance.SuspendMonitoring where FinanceNoteID in (select FinanceNoteID from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"')")
	
	UI.execute("delete from Finance.FinanceNoteAdditional where NoteNumber='"+NoteNumber+"'")
	
	UI.execute("delete from Finance.FinanceNoteInfos where FinanceNoteID in (select FinanceNoteID from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"')")
	
	UI.execute("delete from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"'")
	
	UI.execute("delete Finance.VoucherDetail where voucherid in (select voucherid from finance.voucher where voucherno='"+ GlobalVariable.VoucherNo +"')")
	
	UI.execute("delete Finance.Voucher where VoucherNo='"+ GlobalVariable.VoucherNo +"'")	
	
} else if (Proses == 'Initial Delete FIN.Note'){

	UI.connectDB('172.16.94.145', 'a2isFinanceDB')
		
	UI.execute("delete from Finance.FinanceNoteAdditional where NoteNumber='"+NoteNumber+"'")
	
	UI.execute("delete from Finance.FinanceNoteInfos where FinanceNoteID in (select FinanceNoteID from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"')")
	
	UI.execute("delete from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"'")
	
} else if (Proses == 'Initial Delete FIN.NoteSuspend'){

	UI.connectDB('172.16.94.145', 'a2isFinanceDB')
	
	UI.execute("delete from Finance.SuspendMonitoring where FinanceNoteID in (select FinanceNoteID from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"')")
		
	UI.execute("delete from Finance.FinanceNoteAdditional where NoteNumber='"+NoteNumber+"'")
	
	UI.execute("delete from Finance.FinanceNoteInfos where FinanceNoteID in (select FinanceNoteID from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"')")
	
	UI.execute("delete from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"'")

} else if (Proses == 'Initial Update FIN'){

	UI.connectDB('172.16.94.145', 'a2isFinanceDB')
	
	println ("update Finance.FinanceNoteAdditional set paidamount="+PaidAmount+", isfinalsettled="+IsFinalSettled+" where NoteNumber='"+NoteNumber+"'")

	UI.execute("update Finance.FinanceNoteAdditional set paidamount="+PaidAmount+", isfinalsettled="+IsFinalSettled+" where NoteNumber='"+NoteNumber+"'")

} else if (Proses == 'Update RowStatus FIN'){

	UI.connectDB('172.16.94.145', 'a2isFinanceDB')

	UI.execute("update Finance.FinanceNotes set rowstatus="+RowStatus+" where NoteNumber='"+NoteNumber+"'")

} else if (Proses == 'Initial Delete SEA'){

	//******************** Delete pVoucher ********************//

	UI.connectDB('172.16.94.70', 'SEA')
	
	UI.execute("delete pVoucher where Voucher='"+ GlobalVariable.VoucherNo +"'")
	
	//******************** Delete fVoucher ********************//

	UI.execute("delete fVoucher where Voucher='"+ GlobalVariable.VoucherNo +"'")
	
	//******************** Delete fSL ********************//
	
	UI.execute("delete fSL where Voucher='"+ GlobalVariable.VoucherNo +"'")
	
} else if (Proses == 'Initial Update SEA'){
	
	//******************** Update nVoucher ********************//

	UI.connectDB('172.16.94.70', 'SEA')
	
	UI.execute("update nVoucher set SettledStatus=0, Payment_CC=0 where Voucher='"+NoteNumber+"'")
	

}
