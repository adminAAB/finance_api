import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI
import com.keyword.GEN5


if (Proses == 'GenerateVoucher Finance.Voucher'){
	
	//******************** Select Finance.Voucher ********************//

	def queryGenerateVoucher = ("select voucherno from Finance.Voucher where voucherid in "
		+"(select voucherid from finance.voucherdetail where financenoteid in "
		+"(select financenoteid from finance.financenotes where notenumber='"+NoteNumber+"'))")
	
	def StrVoucherNo = UI.getValueDatabase('172.16.94.145', 'a2isFinanceDB', queryGenerateVoucher, 'voucherno')
	
	def StrVoucherStatusID
	
	def StrVoucherID
	
	println (StrVoucherNo)
	
	GlobalVariable.VoucherNo=StrVoucherNo
	
	println (GlobalVariable.VoucherNo)
	
	queryGenerateVoucher = ("select VoucherID from Finance.Voucher where VoucherNo='"+StrVoucherNo+"'")
	
	StrVoucherID = UI.getValueDatabase('172.16.94.145', 'a2isFinanceDB', queryGenerateVoucher, 'VoucherID')
	
	println(StrVoucherID)
	
	GlobalVariable.VoucherID=StrVoucherID
	
	println(GlobalVariable.VoucherID)
	
	queryGenerateVoucher = ("select voucherstatusid from Finance.Voucher where voucherno='"+StrVoucherNo+"'")
	
	StrVoucherStatusID=UI.getValueDatabase('172.16.94.145', 'a2isFinanceDB', queryGenerateVoucher, 'voucherstatusid')
	
	WS.verifyMatch(StrVoucherStatusID.toString(), VoucherStatusID.toString(), false)
	
} else if (Proses == 'GenerateVoucher Finance.PaymentInstrument'){
	
	//******************** Select Finance.PaymentInstrument ********************//
	
	def queryPaymentInstrument = ("select PaymentInstrumentNo from Finance.PaymentInstrument where PaymentInstrumentID in "
		+"(select PaymentInstrumentID from finance.PaymentInstrumentDetail where VoucherNo='"+ GlobalVariable.VoucherNo +"')")
	
	def StrqueryPaymentInstrumentNo = UI.getValueDatabase('172.16.94.145', 'a2isFinanceDB', queryPaymentInstrument, 'PaymentInstrumentNo')
	
	println (StrqueryPaymentInstrumentNo)
	
	GlobalVariable.PaymentInstrumentNo=StrqueryPaymentInstrumentNo
	
	println (GlobalVariable.PaymentInstrumentNo)
	
} else if (Proses == 'GenerateVoucher Finance.FinanceNoteAdditional'){
	
	def queryFinanceNoteAdditional = ("select IsFinalSettled from Finance.FinanceNoteAdditional where NoteNumber='"+NoteNumber+"'")
	
	def StrIsFinalSettled = UI.getValueDatabase('172.16.94.145', 'a2isFinanceDB', queryFinanceNoteAdditional, 'IsFinalSettled')
	
	println (StrIsFinalSettled)
	
	WS.verifyMatch(StrIsFinalSettled.toString(), IsFinalSettled.toString(), false)
	
} else if (Proses == 'SubmitVoucher Finance.Voucher'){

	def queryVoucher = ("select voucherno from Finance.Voucher where voucherno='"+GlobalVariable.VoucherNo+"'")
	
	def StrVoucherNo = UI.getValueDatabase('172.16.94.145', 'a2isFinanceDB', queryVoucher, 'voucherno')
	
	println (StrVoucherNo)
	
	WS.verifyMatch(StrVoucherNo, GlobalVariable.VoucherNo, false)
	
} else if (Proses == 'SubmitVoucher Finance.FinanceNotes'){

	def queryFinanceNotes = ("select docno from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"'")
	
	def StrDocNo = UI.getValueDatabase('172.16.94.145', 'a2isFinanceDB', queryFinanceNotes, 'DocNo')
	
	println (StrDocNo)
	
	WS.verifyMatch(StrDocNo, DocNo, false)
	
} else if (Proses == 'SubmitFN Finance.FinanceNotes-Journal'){

	queryFinanceNotes = ("select tpjournal from Finance.FinanceNotes where NoteNumber='"+NoteNumber+"'")
	
	StrTpJournal = UI.getValueDatabase('172.16.94.145', 'a2isFinanceDB', queryFinanceNotes, 'tpjournal')
	
	println (StrTpJournal)
	
	WS.verifyMatch(StrTpJournal, TpJournal, false)
	
}


