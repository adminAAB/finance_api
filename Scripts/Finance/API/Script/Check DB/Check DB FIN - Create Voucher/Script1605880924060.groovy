import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5
import java.text.DecimalFormat
import java.text.NumberFormat
import java.math.RoundingMode

DecimalFormat df = new DecimalFormat("#.####")

DecimalFormat de = new DecimalFormat("#.##")

DecimalFormat dg = new DecimalFormat("#.###")

df.setRoundingMode(RoundingMode.HALF_UP);

de.setRoundingMode(RoundingMode.HALF_UP);

dg.setRoundingMode(RoundingMode.DOWN);

if (Proses == "GetNoteData"){
	
	String Query = ""
	
	if (Skenario == "Tax"){
		
		Query = "CAST(RequestAmount - ABS(WithHoldingTaxAmount) - BankCharges as decimal (20,4)) "
				
	} else if (Skenario == "NonTax"){
		
		Query = "CAST(RequestAmount as decimal (20,4)) "
	
	}
	
	GlobalVariable.GetDataCompareToVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataTaxCompareToVoucherDetail.replace("GVNoteNumber",GlobalVariable.ParamNoteNumber.toString().replace('"', "'")).replace("'GVCashAmount'",Query))
	
	println (GlobalVariable.GetDataCompareToVD)

} else if (Proses == "CreateVoucher"){
	
	/* untuk cek pembentukan data FinanceNoteAdditional */
	
	println (GlobalVariable.VoucherNo)

	println (GlobalVariable.FIN_GetFinanceNoteAdditional.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	ArrayList SourceDataFNA = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNotes.replace("GVnotenumber",GlobalVariable.ParamNoteNumber.replace('"', "'")))
	
	ArrayList APIDataFNA = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNoteAdditional.replace("GVvoucherno",GlobalVariable.VoucherNo))
			
	println (APIDataFNA)
	
	println (SourceDataFNA)
	
	WebUI.verifyMatch(APIDataFNA.size().toString(), SourceDataFNA.size().toString(), false)
	
	for (j = 0; j < SourceDataFNA.size(); j++){
		
		for (k = 0; k < SourceDataFNA[0].size(); k++){
					
			if ((APIDataFNA[j])[k] == (SourceDataFNA[j])[k]) {
					
				KeywordUtil.markPassed("Value " + (APIDataFNA[j])[k] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFNA[j])[k] + " has different Value from database = " + (SourceDataFNA[j])[k])
			}
				
		}
			
	}
	
	/* untuk cek pembentukan data nota SST dan CMT */
	
	ArrayList APIDataSSTCMT = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNotesSSTCMT.replace("GVVoucherNo",GlobalVariable.VoucherNo))
	
	ArrayList SourceDataCMT = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesCMT.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	if (SourceDataCMT != null){
		
		ArrayList SourceDataSSTPPH = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesSSTPPH.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
		ArrayList SourceDataSSTPPN = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesSSTPPN.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
		println (SourceDataSSTPPN)
		
		ArrayList SourceDataSSTCMT = new ArrayList()
			
		// add array nota CMT
		for (T = 0; T < SourceDataCMT.size(); T++){
				
			SourceDataSSTCMT.add(SourceDataCMT[T])
				
		}
		
		if (SourceDataSSTPPH != null){
			
			// add array nota SST PPH
			for (H = 0; H < SourceDataSSTPPH.size(); H++){
				
				SourceDataSSTCMT.add(SourceDataSSTPPH[H])
				
			}
		}
		
		if (SourceDataSSTPPN != null){
			
			// add array nota SST PPN
			for (N = 0; N < SourceDataSSTPPN.size(); N++){
				
				SourceDataSSTCMT.add(SourceDataSSTPPN[N])
				
			}
		}
		
		println (APIDataSSTCMT)
		
		println (SourceDataSSTCMT)
		
		WebUI.verifyMatch(APIDataSSTCMT.size().toString(), SourceDataSSTCMT.size().toString(), false)
		
		for (ij = 0; ij < SourceDataSSTCMT.size(); ij++){
			
			for (ik = 0; ik < SourceDataSSTCMT[0].size(); ik++){
						
				if ((APIDataSSTCMT[ij])[ik] == (SourceDataSSTCMT[ij])[ik]) {
						
					KeywordUtil.markPassed("Value " + (APIDataSSTCMT[ij])[ik] +" from API same with Database.")
						
				} else {
						
					KeywordUtil.markFailedAndStop("Value from API = " + (APIDataSSTCMT[ij])[ik] + " has different Value from database = " + (SourceDataSSTCMT[ij])[ik])
				}
					
			}
				
		}
		
	} else {
	
		if (APIDataSSTCMT == null) {
				
			KeywordUtil.markPassed("Value " + APIDataSSTCMT +" from API same with Database.")
				
		} else {
				
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataSSTCMT + " has different Value from database null")
		}
	
	}
	
	/* untuk cek pembentukan data VoucherDetail */
	
	ArrayList APIDataVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherDetail.replace("GVvoucherno", GlobalVariable.VoucherNo))
		
	ArrayList SourceDataVD = new ArrayList()
		
	SourceDataVD = GlobalVariable.GetDataCompareToVD

	for (b = 0; b < SourceDataVD.size(); b++){
		
		SourceDataVD[b][37] = GlobalVariable.username[0] // set CreatedBy
		
		if (GlobalVariable.VoucherTypeCode == 'PS'){
						
			if (Skenario == "Tax"){
				
				double OffsetAmount = 0
				
				SourceDataVD[b][15] = de.format(SourceDataVD[b][15].toDouble()) //convert to decimal 2 angka field WithHoldingTaxAmount
					
				APIDataVD[b][15] = de.format(APIDataVD[b][15].toDouble()) //convert to decimal 2 angka field WithHoldingTaxAmount
								
				SourceDataVD[b][19] = '0.00' //convert to decimal 2 angka field OverShortage
					
				APIDataVD[b][19] = '0.00' //convert to decimal 2 angka field OverShortage
					
				if (SourceDataVD[b][9] != 'SCM' && SourceDataVD[b][9] != 'SOT' && SourceDataVD[b][9] != 'SPR'){
						
					OffsetAmount = SourceDataVD[b][29].toDouble()
						
				}
					
				if (SourceDataVD[b][9] == 'SCM' || SourceDataVD[b][9] == 'SOT' || SourceDataVD[b][9] == 'SPR'){
						
					if (SourceDataVD[b][29].toDouble() < OffsetAmount){
							
						Double d = new Double( SourceDataVD[b][7].toDouble() - (OffsetAmount.toDouble() - SourceDataVD[b][29].toDouble()))
							
						def settlementAmount = de.format(d)
							
						SourceDataVD[b][30] =  settlementAmount //set SettlementAmount
							
						SourceDataVD[b][29] =  OffsetAmount //set CashAmount
							
					}
						
				}
				
				SourceDataVD[b][30] = de.format(SourceDataVD[b][30].toDouble())
				
				SourceDataVD[b][29] = de.format(SourceDataVD[b][29].toDouble())
				
				APIDataVD[b][30] = de.format(APIDataVD[b][30].toDouble())
				
				APIDataVD[b][29] = de.format(APIDataVD[b][29].toDouble())		
				
			} else if (Skenario == "NonTax"){
				
					
			}			

		} else if (GlobalVariable.VoucherTypeCode == 'RO'){
		
				
				
		} else if (GlobalVariable.VoucherTypeCode == 'PO'){ 
		

				
									
		} else if (GlobalVariable.VoucherTypeCode == 'RC'){
		
		
		} else if (GlobalVariable.VoucherTypeCode == 'PY'){
		
			SourceDataVD[b][15] = de.format(SourceDataVD[b][15].toDouble()) //convert to decimal 3 angka field WithHoldingTaxAmount
					
			APIDataVD[b][15] = de.format(APIDataVD[b][15].toDouble()) //convert to decimal 3 angka field WithHoldingTaxAmount

			SourceDataVD[b][29] = dg.format(SourceDataVD[b][29].toDouble()) // convert to decimal 3 angka field CashAmount
			
			APIDataVD[b][29] = dg.format(APIDataVD[b][29].toDouble()) // convert to decimal 3 angka field CashAmount
				
			SourceDataVD[b][32] = dg.format(SourceDataVD[b][32].toDouble()) // convert to decimal 3 angka field TaxGrossAmount
			
			APIDataVD[b][32] = dg.format(APIDataVD[b][32].toDouble()) // convert to decimal 3 angka field TaxGrossAmount
				
		} else if (GlobalVariable.VoucherTypeCode == 'W1'){
		
			SourceDataVD[b][12] = '0.00' // set VATPct
			
			SourceDataVD[b][13] = '0.00' // set WithHoldingTaxPct
									
			SourceDataVD[b][14] = '0.00' // set VATAmount
									
			SourceDataVD[b][15] = '0.00' // set WithHoldingTaxAmount
			
			APIDataVD[b][15] = '0.00' // set WithHoldingTaxAmount
									
			SourceDataVD[b][18] = '1' // set IsFullPayment
									
			SourceDataVD[b][22] = '0' // set IsProgressive
									
			SourceDataVD[b][25] = SourceDataVD[b][6] // set SuspendAmount
			
			SourceDataVD[b][26] = '0' // set TaxTypeID
			
			SourceDataVD[b][29] = '0.0000' // set CashAmount
									
			SourceDataVD[b][30] = '0.0000' // set SettlementAmount	
			
			SourceDataVD[b][32] = '0.0000' // set TaxGrossAmount						
				
		} else if (GlobalVariable.VoucherTypeCode == 'W2'){
		
			SourceDataVD[b][13] = '0.00' // set WithHoldingTaxPct
								
			SourceDataVD[b][15] = '0.0000' // set WithHoldingTaxAmount
			
			SourceDataVD[b][22] = '0' // set IsProgressive
			
			SourceDataVD[b][26] = '0' // set TaxTypeID
			
			SourceDataVD[b][32] = '0.0000' // set TaxGrossAmount
					
		}
	}
	
	println (SourceDataVD)
	
	println (APIDataVD)
	
	println (APIDataVD.size().toString() +" - "+  SourceDataVD.size().toString())
		
	WebUI.verifyMatch(APIDataVD.size().toString(), SourceDataVD.size().toString(), false)
	
	WebUI.verifyMatch(APIDataVD[0].size().toString(), SourceDataVD[0].size().toString(), false)
		
	for (k = 0; k < SourceDataVD.size(); k++){
			
		for (l = 0; l < SourceDataVD[0].size(); l++){
			
			if (APIDataVD[k][l] == SourceDataVD[k][l]) {
					
				KeywordUtil.markPassed("Value " + APIDataVD[k][l] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + APIDataVD[k][l] + " has different Value from database = " + SourceDataVD[k][l])
			}
		}
	}
	
	/* untuk cek pembentukan data Voucher */
	
	ArrayList SourceDataV = new ArrayList()
	
	SourceDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVoucher.replace("GVAABAccountID",GlobalVariable.AABBankAccountID.toString()))
	
	println (GlobalVariable.username)
	
	println (GlobalVariable.BEY_GetCdEntity.replace("GVlogid",GlobalVariable.username[0]))
	
	GlobalVariable.CdEntity = GEN5.getValueDatabase("172.16.94.70", "BeyondDB_Apr2020", GlobalVariable.BEY_GetCdEntity.replace("GVlogid",GlobalVariable.username[0]),"cdEntity")

	double Nominal = 0.0000
	
	double Diff = 0.0000
	
	for (y = 0; y < GlobalVariable.GetDataCompareToVD.size(); y++){
		
		if (GlobalVariable.GetDataCompareToVD[y][4] == '1'){
			
			Nominal += GlobalVariable.GetDataCompareToVD[y][29].toDouble()
			
		} else {
		
			Diff += GlobalVariable.GetDataCompareToVD[y][29].toDouble()
		}
	}
	
	println (Nominal)
	
	println (Diff)
	
	def NominalFix = de.format(Nominal)
	
	def DiffFix = de.format(Diff)
	
	if (Nominal > Diff){
		
		SourceDataV[13] = NominalFix.toString() //set nominal
		
		if (Diff > 0){
			
			SourceDataV[14] = DiffFix.toString() //set diff
			
		} else {
		
			SourceDataV[14] = '0.00' //set diff
			
		}
		
	} else if (Nominal == Diff){
	
		SourceDataV[13] = NominalFix.toString() //set nominal
	
		SourceDataV[14] = DiffFix.toString() //set diff
		
	} else {
	
		SourceDataV[13] = DiffFix.toString() //set nominal
		
		SourceDataV[14] = '0.00' //set diff
	}
		
	SourceDataV[10] = GlobalVariable.PaymentMethodID.toString() //set PaymentMethodID
		
	SourceDataV[12] = GlobalVariable.Remarks //set Remark
			
	SourceDataV[51] = GlobalVariable.CdEntity //set EntryUserOrganizationEntityCode
		
	SourceDataV[45] = GlobalVariable.username[0] //set CreatedBy
		
	SourceDataV[47] = GlobalVariable.username[GlobalVariable.username.size()-1] //set ModifiedBy
	
	if (GlobalVariable.VoucherTypeCode == 'PS'){
			
		SourceDataV[2] = '0' //set IsGeneratedBySUNGL
				
		SourceDataV[3] = '3' //set VoucherTypeID
				
		SourceDataV[5] = '3' //set ExpectedVoucherStatusID			
		
	} else if (GlobalVariable.VoucherTypeCode == 'PY'){
	
		SourceDataV[3] = '2' //set VoucherTypeID
		
		SourceDataV[5] = '2' //set ExpectedVoucherStatusID
		
		SourceDataV[15] = '0' //set IsDebtor
		
		SourceDataV[16] = '1' //set IsCreditor

	} else if (GlobalVariable.VoucherTypeCode == 'RO'){
			
		SourceDataV[2] = '1' //set IsGeneratedBySUNGL
				
		SourceDataV[3] = '9' //set VoucherTypeID
				
		SourceDataV[5] = '3' //set ExpectedVoucherStatusID
				
	} else if (GlobalVariable.VoucherTypeCode == 'PO'){
			
		SourceDataV[2] = '1' //set IsGeneratedBySUNGL
				
		SourceDataV[3] = '10' //set VoucherTypeID
				
		SourceDataV[5] = '3' //set ExpectedVoucherStatusID				
								
	} else if (GlobalVariable.VoucherTypeCode == 'RC'){
			
		SourceDataV[2] = '0' //set IsGeneratedBySUNGL
				
		SourceDataV[3] = '1' //set VoucherTypeID
				
		SourceDataV[5] = '3' //set ExpectedVoucherStatusID				
			
		if (Skenario == "Tax"){
					
			SourceDataV[45] = GlobalVariable.username[0] //set CreatedBy
					
			SourceDataV[47] = GlobalVariable.username[GlobalVariable.username.size()-1] //set ModifiedBy
					
		} else if (Skenario == "NonTax"){
				
			SourceDataV[45] = GlobalVariable.username[0] //set CreatedBy
					
			SourceDataV[47] = GlobalVariable.username[GlobalVariable.username.size()-1] //set ModifiedBy
					
		}			
			
	} else if (GlobalVariable.VoucherTypeCode == 'W1'){
			
		SourceDataV[2] = '0' //set IsGeneratedBySUNGL
				
		SourceDataV[3] = '7' //set VoucherTypeID
				
		SourceDataV[5] = '3' //set ExpectedVoucherStatusID
	
		SourceDataV[13] = '0' //set Nominal
				
		SourceDataV[14] = '0.00' //set Diff					
			
	} else if (GlobalVariable.VoucherTypeCode == 'W2'){
			
		SourceDataV[2] = '0' //set IsGeneratedBySUNGL
				
		SourceDataV[3] = '8' //set VoucherTypeID
				
		SourceDataV[5] = '3' //set ExpectedVoucherStatusID		
		
		SourceDataV[47] = GlobalVariable.username[GlobalVariable.username.size()-1] //set ModifiedBy
			
	}
	
	ArrayList APIDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	APIDataV[13] = de.format(APIDataV[13].toDouble()) //set dg format yg sama
	
	if (APIDataV[14].toDouble() > 0){
		
		APIDataV[14] = de.format(APIDataV[14].toDouble()) //set dg format yg sama
	
	}
	
	println (APIDataV)
	
	println (SourceDataV)
	
	println (APIDataV.size().toString() +" - "+  SourceDataV.size().toString())
	
	WebUI.verifyMatch(APIDataV.size().toString(), SourceDataV.size().toString(), false)
	
	for (c = 0; c < SourceDataV.size(); c++){
		
		if (APIDataV[c] == SourceDataV[c]) {
			
			KeywordUtil.markPassed("Value " + APIDataV[c] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataV[c] + " has different Value from database = " + SourceDataV[c])
		}
	}
			
	/* untuk cek pembentukan data FinanceSettlements */
	
	ArrayList SourceDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceSettlements.replace("GVnotenumber",GlobalVariable.ParamNoteNumber.replace('"', "'")))
	
	ArrayList APIDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceSettlements.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	for (jj = 0; jj < SourceDataFS.size(); jj++){
		
		if (GlobalVariable.VoucherTypeCode == 'PS'){
						
			SourceDataFS[jj][3] = de.format(SourceDataFS[jj][3].toDouble()) // set SettlementAmount
			
			APIDataFS[jj][3] = de.format(APIDataFS[jj][3].toDouble()) // set SettlementAmount
			
			SourceDataFS[jj][9] = de.format(SourceDataFS[jj][9].toDouble()) // set SettlementAmount
			
			APIDataFS[jj][9] = de.format(APIDataFS[jj][9].toDouble()) // set SettlementAmount		
			
		} else if (GlobalVariable.VoucherTypeCode == 'RO'){
						
						
		} else if (GlobalVariable.VoucherTypeCode == 'PO'){
						
											
		} else if (GlobalVariable.VoucherTypeCode == 'RC'){
						
						
		} else if (GlobalVariable.VoucherTypeCode == 'W1'){
					
			SourceDataFS[jj][8] = SourceDataFS[jj][3] // set SuspendAmount
			
			SourceDataFS[jj][3] = '0.0000' // set SettlementAmount
						
						
		} else if (GlobalVariable.VoucherTypeCode == 'W2'){
		
		
		}
		
		SourceDataFS[jj][4] = GlobalVariable.username[GlobalVariable.username.size()-1] // set CdHandBy
		
		SourceDataFS[jj][13] = GlobalVariable.username[GlobalVariable.username.size()-1] // set CreatedBy
										
	}
		
	println (APIDataFS)
	
	println (SourceDataFS)
	
	WebUI.verifyMatch(APIDataFS.size().toString(), SourceDataFS.size().toString(), false)
	
	for (d = 0; d < SourceDataFS.size(); d++){
		
		for (e = 0; e < SourceDataFS[0].size(); e++){
					
			if ((APIDataFS[d])[e] == (SourceDataFS[d])[e]) {
					
				KeywordUtil.markPassed("Value " + (APIDataFS[d])[e] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFS[d])[e] + " has different Value from database = " + (SourceDataFS[d])[e])
			}
				
		}
			
	}
	
	/* untuk cek pembentukan data vouchersettlementbackhistory */
	
	ArrayList SourceDataVSH = new ArrayList()
	
	SourceDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVoucherSettlementBackHistory)

	if (GlobalVariable.VoucherTypeCode == 'PS'){
		
		SourceDataVSH[1] = '3' //set VoucherTypeID	
		
	} else if (GlobalVariable.VoucherTypeCode == 'RO'){
					
		SourceDataVSH[1] = '9' //set VoucherTypeID
					
	} else if (GlobalVariable.VoucherTypeCode == 'PO'){
					
		SourceDataVSH[1] = '10' //set VoucherTypeID
	
	} else if (GlobalVariable.VoucherTypeCode == 'RC'){
					
		SourceDataVSH[1] = '1' //set VoucherTypeID
		
	} else if (GlobalVariable.VoucherTypeCode == 'PY'){
	
		SourceDataVSH[1] = '2' //set VoucherTypeID
					
	} else if (GlobalVariable.VoucherTypeCode == 'W1'){
	
		SourceDataVSH[1] = '7' //set VoucherTypeID					
					
	} else if (GlobalVariable.VoucherTypeCode == 'W2'){
						
		SourceDataVSH[1] = '8' //set VoucherTypeID
		
	}
	
	ArrayList APIDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherSettlementBackHistory.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataVSH)
	
	println (SourceDataVSH)
	
	println (APIDataVSH.size().toString() +" - "+  SourceDataVSH.size().toString())
	
	WebUI.verifyMatch(APIDataVSH.size().toString(), SourceDataVSH.size().toString(), false)
	
	for (k = 0; k < SourceDataVSH.size(); k++){
		
		if (APIDataVSH[k] == SourceDataVSH[k]) {
			
			KeywordUtil.markPassed("Value " + APIDataVSH[k] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataVSH[k] + " has different Value from database = " + SourceDataVSH[k])
		}
	}
			
}
		
