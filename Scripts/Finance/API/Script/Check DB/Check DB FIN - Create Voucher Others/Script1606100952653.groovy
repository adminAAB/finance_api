import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5
import java.text.DecimalFormat
import java.text.NumberFormat
import java.math.RoundingMode

	DecimalFormat df = new DecimalFormat("#.####")
	
	DecimalFormat de = new DecimalFormat("#.##")
	
	df.setRoundingMode(RoundingMode.HALF_UP);
	
	de.setRoundingMode(RoundingMode.HALF_UP);

	/* untuk cek pembentukan data FinanceNotes */
	
	if (GlobalVariable.VoucherTypeCode =='MP' || GlobalVariable.VoucherTypeCode =='MR'){
		
		ArrayList APIDataFN = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNotesSuspend.replace("GVVoucherNo", GlobalVariable.VoucherNo))
		
		println (APIDataFN)
		
		if (APIDataFN == null) {
		
			KeywordUtil.markPassed("Value " + APIDataFN +" from API same with Database.")
		
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataFN + " has different Value from database = null")
		}
		
	} else if (GlobalVariable.VoucherTypeCode =='PO' || GlobalVariable.VoucherTypeCode =='RO'){
	
		ArrayList SourceDataFN = new ArrayList()
		
		println (GlobalVariable.ParamList)
		
		println (GlobalVariable.FIN_GetDataCompareToFinanceNotesSuspend)
			
		for (aa = 0; aa < GlobalVariable.ParamList.size(); aa++){			
				
			ArrayList SourceData0 = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesSuspend.replace("GVVoucherNo",GlobalVariable.VoucherNo))
				
			ArrayList SourceData1 = SourceData0[0]
										
			if (GlobalVariable.ParamList[aa][3].toInteger() < 0){
						
				SourceData1.set(8 , "1")
						
				SourceData1.set(9 , "0")
						
				SourceData1.set(6 , GlobalVariable.ParamList[aa][3].replace("-","") + ".0000")
						
				SourceData1.set(12 , GlobalVariable.ParamList[aa][0])
						
				SourceData1.set(39 , GlobalVariable.ParamList[aa][1])
						
			} else {
						
				SourceData1.set(8 , "0")
						
				SourceData1.set(9 , "1")
						
				SourceData1.set(6 , GlobalVariable.ParamList[aa][3] + ".0000")
						
				SourceData1.set(12 , GlobalVariable.ParamList[aa][0])
						
				SourceData1.set(39 , GlobalVariable.ParamList[aa][1])
					
			}
		
			SourceDataFN.add(SourceData1)
				
		}
		
		ArrayList APIDataFN = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNotesSuspend.replace("GVVoucherNo", GlobalVariable.VoucherNo))
		
		println (SourceDataFN[0].size() +  " - " + APIDataFN[0].size())
		
		println (SourceDataFN)
		
		println (APIDataFN)
		
		WebUI.verifyMatch(APIDataFN[0].size().toString(), SourceDataFN[0].size().toString(), false)
		
		WebUI.verifyMatch(APIDataFN.size().toString(), SourceDataFN.size().toString(), false)
		
		for (j = 0; j < SourceDataFN.size(); j++){
			
			for (k = 0; k < SourceDataFN[0].size(); k++){
						
				if ((APIDataFN[j])[k] == (SourceDataFN[j])[k]) {
						
					KeywordUtil.markPassed("Value " + (APIDataFN[j])[k] +" from API same with Database.")
						
				} else {
						
					KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFN[j])[k] + " has different Value from database = " + (SourceDataFN[j])[k])
				}
					
			}
				
		}
	}
			
	/* untuk cek pembentukan data FinanceSettlements */
			
	ArrayList APIDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceSettlements.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataFS)
	
	if (APIDataFS == null) {
		
		KeywordUtil.markPassed("Value " + APIDataFS +" from API same with Database.")
		
	} else {
			
		KeywordUtil.markFailedAndStop("Value from API = " + APIDataFS + " has different Value from database = null")
	}
	
	/* untuk cek pembentukan data SuspendMonitoring */
	
	if (GlobalVariable.VoucherTypeCode =='MP' || GlobalVariable.VoucherTypeCode =='MR'){
		
		ArrayList APIDataSM = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetSuspendMonitoring.replace("GVVoucherNo",GlobalVariable.VoucherNo))
				
		println (APIDataSM)
		
		if (APIDataSM == null) {
			
			KeywordUtil.markPassed("Value " + APIDataSM +" from API same with Database.")
			
		} else {
				
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataSM + " has different Value from database = null")
		}
	
	} else if (GlobalVariable.VoucherTypeCode =='PO' || GlobalVariable.VoucherTypeCode =='RO'){
	
		ArrayList SourceDataSM = new ArrayList()
		
		if (GlobalVariable.ParamList.size() > 1){
				
			for (cc = 0; cc < GlobalVariable.ParamList.size(); cc++){
				
				ArrayList SourceData = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToSuspendMonitoring.replace("GVSuspendResultCategoryCode", GlobalVariable.ParamList[cc][2]))
					
				SourceDataSM.add(SourceData)
				
				SourceDataSM[0][17] = GlobalVariable.username[1]
			}
				
		} else {
			
			SourceDataSM = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToSuspendMonitoring.replace("GVSuspendResultCategoryCode", GlobalVariable.ParamList[0][2]))
			
			SourceDataSM[0][17] = GlobalVariable.username[1]
		}
			
		ArrayList APIDataSM = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetSuspendMonitoring.replace("GVVoucherNo",GlobalVariable.VoucherNo))
			
		println (SourceDataSM[0].size() +  " - " + APIDataSM[0].size())
			
		println (SourceDataSM)
			
		println (APIDataSM)
			
		WebUI.verifyMatch(APIDataSM.size().toString(), SourceDataSM.size().toString(), false)
			
		WebUI.verifyMatch(APIDataSM[0].size().toString(), SourceDataSM[0].size().toString(), false)
			
		for (a = 0; a < SourceDataSM.size(); a++){
				
			for (b = 0; b < SourceDataSM[0].size(); b++){
							
				if ((APIDataSM[a])[b] == (SourceDataSM[a])[b]) {
							
					KeywordUtil.markPassed("Value " + (APIDataSM[a])[b] +" from API same with Database.")
							
				} else {
							
					KeywordUtil.markFailedAndStop("Value from API = " + (APIDataSM[a])[b] + " has different Value from database = " + (SourceDataSM[a])[b])
				}
						
			}
					
		}
	}

	
	/* untuk cek pembentukan data Voucher */
	
	ArrayList SourceDataV = new ArrayList()
	
	SourceDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVoucher.replace("GVAABAccountID",GlobalVariable.AABBankAccountID.toString()))
	
	GlobalVariable.CdEntity = GEN5.getValueDatabase("172.16.94.70", "BeyondDB_Apr2020", GlobalVariable.BEY_GetCdEntity.replace("GVlogid",GlobalVariable.username[1]),"cdEntity")
	
	ArrayList APIDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	// untuk hitung total nominal nota
	String Nominal = ''
	
	if (GlobalVariable.ParamList.size() > 1) {
		
		Integer CalcNominal = 0
		
		for (xx = 0; xx < GlobalVariable.ParamList.size(); xx ++){
			
			println (GlobalVariable.ParamList[xx][3].toInteger())
		
			CalcNominal += GlobalVariable.ParamList[xx][3].toInteger()
							
		}
		
		Nominal = CalcNominal.toString().replace("-","")
		
	} else {
	
		Nominal = GlobalVariable.ParamList[0][3].toString().replace("-","")
	
	}
	
	SourceDataV[5] = '3' //set ExpectedVoucherStatusID
	
	SourceDataV[12] = GlobalVariable.Remarks //set Remark
			
	SourceDataV[13] = Nominal //set Nominal	
	
	APIDataV[13] = APIDataV[13].replace('.00','') //set Nominal
	
	SourceDataV[14] = '0.00' //set Diff
	
	SourceDataV[45] = GlobalVariable.username[0] //set CreatedBy
	
	SourceDataV[47] = GlobalVariable.username[GlobalVariable.username.size()-1] //set CreatedBy
	
	SourceDataV[51] = GlobalVariable.CdEntity //set EntryUserOrganizationEntityCode	
	
	SourceDataV[38] = '0' //set DirectorateID
	
	if (GlobalVariable.VoucherTypeCode == 'RO'){
				
		SourceDataV[3] = '9' //set VoucherTypeID
		
		SourceDataV[15] = '1' //set IsDebtor
	
		SourceDataV[16] = '0' //set IsCreditor
		
	} else if (GlobalVariable.VoucherTypeCode == 'MR'){
	
		SourceDataV[3] = '14' //set VoucherTypeID
		
		SourceDataV[15] = '1' //set IsDebtor
	
		SourceDataV[16] = '0' //set IsCreditor
				
	} else if (GlobalVariable.VoucherTypeCode == 'PO'){
	
		SourceDataV[3] = '10' //set VoucherTypeID
			
		SourceDataV[15] = '0' //set IsDebtor
		
		SourceDataV[16] = '1' //set IsCreditor
		
	} else if (GlobalVariable.VoucherTypeCode == 'MP'){
	
		SourceDataV[3] = '15' //set VoucherTypeID
			
		SourceDataV[15] = '0' //set IsDebtor
		
		SourceDataV[16] = '1' //set IsCreditor
	}
	
	println (APIDataV)
	
	println (SourceDataV)
	
	println (APIDataV.size().toString() +" - "+  SourceDataV.size().toString())
	
	WebUI.verifyMatch(APIDataV.size().toString(), SourceDataV.size().toString(), false)
	
	for (c = 0; c < SourceDataV.size(); c++){
	
		if (APIDataV[c] == SourceDataV[c]) {
			
			KeywordUtil.markPassed("Value " + APIDataV[c] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataV[c] + " has different Value from database = " + SourceDataV[c])
		}
	}
	
	/* untuk cek pembentukan data VoucherDetail */
		
	ArrayList APIDataVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherDetail.replace("GVvoucherno",GlobalVariable.VoucherNo))

	ArrayList SourceDataVD = new ArrayList()
	
	if (GlobalVariable.ParamList.size() > 1){
		
		for (dd = 0; dd < GlobalVariable.ParamList.size(); dd++){			
			
			ArrayList SDataVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVoucherDetailOthers.replace("GVSubJournalTypeCode",GlobalVariable.ParamList[dd][1]).replace("GVJournalTypeCode",GlobalVariable.ParamList[dd][0]).replace("GVTaxTypeCode","").replace("GVNominal",GlobalVariable.ParamList[dd][3]).replace("GVSuspendResultCategoryCode",GlobalVariable.ParamList[dd][2]))
						
			if (GlobalVariable.VoucherTypeCode == 'RO' || GlobalVariable.VoucherTypeCode == 'MR'){
				
				SourceDataVD[dd][4] = "1" // set IsDebtor
				
				SourceDataVD[dd][5] = "0" // set IsCreditor
				
			} else if (GlobalVariable.VoucherTypeCode == 'PO' || GlobalVariable.VoucherTypeCode == 'MP'){
			
				SourceDataVD[dd][4] = "0" // set IsDebtor
				
				SourceDataVD[dd][5] = "1" // set IsCreditor
				
			}
			
			SDataVD[dd][27] = "Remark suspend automation" // set remark
			
			SDataVD[dd][37] = GlobalVariable.username[0] // set CreatedBy
			
			SourceDataVD.add(SDataVD)
		}
		
	} else {	

		SourceDataVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVoucherDetailOthers.replace("GVSubJournalTypeCode",GlobalVariable.ParamList[0][1]).replace("GVJournalTypeCode",GlobalVariable.ParamList[0][0]).replace("GVTaxTypeCode","").replace("GVNominal",GlobalVariable.ParamList[0][3]).replace("GVSuspendResultCategoryCode",GlobalVariable.ParamList[0][2]))
		
		if (GlobalVariable.VoucherTypeCode == 'RO' || GlobalVariable.VoucherTypeCode == 'MR'){
			
			SourceDataVD[0][4] = "1" // set IsDebtor
			
			SourceDataVD[0][5] = "0" // set IsCreditor
			
		} else if (GlobalVariable.VoucherTypeCode == 'PO' || GlobalVariable.VoucherTypeCode == 'MP'){
		
			SourceDataVD[0][4] = "0" // set IsDebtor
			
			SourceDataVD[0][5] = "1" // set IsCreditor
			
		}
		
		SourceDataVD[0][27] = "Remark suspend automation"
		
		SourceDataVD[0][37] = GlobalVariable.username[0] //set CreatedBy
		
	}
	
	println (APIDataVD)
	
	println (SourceDataVD)
	
	println (APIDataVD[0].size().toString() +" - "+  SourceDataVD[0].size().toString())
	
	WebUI.verifyMatch(APIDataV[0].size().toString(), SourceDataV[0].size().toString(), false)
	
	for (f = 0; f < SourceDataVD.size(); f++){
	
		for (g = 0; g < SourceDataVD[0].size(); g++){
					
			if ((APIDataVD[f])[g] == (SourceDataVD[f])[g]) {
					
				KeywordUtil.markPassed("Value " + (APIDataVD[f])[g] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + (APIDataVD[f])[g] + " has different Value from database = " + (SourceDataVD[f])[g])
			}
				
		}
		
	}
	
	/* untuk cek pembentukan data vouchersettlementbackhistory */
	
	ArrayList APIDataVSH = new ArrayList()
	
	APIDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherSettlementBackHistory.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (APIDataVSH)
	
	if (APIDataVSH == null) {
	
		KeywordUtil.markPassed("Value " + APIDataVSH +" from API same with Database.")
	
	} else {
		
		KeywordUtil.markFailedAndStop("Value from API = " + APIDataVSH + " has different Value from database = null")
	}
	
	