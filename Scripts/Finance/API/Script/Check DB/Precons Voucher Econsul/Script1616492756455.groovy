import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5
import java.text.DecimalFormat
import java.text.NumberFormat
import java.math.RoundingMode

	if (Skenario == "PS"){
	
	String VoucherPS = GEN5.getValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherFromNote.replace("GVNotenumber", NoteNumber), "VoucherNo")
	
	println (VoucherPS)
	
	GlobalVariable.ParamList1 = GEN5.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetAmountDataVoucher.replace("GVnoteno", NoteNumber))
	
	//GlobalVariable.ParamList1 = [['00209/DN/JKT00/02/21', 147000.0, 3000.0, '2021-03-03','JKT00FPS21000467'], ['21FSCXDRCBA0108', 147000.0, 0.0, '2021-03-03','JKT00FPS21000467']]
	
	println (GlobalVariable.ParamList1)
	
	GEN5.updateValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_DeleteUpdateFIN.replace("GVVoucherNo",VoucherPS).replace("GVNotenumber",NoteNumber))
	
	GEN5.updateValueDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_DeleteUpdateSEA.replace("GVVoucherNo",VoucherPS).replace("GVNotenumber",NoteNumber))
	
	GlobalVariable.ParamNoteNumber = NoteNumber.replace("'",'"')
	
	println (GlobalVariable.ParamNoteNumber)
	
	GlobalVariable.username.add('SYS')
	
} else if (Skenario == "RO"){

	ArrayList LatestAppHist = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetLatestApprovalHistory.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	println (LatestAppHist)
	
	if (LatestAppHist[4] != 'VS3'){
	
		println (GlobalVariable.FIN_UpdateVoucherROEconsul.replace("GVvoucherno",GlobalVariable.VoucherNo).replace("GVTaskApprovalID",LatestAppHist[1]).replace("GVID",LatestAppHist[0]))
	
		GEN5.updateValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_UpdateVoucherROEconsul.replace("GVvoucherno",GlobalVariable.VoucherNo).replace("GVTaskApprovalID",LatestAppHist[1]).replace("GVID",LatestAppHist[0]))
		
	} else {
	
		println (GlobalVariable.FIN_UpdateVoucherROEconsul.replace("GVvoucherno",GlobalVariable.VoucherNo).replace("GVTaskApprovalID",LatestAppHist[1]).replace("GVID",LatestAppHist[0]))
	
		GEN5.updateValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_UpdateVoucherROEconsul.replace("GVvoucherno",GlobalVariable.VoucherNo).replace("GVTaskApprovalID",LatestAppHist[1]).replace("GVID",LatestAppHist[0]).replace("rowstatus=1","rowstatus=0"))
		
	}
}

