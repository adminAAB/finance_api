import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5
import com.keyword.REA

	
if (scenario == "hold"){
	
	println (GlobalVariable.FIN_UpdateHoldFlag.replace("GVFinanceNoteID", GlobalVariable.ParamString).replace("'0'","1"))
	
	GEN5.updateValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_UpdateHoldFlag.replace("GVFinanceNoteID", GlobalVariable.ParamString).replace("'0'","1"))
	
} else if (scenario == "unhold"){

	GlobalVariable.ParamNoteNumber = ParamNoteNo
	
	String FinNoteID = ''
	
	String InputNote = ParamNoteNo.replace('"', "'")
	
	println (GlobalVariable.FIN_GetFinanceNoteID.replace("GVnotenumber",InputNote))
	
	ArrayList FinanceNoteID = GEN5.getOneColumnDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNoteID.replace("GVnotenumber",InputNote),"FinanceNoteID")
	
	for (xx = 0; xx < FinanceNoteID.size(); xx ++){
			
		if (xx == FinanceNoteID.size()-1) {
			
			FinNoteID += FinanceNoteID[xx]
					
		} else {
			
			FinNoteID += FinanceNoteID[xx] + ','
	
		}
				
	}
	
	GlobalVariable.ParamString = FinNoteID
	
	GEN5.updateValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_UpdateHoldFlag.replace("GVFinanceNoteID", FinNoteID))
	
} else {

	GlobalVariable.ParamNoteNumber = ParamNoteNo
	
	String FinNoteID = ''
	
	String InputNote = ParamNoteNo.replace('"', "'")
	
	println (GlobalVariable.FIN_GetFinanceNoteID.replace("GVnotenumber",InputNote))
	
	ArrayList FinanceNoteID = GEN5.getOneColumnDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNoteID.replace("GVnotenumber",InputNote),"FinanceNoteID")
	
	for (xx = 0; xx < FinanceNoteID.size(); xx ++){
			
		if (xx == FinanceNoteID.size()-1) {
			
			FinNoteID += FinanceNoteID[xx]
					
		} else {
			
			FinNoteID += FinanceNoteID[xx] + ','
	
		}
				
	}
	
	GlobalVariable.ParamString = FinNoteID

}

