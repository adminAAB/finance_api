import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.text.DecimalFormat
import java.text.NumberFormat
import java.math.RoundingMode
	
	
DecimalFormat df = new DecimalFormat("#.####")
	
DecimalFormat de = new DecimalFormat("#.##")
	
df.setRoundingMode(RoundingMode.HALF_UP);
	
de.setRoundingMode(RoundingMode.HALF_UP);

GlobalVariable.VoucherNo = GlobalVariable.ListVoucher[0]

GlobalVariable.VoucherNoCA = GlobalVariable.ListVoucher[1]
		
/* untuk cek pembentukan data VoucherDetail */
							
ArrayList SourceDataVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVDPS.replace("GVVoucherNo",GlobalVariable.VoucherNo))
		
println (GlobalVariable.VoucherNo + " : " + GlobalVariable.VoucherNoCA)

ArrayList APIDataVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVDPS.replace("GVVoucherNo",GlobalVariable.VoucherNoCA))
		
				
println (APIDataVD)
		
println (SourceDataVD)
		
println (APIDataVD.size().toString() +" - "+  SourceDataVD.size().toString())
		
WebUI.verifyMatch(APIDataVD.size().toString(), SourceDataVD.size().toString(), false)
		
for (f = 0; f < SourceDataVD.size(); f++){
			
	for (g = 0; g < SourceDataVD[0].size(); g++){
						
		if ((APIDataVD[f])[g] == (SourceDataVD[f])[g]) {
						
			KeywordUtil.markPassed("Value " + (APIDataVD[f])[g] +" from API same with Database.")
						
		} else {
						
			KeywordUtil.markFailedAndStop("Value from API = " + (APIDataVD[f])[g] + " has different Value from database = " + (SourceDataVD[f])[g])
		}
					
	}
				
}

/* untuk cek pembentukan data FinanceSettlements */

ArrayList SourceDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFSPS.replace("GVVoucherNo",GlobalVariable.VoucherNo))

ArrayList APIDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFSPS.replace("GVVoucherNo",GlobalVariable.VoucherNoCA))
		

println (APIDataFS)

println (SourceDataFS)

WebUI.verifyMatch(APIDataFS.size().toString(), SourceDataFS.size().toString(), false)

for (d = 0; d < SourceDataFS.size(); d++){

	for (e = 0; e < SourceDataFS[0].size(); e++){
				
		if ((APIDataFS[d])[e] == (SourceDataFS[d])[e]) {
				
			KeywordUtil.markPassed("Value " + (APIDataFS[d])[e] +" from API same with Database.")
				
		} else {
				
			KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFS[d])[e] + " has different Value from database = " + (SourceDataFS[d])[e])
		}
			
	}
	
}
