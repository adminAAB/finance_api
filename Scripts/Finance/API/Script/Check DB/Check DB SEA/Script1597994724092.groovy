import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

	
if (Proses == "CreateVoucher"){

	/* untuk cek pembentukan data fSL */
	
	ArrayList NotaNoRaw = GEN5.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetFSL.replace("GVvoucherno",GlobalVariable.VoucherNo),'Doc_No')
	
	String NoteNo = ''
	
	for (xx = 0; xx < NotaNoRaw.size(); xx ++){
		
		if (xx == NotaNoRaw.size()-1) {
			
			NoteNo += "'" + NotaNoRaw[xx] + "'"
					
		} else {
			
			NoteNo += "'" + NotaNoRaw[xx] + "',"
	
		}
			
	}
	
	println (NoteNo)

	ArrayList SourceDataFSL = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataCompareToFSL.replace("'GVvoucherno'","'" + GlobalVariable.VoucherNo + "' and FN.NoteNumber in ("+ NoteNo +")"))
	
	ArrayList APIDataFSL = GEN5.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetFSL.replace("GVvoucherno",GlobalVariable.VoucherNo))
			
	println (APIDataFSL)
	
	println (SourceDataFSL)
	
	WebUI.verifyMatch(APIDataFSL.size().toString(), SourceDataFSL.size().toString(), false)
	
	for (d = 0; d < SourceDataFSL.size(); d++){
		
		for (e = 0; e < SourceDataFSL[0].size(); e++){
					
			if ((APIDataFSL[d])[e] == (SourceDataFSL[d])[e]) {
					
				KeywordUtil.markPassed("Value " + (APIDataFSL[d])[e] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFSL[d])[e] + " has different Value from database = " + (SourceDataFSL[d])[e])
			}
				
		}
			
	}
	
	/* untuk cek pembentukan data nVoucher */
	
	ArrayList SourceDatanV = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataCompareTonVoucher.replace("'GVvoucherno'","'" + GlobalVariable.VoucherNo + "' and FN.NoteNumber in ("+ NoteNo +")"))
			
	ArrayList APIDatanV = GEN5.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetnVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
			
	println (APIDatanV)
	
	println (SourceDatanV)
	
	WebUI.verifyMatch(APIDatanV.size().toString(), SourceDatanV.size().toString(), false)
	
	for (x = 0; x < SourceDatanV.size(); x++){
		
		for (y = 0; y < SourceDatanV[0].size(); y++){
					
			if ((APIDatanV[x])[y] == (SourceDatanV[x])[y]) {
					
				KeywordUtil.markPassed("Value " + (APIDatanV[x])[y] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + (APIDatanV[x])[y] + " has different Value from database = " + (SourceDatanV[x])[y])
			}
				
		}
			
	}
	
	/* untuk cek pembentukan data pVoucher */
	
	ArrayList SourceDatapV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataCompareTopVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	ArrayList APIDatapV = GEN5.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetpVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	if (GlobalVariable.VoucherTypeCode == "RC"){
		
		SourceDatapV[3] = 'RV-01-R'	
		
	} else if (GlobalVariable.VoucherTypeCode == "PS"){
	
		SourceDatapV[3] = 'PS-01'
		
	} else if (GlobalVariable.VoucherTypeCode == "PY"){
	
		SourceDatapV[3] = 'PV-01-P'
		
	}
	
	SourceDatapV[4] = GlobalVariable.Remarks
	
	println (APIDatapV)
	
	println (SourceDatapV)
	
	println (APIDatapV.size().toString() +" - "+  SourceDatapV.size().toString())
	
	WebUI.verifyMatch(APIDatapV.size().toString(), SourceDatapV.size().toString(), false)
	
	for (k = 0; k < SourceDatapV.size(); k++){
		
		if (APIDatapV[k] == SourceDatapV[k]) {
			
			KeywordUtil.markPassed("Value " + APIDatapV[k] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDatapV[k] + " has different Value from database = " + SourceDatapV[k])
		}
	}
	
	/* untuk cek pembentukan data fVoucher */
	
	ArrayList SourceDatafV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataCompareTofVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
					
	ArrayList APIDatafV = GEN5.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetfVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	if (GlobalVariable.VoucherTypeCode == "RC"){
		
		SourceDatafV[4] = 'RV-01-R'		
		
	} else if (GlobalVariable.VoucherTypeCode == "PS"){
	
		SourceDatafV[4] = 'PS-01'
		
	} else if (GlobalVariable.VoucherTypeCode == "PY"){
	
		SourceDatafV[4] = 'PV-01-P'
	
	}
	
	SourceDatafV[5] = GlobalVariable.Remarks
	
	println (APIDatafV)
	
	println (SourceDatafV)
	
	println (APIDatafV.size().toString() +" - "+  SourceDatafV.size().toString())
	
	WebUI.verifyMatch(APIDatafV.size().toString(), SourceDatafV.size().toString(), false)
	
	for (l = 0; l < SourceDatafV.size(); l++){
		
		if (APIDatafV[l] == SourceDatafV[l]) {
			
			KeywordUtil.markPassed("Value " + APIDatafV[l] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDatafV[l] + " has different Value from database = " + SourceDatafV[l])
		}
	}
	
	
} else if (Proses == "CancelVoucher"){

	/* untuk cek pembentukan data fSL */
	
	ArrayList SourceDataFSL = new ArrayList()

	String VoucherNo = ''
	
	ArrayList NotaNoRaw = GEN5.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetFSL.replace("GVvoucherno",GlobalVariable.VoucherNo),'Doc_No')
	
	String NoteNo = ''
	
	for (xx = 0; xx < NotaNoRaw.size(); xx ++){
		
		if (xx == NotaNoRaw.size()-1) {
			
			NoteNo += "'" + NotaNoRaw[xx] + "'"
					
		} else {
			
			NoteNo += "'" + NotaNoRaw[xx] + "',"
	
		}
			
	}
	
	println (NoteNo)
	
	if (GlobalVariable.VoucherTypeCodeBefore == "RC" || GlobalVariable.VoucherTypeCodeBefore == "PS" || GlobalVariable.VoucherTypeCodeBefore == "PY"){
		
		VoucherNo = GlobalVariable.VoucherNoCA
		
		SourceDataFSL = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataCompareToFSL.replace("'GVvoucherno'","'" + GlobalVariable.VoucherNo + "' and FN.NoteNumber in ("+ NoteNo +")").replace("FN.IsDebtor = '0'","FN.IsDebtor = '1'"))
		
	} else if (GlobalVariable.VoucherTypeCodeBefore == "W1" || GlobalVariable.VoucherTypeCodeBefore == "W2"){
	
		VoucherNo = GlobalVariable.VoucherNo
		
		SourceDataFSL = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataCompareToFSL.replace("'GVvoucherno'","'" + GlobalVariable.VoucherNo + "' and FN.NoteNumber in ("+ NoteNo +")").replace("'0' AS","'1' AS"))
	}
	
	println (GlobalVariable.VoucherNo)
	
	ArrayList APIDataFSL = GEN5.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetFSL.replace("GVvoucherno",VoucherNo))
			
	println (APIDataFSL)
	
	println (SourceDataFSL)
	
	WebUI.verifyMatch(APIDataFSL.size().toString(), SourceDataFSL.size().toString(), false)
	
	for (d = 0; d < SourceDataFSL.size(); d++){
		
		for (e = 0; e < SourceDataFSL[0].size(); e++){
					
			if ((APIDataFSL[d])[e] == (SourceDataFSL[d])[e]) {
					
				KeywordUtil.markPassed("Value " + (APIDataFSL[d])[e] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFSL[d])[e] + " has different Value from database = " + (SourceDataFSL[d])[e])
			}
				
		}
			
	}
	
	/* untuk cek pembentukan data nVoucher */
																																																				 
	ArrayList SourceDatanV =  GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataCompareTonVoucher.replace("'GVvoucherno'","'" + GlobalVariable.VoucherNo + "' and FN.NoteNumber in ("+ NoteNo +")").replace("'1'","'0'").replace("CAST(FN.Nominal AS DECIMAL(10,2)) AS Payment_CC","'0.00' AS Payment_CC"))

	ArrayList APIDatanV = GEN5.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetnVoucher.replace("GVvoucherno",VoucherNo))
			
	println (APIDatanV)
	
	println (SourceDatanV)
	
	WebUI.verifyMatch(APIDatanV.size().toString(), SourceDatanV.size().toString(), false)
	
	for (x = 0; x < SourceDatanV.size(); x++){
		
		for (y = 0; y < SourceDatanV[0].size(); y++){
					
			if ((APIDatanV[x])[y] == (SourceDatanV[x])[y]) {
					
				KeywordUtil.markPassed("Value " + (APIDatanV[x])[y] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + (APIDatanV[x])[y] + " has different Value from database = " + (SourceDatanV[x])[y])
			}
				
		}
			
	}
	
	/* untuk cek pembentukan data pVoucher */
	
	println (GlobalVariable.Remarks)
	
	ArrayList SourceDatapV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataCompareTopVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo).replace("GVRemark", GlobalVariable.Remarks))
	
	ArrayList APIDatapV = GEN5.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetpVoucher.replace("GVvoucherno",VoucherNo))
	
	if (GlobalVariable.VoucherTypeCodeBefore == "RC"){
		
		SourceDatapV[3] = 'RV-01-R'		
		
	} else if (GlobalVariable.VoucherTypeCodeBefore == "PS"){
		
		SourceDatapV[3] = 'PS-01'	
		
	} else if (GlobalVariable.VoucherTypeCodeBefore == "PY"){
	
		SourceDatapV[3] = 'PV-01-P'
		
	}
	
	SourceDatapV[4] = GlobalVariable.Remarks
	
	println (APIDatapV)
	
	println (SourceDatapV)
	
	println (APIDatapV.size().toString() +" - "+  SourceDatapV.size().toString())
	
	WebUI.verifyMatch(APIDatapV.size().toString(), SourceDatapV.size().toString(), false)
	
	for (k = 0; k < SourceDatapV.size(); k++){
		
		if (APIDatapV[k] == SourceDatapV[k]) {
			
			KeywordUtil.markPassed("Value " + APIDatapV[k] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDatapV[k] + " has different Value from database = " + SourceDatapV[k])
		}
	}
	
	/* untuk cek pembentukan data fVoucher */
	
	ArrayList SourceDatafV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataCompareTofVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo).replace("GVRemark", GlobalVariable.Remarks))
	
	ArrayList APIDatafV = GEN5.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetfVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
	if (GlobalVariable.VoucherTypeCodeBefore == "RC"){ 
		
		SourceDatafV[4] = 'RV-01-R'		
		
	} else if (GlobalVariable.VoucherTypeCodeBefore == "PS"){
	
		SourceDatafV[4] = 'PS-01'
		
	} else if (GlobalVariable.VoucherTypeCodeBefore == "PY"){
	
		SourceDatafV[4] = 'PV-01-P'
		
	}
	
	SourceDatafV[1] = '1' //set cancelstatus
	
	SourceDatafV[5] = GlobalVariable.Remarks
	
	println (APIDatafV)
	
	println (SourceDatafV)
	
	println (APIDatafV.size().toString() +" - "+  SourceDatafV.size().toString())
	
	WebUI.verifyMatch(APIDatafV.size().toString(), SourceDatafV.size().toString(), false)
	
	for (l = 0; l < SourceDatafV.size(); l++){
		
		if (APIDatafV[l] == SourceDatafV[l]) {
			
			KeywordUtil.markPassed("Value " + APIDatafV[l] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDatafV[l] + " has different Value from database = " + SourceDatafV[l])
		}
	}
	
}
		

