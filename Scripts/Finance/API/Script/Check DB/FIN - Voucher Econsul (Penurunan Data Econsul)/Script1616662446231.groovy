import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
import java.text.DecimalFormat
import java.text.NumberFormat
import java.math.RoundingMode

DecimalFormat df = new DecimalFormat("#.####")

DecimalFormat de = new DecimalFormat("#.##")

DecimalFormat dg = new DecimalFormat("#.###")

df.setRoundingMode(RoundingMode.HALF_UP);

de.setRoundingMode(RoundingMode.HALF_UP);

dg.setRoundingMode(RoundingMode.DOWN);

if (Proses == "Delay VS18"){

	def StatusPS = ''

	while (StatusPS != '2') {

		KeywordUtil.logInfo('Processing still run')

		Thread.sleep(15000)

		StatusPS = GEN5.getValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetStatusPSEconsul.replace("GVreferenceno", GlobalVariable.VoucherNo),"BulkSettlementStatusID")

		println (StatusPS)

	}

	KeywordUtil.markPassed('Processing has finished')

	Thread.sleep(1)
	
} else if (Proses == "CreateVoucher.PS"){

	/* untuk cek pembentukan data GenerateEConsultationVoucherHistory */

	def APIDataGEC = GEN5.getValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GeneratePSEconsul.replace("GVvoucherno",GlobalVariable.VoucherNo), "requestCode")
	
	println (APIDataGEC)
	
	if (APIDataGEC == GlobalVariable.RequestCode) {
		
		KeywordUtil.markPassed("Value " + APIDataGEC +" from API same with Database.")
			
	} else {
			
		KeywordUtil.markFailedAndStop("Value from API = " + APIDataGEC + " has different Value from database")
	}
		
	/* untuk cek pembentukan data FinanceNoteAdditional */
	
	println (GlobalVariable.VoucherNo)
	
	def SourceDataFNA
	
	if (Status == "VS19"){
	
		SourceDataFNA = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNotes.replace("GVnotenumber",GlobalVariable.ParamNoteNumber.replace('"', "'")).replace("Nominal,'1' isfinalsettled","'0.0000' as Nominal,'0' isfinalsettled"))
	
	} else if (Status == "VS18"){
	
		SourceDataFNA = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNotes.replace("GVnotenumber",GlobalVariable.ParamNoteNumber.replace('"', "'")))
	
	}
	
	ArrayList APIDataFNA = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNoteAdditional.replace("GVvoucherno",GlobalVariable.VoucherNo))
			
	println (APIDataFNA)
	
	println (SourceDataFNA)
	
	WebUI.verifyMatch(APIDataFNA.size().toString(), SourceDataFNA.size().toString(), false)
	
	for (j = 0; j < SourceDataFNA.size(); j++){
		
		for (k = 0; k < SourceDataFNA[0].size(); k++){
					
			if ((APIDataFNA[j])[k] == (SourceDataFNA[j])[k]) {
					
				KeywordUtil.markPassed("Value " + (APIDataFNA[j])[k] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFNA[j])[k] + " has different Value from database = " + (SourceDataFNA[j])[k])
			}
				
		}
			
	}
	
	/* untuk cek pembentukan data nota SST dan CMT */
	
	ArrayList APIDataSSTCMT = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceNotesSSTCMT.replace("GVVoucherNo",GlobalVariable.VoucherNo))
	
	ArrayList SourceDataCMT = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesCMT.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	if (SourceDataCMT != null){
		
		ArrayList SourceDataSSTPPH = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesSSTPPH.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
		ArrayList SourceDataSSTPPN = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceNotesSSTPPN.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
		println (SourceDataSSTPPN)
		
		ArrayList SourceDataSSTCMT = new ArrayList()
			
		// add array nota CMT
		for (T = 0; T < SourceDataCMT.size(); T++){
				
			SourceDataSSTCMT.add(SourceDataCMT[T])
				
		}
		
		if (SourceDataSSTPPH != null){
			
			// add array nota SST PPH
			for (H = 0; H < SourceDataSSTPPH.size(); H++){
				
				SourceDataSSTCMT.add(SourceDataSSTPPH[H])
				
			}
		}
		
		if (SourceDataSSTPPN != null){
			
			// add array nota SST PPN
			for (N = 0; N < SourceDataSSTPPN.size(); N++){
				
				SourceDataSSTCMT.add(SourceDataSSTPPN[N])
				
			}
		}
		
		println (APIDataSSTCMT)
		
		println (SourceDataSSTCMT)
		
		WebUI.verifyMatch(APIDataSSTCMT.size().toString(), SourceDataSSTCMT.size().toString(), false)
		
		for (ij = 0; ij < SourceDataSSTCMT.size(); ij++){
			
			for (ik = 0; ik < SourceDataSSTCMT[0].size(); ik++){
						
				if ((APIDataSSTCMT[ij])[ik] == (SourceDataSSTCMT[ij])[ik]) {
						
					KeywordUtil.markPassed("Value " + (APIDataSSTCMT[ij])[ik] +" from API same with Database.")
						
				} else {
						
					KeywordUtil.markFailedAndStop("Value from API = " + (APIDataSSTCMT[ij])[ik] + " has different Value from database = " + (SourceDataSSTCMT[ij])[ik])
				}
					
			}
				
		}
		
	} else {
	
		if (APIDataSSTCMT == null) {
				
			KeywordUtil.markPassed("Value " + APIDataSSTCMT +" from API same with Database.")
				
		} else {
				
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataSSTCMT + " has different Value from database null")
		}
	
	}
	
	/* untuk cek pembentukan data VoucherDetail */
	
	ArrayList APIDataVD = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherDetail.replace("GVvoucherno", GlobalVariable.VoucherNo))
		
	ArrayList SourceDataVD = new ArrayList()
		
	SourceDataVD = GlobalVariable.GetDataCompareToVD

	for (b = 0; b < SourceDataVD.size(); b++){
		
		SourceDataVD[b][37] = GlobalVariable.username[0] // set CreatedBy
		
		if (SourceDataVD[b][9] == "EXS"){
						
			SourceDataVD[b][7] = GlobalVariable.ParamList1[0][1] //set RequestAmount
			
			SourceDataVD[b][29] = GlobalVariable.ParamList1[0][1] //set CashAmount
				
			SourceDataVD[b][30] = GlobalVariable.ParamList1[0][1] //set SettlementAmount
			
		}

	}
	
	println (SourceDataVD)
	
	println (APIDataVD)
	
	println (APIDataVD.size().toString() +" - "+  SourceDataVD.size().toString())
		
	WebUI.verifyMatch(APIDataVD.size().toString(), SourceDataVD.size().toString(), false)
	
	WebUI.verifyMatch(APIDataVD[0].size().toString(), SourceDataVD[0].size().toString(), false)
		
	for (k = 0; k < SourceDataVD.size(); k++){
			
		for (l = 0; l < SourceDataVD[0].size(); l++){
			
			if (APIDataVD[k][l] == SourceDataVD[k][l]) {
					
				KeywordUtil.markPassed("Value " + APIDataVD[k][l] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + APIDataVD[k][l] + " has different Value from database = " + SourceDataVD[k][l])
			}
		}
	}
	
	/* untuk cek pembentukan data Voucher */
	
	ArrayList SourceDataV = new ArrayList()
	
	GlobalVariable.AABBankAccountID = '167'
	
	println (GlobalVariable.FIN_GetDataCompareToVoucher.replace("GVAABAccountID",GlobalVariable.AABBankAccountID.toString()))
	
	SourceDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVoucher.replace("GVAABAccountID",GlobalVariable.AABBankAccountID.toString()))
	
	println (GlobalVariable.username)
	
	println (GlobalVariable.BEY_GetCdEntity.replace("GVlogid",GlobalVariable.username[0]))
	
	GlobalVariable.CdEntity = GEN5.getValueDatabase("172.16.94.70", "BeyondDB_Apr2020", GlobalVariable.BEY_GetCdEntity.replace("GVlogid",GlobalVariable.username[0]),"cdEntity")

	double Nominal = 0.0000
	
	double Diff = 0.0000
	
	for (y = 0; y < GlobalVariable.GetDataCompareToVD.size(); y++){
		
		if (GlobalVariable.GetDataCompareToVD[y][4] == '1'){
			
			Nominal += GlobalVariable.GetDataCompareToVD[y][29].toDouble()
			
		} else {
		
			Diff += GlobalVariable.GetDataCompareToVD[y][29].toDouble()
		}
	}
	
	println (Nominal)
	
	println (Diff)
	
	def NominalFix = de.format(Nominal)
	
	def DiffFix = de.format(Diff)
	
	if (Nominal > Diff){
		
		SourceDataV[13] = NominalFix.toString() //set nominal
		
		if (Diff > 0){
			
			SourceDataV[14] = DiffFix.toString() //set diff
			
		} else {
		
			SourceDataV[14] = '0.00' //set diff
			
		}
		
	} else if (Nominal == Diff){
	
		SourceDataV[13] = NominalFix.toString() //set nominal
	
		SourceDataV[14] = DiffFix.toString() //set diff
		
	} else {
	
		SourceDataV[13] = DiffFix.toString() //set nominal
		
		SourceDataV[14] = '0.00' //set diff
	}
				
	SourceDataV[2] = '0' //set IsGeneratedBySUNGL
				
	SourceDataV[3] = '3' //set VoucherTypeID
	
	if (Status == "VS19"){
	
		SourceDataV[4] = '19' //set VoucherStatusID
		
		SourceDataV[0] = 'null' // set SettlementDate (krn voucher pertama kali dibentukin tdk langsung 18, jadi statusnya 19)
	
	} else if (Status == "VS18"){
		
		SourceDataV[4] = '18' //set VoucherStatusID
			
	}
				
	SourceDataV[5] = '19' //set ExpectedVoucherStatusID
	
	SourceDataV[6] = '13' //set VoucherCreatedTypeID 13: econsul
	
	SourceDataV[10] = GlobalVariable.PaymentMethodID.toString() //set PaymentMethodID
	
	SourceDataV[12] = GlobalVariable.Remarks //set Remark
	
	SourceDataV[23] = 'IDR' // Set PayerAccountCurrencyCode
	
	SourceDataV[39] = '7' // Set ChannelID
	
	SourceDataV[43] = '0' //set EntryUserInternalDistributionID
	
	SourceDataV[45] = GlobalVariable.username[0] //set CreatedBy
		
	SourceDataV[47] = GlobalVariable.username[0] //set ModifiedBy , 0 : yg settlein service juga

	SourceDataV[51] = ''
	
	SourceDataV[52] = ''
	
	ArrayList APIDataV = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo))
	
	APIDataV[13] = de.format(APIDataV[13].toDouble()) //set dg format yg sama
	
	if (APIDataV[14].toDouble() > 0){
		
		APIDataV[14] = de.format(APIDataV[14].toDouble()) //set dg format yg sama
	
	}
	
	println (APIDataV)
	
	println (SourceDataV)
	
	println (APIDataV.size().toString() +" - "+  SourceDataV.size().toString())
	
	WebUI.verifyMatch(APIDataV.size().toString(), SourceDataV.size().toString(), false)
	
	for (c = 0; c < SourceDataV.size(); c++){
		
		if (APIDataV[c] == SourceDataV[c]) {
			
			KeywordUtil.markPassed("Value " + APIDataV[c] +" from API same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataV[c] + " has different Value from database = " + SourceDataV[c])
		}
	}
			
	/* untuk cek pembentukan data FinanceSettlements */
	
	if (Status == "VS19"){
		
		ArrayList APIDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceSettlements.replace("GVvoucherno",GlobalVariable.VoucherNo))
				
		println (APIDataFS)
	
		if (APIDataFS == null) {
			
			KeywordUtil.markPassed("Value " + APIDataFS +" from API same with Database.")
				
		} else {
				
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataFS + " has different Value from database null")
		}
	
	} else if (Status == "VS18"){
		
		ArrayList SourceDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToFinanceSettlements.replace("GVnotenumber",GlobalVariable.ParamNoteNumber.replace('"', "'")))
		
		ArrayList APIDataFS = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetFinanceSettlements.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
		for (jj = 0; jj < SourceDataFS.size(); jj++){
			
			def FinanceNoteID
			
			def Overshortage
			
			for (jb = 0; jb < GlobalVariable.GetDataCompareToVD.size(); jb++){
								
				if (GlobalVariable.GetDataCompareToVD[jb][9] == "EXS"){
								
					FinanceNoteID = GlobalVariable.GetDataCompareToVD[jb][0]
					
					Overshortage = GlobalVariable.GetDataCompareToVD[jb][19]
					
				}
		
			}
			
			if (SourceDataFS[jj][0] == FinanceNoteID){
				
				SourceDataFS[jj][3] = de.format(GlobalVariable.ParamList1[0][1].toDouble()) // set SettlementAmount
				
				SourceDataFS[jj][9] = de.format(Overshortage.toDouble()) // set Overshortage
			}
			
			SourceDataFS[jj][3] = de.format(SourceDataFS[jj][3].toDouble()) // set SettlementAmount
			
			SourceDataFS[jj][9] = de.format(SourceDataFS[jj][9].toDouble()) // set Overshortage
				
			APIDataFS[jj][3] = de.format(APIDataFS[jj][3].toDouble()) // set SettlementAmount
				
			APIDataFS[jj][9] = de.format(APIDataFS[jj][9].toDouble()) // set Overshortage
			
			SourceDataFS[jj][4] = GlobalVariable.username[0] // set CdHandBy
			
			SourceDataFS[jj][13] = GlobalVariable.username[0] // set CreatedBy
				
		}
		
		println (APIDataFS)
		
		println (SourceDataFS)
		
		WebUI.verifyMatch(APIDataFS.size().toString(), SourceDataFS.size().toString(), false)
		
		for (d = 0; d < SourceDataFS.size(); d++){
			
			for (e = 0; e < SourceDataFS[0].size(); e++){
						
				if ((APIDataFS[d])[e] == (SourceDataFS[d])[e]) {
						
					KeywordUtil.markPassed("Value " + (APIDataFS[d])[e] +" from API same with Database.")
						
				} else {
						
					KeywordUtil.markFailedAndStop("Value from API = " + (APIDataFS[d])[e] + " has different Value from database = " + (SourceDataFS[d])[e])
				}
					
			}
				
		}
		
	}
	
	/* untuk cek pembentukan data vouchersettlementbackhistory */
	
	if (Status == "VS19"){
	
		ArrayList APIDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherSettlementBackHistory.replace("GVvoucherno",GlobalVariable.VoucherNo))
		
		println (APIDataVSH)
		
		if (APIDataVSH == null) {
			
			KeywordUtil.markPassed("Value " + APIDataVSH +" from API same with Database.")
				
		} else {
				
			KeywordUtil.markFailedAndStop("Value from API = " + APIDataVSH + " has different Value from database null")
		}
	
	} else if (Status == "VS18"){
	
		ArrayList SourceDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetDataCompareToVoucherSettlementBackHistory)
	
		def StatusSettleback = ''
		
			while (StatusSettleback != '1') {
		
				KeywordUtil.logInfo('Processing still run')
		
				Thread.sleep(1000)
		
				StatusSettleback = GEN5.getValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherSettlementBackHistory.replace("GVvoucherno",GlobalVariable.VoucherNo),"processstatus")
		
				println (StatusSettleback)
		
			}
		
		KeywordUtil.markPassed('Processing has finished')
		
		Thread.sleep(1)
		
		ArrayList APIDataVSH = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherSettlementBackHistory.replace("GVvoucherno",GlobalVariable.VoucherNo))
			
		SourceDataVSH[1] = '3' //set VoucherTypeID
		
		println (APIDataVSH)
		
		println (SourceDataVSH)
		
		println (APIDataVSH.size().toString() +" - "+  SourceDataVSH.size().toString())
		
		WebUI.verifyMatch(APIDataVSH.size().toString(), SourceDataVSH.size().toString(), false)
		
		for (k = 0; k < SourceDataVSH.size(); k++){
			
			if (APIDataVSH[k] == SourceDataVSH[k]) {
				
				KeywordUtil.markPassed("Value " + APIDataVSH[k] +" from API same with Database.")
				
			} else {
				
				KeywordUtil.markFailedAndStop("Value from API = " + APIDataVSH[k] + " has different Value from database = " + SourceDataVSH[k])
			}
		}
	}
	
} else if (Proses == "GetVoucherRO"){

	println (FIN_GetDataVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo).replace("(ModifiedDate","(GetDate()"))
	
	GlobalVariable.ParamList = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB",FIN_GetDataVoucher.replace("GVvoucherno",GlobalVariable.VoucherNo).replace("(ModifiedDate","(GetDate()"))
	
	println (GlobalVariable.ParamList)
		
} else if (Proses == "Penurunan EConsultationAutomaticVoucherQueue"){

	/* untuk cek pembentukan data Finance.EConsultationAutomaticVoucherQueue */

	println (GlobalVariable.SEA_GetDataEConsultationAutomaticVoucherQueue.replace("GVTransactionReference", GlobalVariable.BookingNo))

	ArrayList SEA_EConsulAutomaticVoucherQueue = new ArrayList()
	
	SEA_EConsulAutomaticVoucherQueue = GEN5.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetDataEConsultationAutomaticVoucherQueue.replace("GVTransactionReference", GlobalVariable.BookingNo))
	
	ArrayList FIN_EConsulAutomaticVoucherQueue = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataEConsultationAutomaticVoucherQueue.replace("dbo.ECons", "Finance.ECons").replace("GVTransactionReference", GlobalVariable.BookingNo))
		
	println (FIN_EConsulAutomaticVoucherQueue)
	
	def StatusPenurunan1 = FIN_EConsulAutomaticVoucherQueue
	
		while (StatusPenurunan1 == null) {
	
			KeywordUtil.logInfo('Processing still run')
	
			Thread.sleep(50000)
	
			StatusPenurunan1 = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataEConsultationAutomaticVoucherQueue.replace("dbo.ECons", "finance.ECons").replace("GVTransactionReference", GlobalVariable.BookingNo))
	
			println (StatusPenurunan1)
	
		}
	
	KeywordUtil.markPassed('Processing has finished')
	
	Thread.sleep(1)
	
	FIN_EConsulAutomaticVoucherQueue = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataEConsultationAutomaticVoucherQueue.replace("dbo.ECons", "Finance.ECons").replace("GVTransactionReference", GlobalVariable.BookingNo))
	
	println (FIN_EConsulAutomaticVoucherQueue)
	
	def StatusPenurunan2 = FIN_EConsulAutomaticVoucherQueue.size()
	
	while (StatusPenurunan2 != SEA_EConsulAutomaticVoucherQueue.size()) {
			
		println (StatusPenurunan2 + " | " + SEA_EConsulAutomaticVoucherQueue.size())
	
		KeywordUtil.logInfo('Processing still run')
	
		Thread.sleep(20000)
	
		FIN_EConsulAutomaticVoucherQueue = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataEConsultationAutomaticVoucherQueue.replace("dbo.ECons", "finance.ECons").replace("GVTransactionReference", GlobalVariable.BookingNo))
	
		StatusPenurunan2 = FIN_EConsulAutomaticVoucherQueue.size()
				
	}
	
	KeywordUtil.markPassed('Processing has finished')
	
	Thread.sleep(1)	
	
	def StatusVoucherSEA = ''
	
	def StatusVoucherFIN = ''
	
	for (x = 0; x < SEA_EConsulAutomaticVoucherQueue.size(); x++){
			
		while (StatusVoucherSEA == '' || StatusVoucherFIN == '') {
			
			println (StatusVoucherSEA + " | " + StatusVoucherFIN)
		
			KeywordUtil.logInfo('Processing still run')
		
			Thread.sleep(1000)
		
			def aa = GEN5.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetDataEConsultationAutomaticVoucherQueue.replace("GVTransactionReference", GlobalVariable.BookingNo))
		
			def bb = GEN5.getAllDataDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.SEA_GetDataEConsultationAutomaticVoucherQueue.replace("dbo.ECons", "finance.ECons").replace("GVTransactionReference", GlobalVariable.BookingNo))
			
			StatusVoucherSEA = aa[x][11]
			
			StatusVoucherFIN = bb[x][11]
			
			println (StatusVoucherSEA + " | " + StatusVoucherFIN)
		
		}
	}
			
	KeywordUtil.markPassed('Processing has finished')
	
	Thread.sleep(1)
		
	SEA_EConsulAutomaticVoucherQueue = GEN5.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.SEA_GetDataEConsultationAutomaticVoucherQueue.replace("GVTransactionReference", GlobalVariable.BookingNo))
	
	for (ad = 0; ad < SEA_EConsulAutomaticVoucherQueue.size(); ad++){
			
		def FinanceCodeAABAccount = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetMappingAABAccountSEA.replace("GVcorecode",SEA_EConsulAutomaticVoucherQueue[ad][1]))
		
		def FinanceCodePaymentMethod = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetMappingAABAccountSEA.replace("GVcorecode",SEA_EConsulAutomaticVoucherQueue[ad][5]))
				
		SEA_EConsulAutomaticVoucherQueue[ad][1] = FinanceCodeAABAccount[0]
		
		SEA_EConsulAutomaticVoucherQueue[ad][5] = FinanceCodePaymentMethod[0]
	}
	
	GlobalVariable.EConsulAutomaticVoucherQueueList = SEA_EConsulAutomaticVoucherQueue
		
	println (SEA_EConsulAutomaticVoucherQueue)
	
	println (FIN_EConsulAutomaticVoucherQueue)	
	
	WebUI.verifyMatch(FIN_EConsulAutomaticVoucherQueue.size().toString(), SEA_EConsulAutomaticVoucherQueue.size().toString(), false)
	
	WebUI.verifyMatch(FIN_EConsulAutomaticVoucherQueue[0].size().toString(), SEA_EConsulAutomaticVoucherQueue[0].size().toString(), false)
	
	for (k = 0; k < SEA_EConsulAutomaticVoucherQueue.size(); k++){
		
		for (l = 0; l < SEA_EConsulAutomaticVoucherQueue[0].size(); l++){
			
			if (SEA_EConsulAutomaticVoucherQueue[k][l] == FIN_EConsulAutomaticVoucherQueue[k][l]) {
					
				KeywordUtil.markPassed("Value " + FIN_EConsulAutomaticVoucherQueue[k][l] +" from API same with Database.")
					
			} else {
					
				KeywordUtil.markFailedAndStop("Value from API = " + FIN_EConsulAutomaticVoucherQueue[k][l] + " has different Value from database = " + SEA_EConsulAutomaticVoucherQueue[k][l])
			}
		}
	}
		
}
		
