import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

UI.SetGlobal("endpoint", "Finance")

UI.SetGlobal("endpointlogin", "Finance2")

def VoucherNo

def NoteNumber = '00205/CN/JKT00/11/20'

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Precondition PI dan Penurunan Data'), [('Proses') : 'Initial Select FIN'
        , ('NoteNumber') : NoteNumber], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Precondition PI dan Penurunan Data'), [('Proses') : 'Initial Delete SEA'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Precondition PI dan Penurunan Data'), [('Proses') : 'Initial Update SEA'
        , ('NoteNumber') : NoteNumber], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Precondition PI dan Penurunan Data'), [('Proses') : 'Initial Delete FIN'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Precondition PI dan Penurunan Data'), [('Proses') : 'Initial Update FIN'
        , ('NoteNumber') : NoteNumber, ('PaidAmount') : 0, ('IsFinalSettled') : 0], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login - FinanceAPI'), [('Username') : 'ZmluYW5jZWFwaUBhc3VyYW5zaWFzdHJhLmNvbQ==', ('Password') : 'RmluYW5jZUFQSTk1'
	, ('username') : 'financeapi@asuransiastra.com'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Payment Instrument/GenerateVoucher'), [('ParamGenerateVoucher') : GlobalVariable.ParamGenerateVoucherSinglePayment], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Create Voucher PI dan Penurunan Data'), [('Proses') : 'GenerateVoucher Finance.Voucher'
        , ('NoteNumber') : NoteNumber, ('VoucherStatusID') : 3], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SubmitWorklist'), [('Skenario') : 'CreateVoucher PaymentInstrument'
        , ('Action') : 'validatedWithPI'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Create Voucher PI dan Penurunan Data'), [('Proses') : 'GenerateVoucher Finance.Voucher'
        , ('NoteNumber') : NoteNumber, ('VoucherStatusID') : 4], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'RVVT', ('username') : 'EUS'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Payment Instrument/SavePaymentInstrument'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Create Voucher PI dan Penurunan Data'), [('Proses') : 'GenerateVoucher Finance.Voucher'
	, ('NoteNumber') : NoteNumber, ('VoucherStatusID') : 10], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Payment Instrument/SubmitFiatBayar'), [('Action') : 'Submit_Fiat_Bayar'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Create Voucher PI dan Penurunan Data'), [('Proses') : 'GenerateVoucher Finance.Voucher'
	, ('NoteNumber') : NoteNumber, ('VoucherStatusID') : 11], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'SUxO', ('username') : 'ILN'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Payment Instrument/SubmitPIApproval'), [('Action') : 'approve'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Create Voucher PI dan Penurunan Data'), [('Proses') : 'GenerateVoucher Finance.Voucher'
	, ('NoteNumber') : NoteNumber, ('VoucherStatusID') : 12], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Payment Instrument/SubmitPIApproval'), [('Action') : 'approve'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Create Voucher PI dan Penurunan Data'), [('Proses') : 'GenerateVoucher Finance.Voucher'
	, ('NoteNumber') : NoteNumber, ('VoucherStatusID') : 18], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Create Voucher PI dan Penurunan Data'), [('Proses') : 'GenerateVoucher Finance.FinanceNoteAdditional'
	, ('NoteNumber') : NoteNumber, ('IsFinalSettled') : 1], FailureHandling.STOP_ON_FAILURE)




