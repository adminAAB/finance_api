import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI
import com.keyword.GEN5
import com.keyword.UI

UI.SetGlobal("endpoint", "Finance")

UI.SetGlobal("endpointlogin", "Finance2")

String DataParam = GEN5.getValueDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetVoucherForInquiry.replace("taxtypeid ='4'","taxtypeid ='0'").replace("vouchertypeid='2'","vouchertypeid='10'"), "voucherno") //JKT00FRC20000472
	

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'RFZJ', ('username') : 'DVI'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Inquiry Voucher/GetVoucher'), [('Param') : DataParam
	, ('Skenario') : 'Inquiry Voucher' , ('Filter') : 'VoucherNo'], FailureHandling.STOP_ON_FAILURE)
