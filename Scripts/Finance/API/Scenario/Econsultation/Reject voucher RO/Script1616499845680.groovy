import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI

UI.SetGlobal("endpoint", "Finance")

UI.SetGlobal("endpointlogin", "Finance2")

def Param = [['1','AUTO','GoPay','350000',''],['2','','','','']] //Skenario,Booking,PaymentMethod(CC,VA,GoPay,ShopeePay),Nominal,Deskripsi

GlobalVariable.ParamAll = Param

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Econsultation/Create Voucher RO'), [("Step"): 'Get Member Valid'], FailureHandling.STOP_ON_FAILURE)

for (i = 0; i < Param.size(); i++){
	
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Econsultation/Create Voucher RO'), [("Step"): 'Insert/Reschedule Biaya Dokter', ("ParamPG") : Param[i]], FailureHandling.STOP_ON_FAILURE)

}

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Voucher Econsul (Penurunan Data Econsul)'), [('Proses'):'Penurunan EConsultationAutomaticVoucherQueue'], FailureHandling.STOP_ON_FAILURE)

println (GlobalVariable.EConsulAutomaticVoucherQueueList)

//GlobalVariable.ParamList = [JournalTypeCode , SubJournalTypeCode , SuspendResultCategoryCode , Amount]

for (j = 0; j < GlobalVariable.EConsulAutomaticVoucherQueueList.size(); j++){
	
	println (GlobalVariable.EConsulAutomaticVoucherQueueList[j][11])
	
	GlobalVariable.VoucherNo = GlobalVariable.EConsulAutomaticVoucherQueueList[j][11]
	
	GlobalVariable.FIN_DataSuspendCategoryEconsul = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetSuspendCategoryEconsul.replace("GVVoucherNo", GlobalVariable.VoucherNo))
	
	GlobalVariable.EConsulAutomaticVoucherRO = GlobalVariable.EConsulAutomaticVoucherQueueList[j] 
	
	GlobalVariable.username.add('SYS')
	
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'RFZJ', ('username') : 'DVI'], FailureHandling.STOP_ON_FAILURE)
			
	WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Voucher Econsul (Penurunan Data Econsul)'), [('Proses'):'GetVoucherRO'], FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Worklist/Worklist OOF'), [('Skenario') : 'Create'], FailureHandling.STOP_ON_FAILURE)
	
	GlobalVariable.Action = 'reject'
				
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : GlobalVariable.EncriptUsername, ('username') : GlobalVariable.NextUser], FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/GetVoucher'), [('Skenario') : 'Create'], FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SubmitWorklist'), [('Skenario') : 'CreateVoucher'
		, ('Action') : GlobalVariable.Action], FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Voucher Econsul'), [('Proses'):'RejectVoucher'], FailureHandling.STOP_ON_FAILURE)
	
	GlobalVariable.username = []
}
