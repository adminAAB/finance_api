import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

UI.SetGlobal('endpoint', 'Finance')

UI.SetGlobal('endpointlogin', 'Finance2')

String ParamNote

if (UI.verifyStaging()) {
	
    ParamNote = ''
	
} else {

    ParamNote = "00018/DN/JKT00/03/21','21FSCXDRCCA0037" 
	
}

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Precons Voucher Econsul'), [('Skenario') :'PS', ('NoteNumber') : ParamNote], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'RVVT', ('username') : 'EUS'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Voucher Econsul'), [('Proses') : 'GetNoteData'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/GenerateEConsultationVoucher'), [('NoteNumber') : ParamNote], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Voucher Econsul (Penurunan Data Econsul)'), [('Proses') : 'CreateVoucher.PS', ('Status') : 'VS19'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Voucher Econsul (Penurunan Data Econsul)'), [('Proses') : 'Delay VS18'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Voucher Econsul (Penurunan Data Econsul)'), [('Proses') : 'CreateVoucher.PS', ('Status') : 'VS18'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Voucher Econsul (Penurunan Data Econsul)'), [('Proses') : 'CreateVoucher.PS' ], FailureHandling.STOP_ON_FAILURE)

