import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI
import com.keyword.API


//RELATE ISSUE DI JIRA NO 4
//https://jiraforit.atlassian.net/browse/AF-747
//Kalau status diubah jd 2, dan d query juga d ubah lolos

import java.text.SimpleDateFormat
String strEndDate = new SimpleDateFormat('dd/MM/yyyy').format(Calendar.getInstance().getTime())
API.Note(strEndDate)
String strStartDate = new SimpleDateFormat('dd/MM/yyyy').format(Calendar.getInstance().getTime()-365)
API.Note(strStartDate)

UI.SetGlobal("endpoint", "Finance")
UI.SetGlobal("endpointlogin", "Finance2")
WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), 
	[('Username') : 'RFZJ', 
		('username') : 'DVI'],
	FailureHandling.STOP_ON_FAILURE)

API.Note(GlobalVariable.Token)

String[] GetEmp = UI.getOneColumnDatabase("172.16.94.70", "SEA", Query, "EmployeeLogID")
API.Note(GetEmp)
int random = new Random().nextInt(GetEmp.length)
String strEmp = GetEmp[random].toString()

API.Note(strEmp)

def var = WS.sendRequest(findTestObject('Object Repository/OutOfOffice/GetOutOfOfficeUserList',
	[('strEmp') : strEmp,
	('strActEmp') : '',
	('strStatus') : '1',
	('strStartDate') : strStartDate,
	('strEndDate') : strEndDate]))

if (var.getStatusCode() == 200 ) { 
	API.Note(var.getResponseBodyContent()) 
	
	if (API.getResponseData(var).status == true) { 
		API.Note("Jumlah Data : " + API.getResponseData(var).data.items.outOfOficeID.size())
		API.Note("Example Data pertama : " + API.getResponseData(var).data.items[0].employeeName)
		
		API.Note('API Success') 
	} else { 
		fail(API.Note('API Failed'))
	}
} else  {
	fail(API.Note('API Error Server - ' + var.getStatusCode()))
} 

GlobalVariable.username = []