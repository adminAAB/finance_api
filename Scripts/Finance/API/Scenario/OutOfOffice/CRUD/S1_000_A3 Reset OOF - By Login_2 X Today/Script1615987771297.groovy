import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI
import com.keyword.API

// DVI (RFZJ/3877) - ILN (SUxO/6977) - LWS (TFdT/3862)

String LoginID = "3877"

String test = "UPDATE [BeyondDB_Apr2020].[General].[A2isFinanceOutOfOffice] SET ActiveStatusID = '2' WHERE EmployeeID = '"+ LoginID +"' AND ActiveStatusID = '1' AND Startdate = convert(date ,getdate()) AND EndDate = convert(date ,getdate())"
API.Note(test)

UI.updateValueDatabase("172.16.94.70", "BeyondDB_Apr2020", test)

		
