import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI
import com.keyword.API

import java.text.SimpleDateFormat
String strGetDate = new SimpleDateFormat('dd/MM/yyyy').format(Calendar.getInstance().getTime()+7)
API.Note(strGetDate)

UI.SetGlobal("endpoint", "Finance")
UI.SetGlobal("endpointlogin", "Finance2")

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), 
	[('Username') : 'TFdT', // DVI (RFZJ/3877) - ILN (SUxO/6977) - LWS (TFdT/3862)
		('username') : 'LWS'],
	FailureHandling.STOP_ON_FAILURE)

String LoginID = "3862"

API.Note(GlobalVariable.Token)

String[] GetEmp0 = UI.getOneColumnDatabase("172.16.94.70", "SEA", Query2, "EmployeeID")
API.Note(GetEmp0)
int random = new Random().nextInt(GetEmp0.length)
String strActEmp = GetEmp0[random].toString()

API.Note(strActEmp)

ArrayList GetEmp = UI.getOneRowDatabase("172.16.94.70", "SEA", Query.replace('_EmployeeID_',LoginID))
API.Note(GetEmp)
if (GetEmp == null){
	fail(API.Note("Lakukan OOF terlebih dahulu! Tidak ada data OOF EmployeeID sesuai LoginID : "+ LoginID))
}
API.Note(GetEmp[0])
API.Note(GetEmp[4])
API.Note(GetEmp[5])
API.Note(GetEmp[6])

def var = WS.sendRequest(findTestObject('Object Repository/OutOfOffice/AddOrUpdateOutOfOffice',
	[('strOOFID') : GetEmp[0],
	('strActEmp') : strActEmp,
	('strStartDate') : strGetDate,
	('strEndDate') : strGetDate,
	('strStatus') : 1,
	('strActionType') : 2]))


if (var.getStatusCode() == 200 ) { 
	API.Note(var.getResponseBodyContent()) 
	
	String test = "SELECT TOP 1000 * FROM [BeyondDB_Apr2020].[General].[vA2isFinanceOutOfOfficeStatus] o LEFT JOIN  [BeyondDB_Apr2020].[General].[Employees] e ON o.employeeID = e.employeeId WHERE OutOfOfficeID IN ("+ GetEmp[0] +") "
	API.Note(test)
	ArrayList table = UI.getOneRowDatabase("172.16.94.70", "BeyondDB_Apr2020", test)
	API.Note(table)
	
	if ((table[5] == GetEmp[5]) && (table[6] == GetEmp[6])) { 
		API.Note('Nilai Date tidak berubah; After : '+ table[5] +' Before : '+GetEmp[5])
		
		API.Note('API Success') 
	} else { 
		fail(API.Note('API Failed'))
	}
} else  {
	fail(API.Note('API Error Server - ' + var.getStatusCode()))
} 
