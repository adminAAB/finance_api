import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.REA

String strDistCode = 'JKT00'
String strTypeCode = 'RO'
String strUserLogin = 'EUS'
String strVoucherNo = ''
//hanya dapat dijalankan di katalon versi 7 atau versi terbaru
//hanya untuk membantu create data, pengecekkan sequence masih secara manual
for (i = 0; i <= 1000; i++) {
	checkDB = REA.getValueDatabase('172.16.94.145', 'a2isFinanceDB', qGetVoucher.replace("_InternalDistributionCode_",strDistCode)
		.replace("_VoucherTypeCode_",strTypeCode).replace("_UserLogin_",strUserLogin),'ResultVoucher')
	WebUI.comment('Data '+ i + ' : ' + checkDB)
}