import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

UI.SetGlobal("endpoint", "Finance")

UI.SetGlobal("endpointlogin", "Finance2")

String NoteNumber1 = '03844/DN/JKT00/12/19'

String NoteNumber2 = '00102/CN/JKT00/08/20'

GlobalVariable.VoucherNo = 'JKT00MV00008/GM/08/20'

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Precondition PI dan Penurunan Data'), [('Proses') : 'Initial Delete FIN - PenurunanNota'], 
    	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Precondition PI dan Penurunan Data'), [('Proses') : 'Initial Update FIN'
        , ('NoteNumber') : NoteNumber1, ('PaidAmount') : 0, ('IsFinalSettled') : 0], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Precondition PI dan Penurunan Data'), [('Proses') : 'Initial Update FIN'
		, ('NoteNumber') : NoteNumber2, ('PaidAmount') : 0, ('IsFinalSettled') : 0], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login - FinanceAPI'), [('Username') : 'ZmluYW5jZWFwaUBhc3VyYW5zaWFzdHJhLmNvbQ=='
        , ('Password') : 'RmluYW5jZUFQSTk1', ('username') : 'financeapi@asuransiastra.com'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Penurunan Data/Voucher/SubmitVoucher'), [('ParamSubmitVoucher'): GlobalVariable.ParamSubmitVoucherW2], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Create Voucher PI dan Penurunan Data'), [('Proses') : 'SubmitVoucher Finance.Voucher'], 
    	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Create Voucher PI dan Penurunan Data'), [('Proses') : 'GenerateVoucher Finance.FinanceNoteAdditional'
        , ('NoteNumber') : NoteNumber1, ('IsFinalSettled') : 1], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/FIN - Create Voucher PI dan Penurunan Data'), [('Proses') : 'GenerateVoucher Finance.FinanceNoteAdditional'
		, ('NoteNumber') : NoteNumber2, ('IsFinalSettled') : 1], FailureHandling.STOP_ON_FAILURE)

