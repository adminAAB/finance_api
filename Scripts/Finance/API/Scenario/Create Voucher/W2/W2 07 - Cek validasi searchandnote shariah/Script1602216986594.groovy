import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

UI.SetGlobal("endpoint", "Finance")

UI.SetGlobal("endpointlogin", "Finance2")

String paramNote

if (UI.verifyStaging()) {
	
	paramNote = '00002/DN/SJK0/04/15","04244/CN/SJK0/12/16'
	
} else {

	paramNote = '04285/CN/SJK0/03/16","00010/CN/SJK0/07/14'
	
}

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'RFZJ'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/Request/Create Edited Note/CreateVoucher'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Update Flag Hold Note'), [('ParamNoteNo') : paramNote, ('scenario') : 'unhold'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SearchAndAddNote'), [('Scenario') : 'InvalidSearchAndNoteW2' , ('DataNota') : 'HarcodeNota' , ('TransactionTypeID') : '5'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Update Flag Hold Note'), [('scenario') : 'hold'], FailureHandling.STOP_ON_FAILURE)

