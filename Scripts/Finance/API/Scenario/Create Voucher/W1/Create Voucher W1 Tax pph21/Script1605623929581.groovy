import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI

UI.SetGlobal("endpoint", "Finance")

UI.SetGlobal("endpointlogin", "Finance2")

String paramNote

if (UI.verifyStaging()) {
	
	paramNote = '15424/CN/JKT00/02/21'
	
} else {

	paramNote = '00105/CN/JKT00/11/20'
	
}


WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'SUxO', ('username') : 'ILN'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/CreateVoucher'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Update Flag Hold Note'), [('ParamNoteNo') : paramNote 
        , ('scenario') : 'unhold'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SearchAndAddNote'), [('Scenario') : 'CreateVoucher', ('DataNota') : 'HarcodeNota'
        , ('TransactionTypeID') : '4'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/ValidateWTVoucher'), [('VoucherTypeCode') : 'W1', ('scenario') : 'FlowVoucher'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SubmitVoucher'), [('TransactionTypeID') : 4
	, ('PaymentMethodID') : 0, ('AABBankAccountID') : 275, ('AABBankAccountNo') : '000', ('Remarks') : 'W1 Tax pph21 automate'
	, ('PayerID') : 0, ('VoucherTypeCode') : 'W1'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Check DB FIN - Create Voucher'), [('Proses') : 'GetNoteData'
	, ('Skenario') : 'NonTax'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/GetVoucher'), [('Skenario') : 'Create'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Worklist/Worklist OOF'), [('Skenario') : 'Create'], FailureHandling.STOP_ON_FAILURE)

while (GlobalVariable.Status != 'Settled'){
	
	println (GlobalVariable.Status)
	
	if (GlobalVariable.Status == 'Approved - Waiting Validation'){
		
		GlobalVariable.Action ='validated'
		
	} else if (GlobalVariable.Status == 'Validated - Waiting Settlement'){
	
		GlobalVariable.Action ='settled'
	}
	
	println (GlobalVariable.Action)
			
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : GlobalVariable.EncriptUsername, ('username') : GlobalVariable.NextUser],
			FailureHandling.STOP_ON_FAILURE)

	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SubmitWorklist'), [('Skenario') : 'CreateVoucher'
			, ('Action') : GlobalVariable.Action], FailureHandling.STOP_ON_FAILURE)
	
	GlobalVariable.Action = ''
	
	GlobalVariable.Status = ''
		
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Worklist/Worklist OOF'), [('Skenario') : 'Create'], FailureHandling.STOP_ON_FAILURE)
}

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Check DB FIN - Create Voucher'), [('Proses') : 'CreateVoucher'
	, ('Skenario') : 'NonTax'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Check DB SEA'), [('Proses') : 'CreateVoucher' ], FailureHandling.STOP_ON_FAILURE)



