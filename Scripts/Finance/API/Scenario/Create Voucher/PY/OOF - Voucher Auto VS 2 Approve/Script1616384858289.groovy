import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
import com.keyword.REA as REA


//ILN harus OOF ke JUR
//ketika JUR create voucher Premi dengan approval VS2 berjenjang maka akan terbentuk otomatis approval VS2 yang ke ILN
UI.SetGlobal('endpoint', 'Finance')

UI.SetGlobal('endpointlogin', 'Finance2')

String paramNote
String strActor
String strActingStatus
String strUserName  = 'JUR' //untuk login awal

if (UI.verifyStaging()) {
	
    paramNote = ''
	
} else {

    paramNote = '00006/DN/JKT00/01/21'
	
}

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'SlVS'
	, ('username') : strUserName], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/CreateVoucher'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Update Flag Hold Note'), [('ParamNoteNo') : paramNote, ('scenario') : 'unhold'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SearchAndAddNote'), [('Scenario') : 'CreateVoucher'
        , ('DataNota') : 'HarcodeNota', ('TransactionTypeID') : '1'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/ValidateWTVoucher'), [('VoucherTypeCode') : 'PY'
        , ('scenario') : 'FlowVoucher'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SubmitVoucher'), [('TransactionTypeID') : 1
        , ('PaymentMethodID') : 1, ('AABBankAccountID') : 62, ('AABBankAccountNo') : '0063069695', ('Remarks') : 'PY OOF Auto Approve VS2'
        , ('PayerID') : 0, ('VoucherTypeCode') : 'PY'], FailureHandling.STOP_ON_FAILURE)

getDataActorOfLogID = REA.getAllDataDatabase('172.16.94.145', 'a2isfinanceDB', qGetActor.replace("_ReffNo_", GlobalVariable.VoucherNo))
strActor = getDataActorOfLogID.get(0).get(0).toString()

getActingStatus = REA.getAllDataDatabase('172.16.94.70', 'BeyondDB_Apr2020', qGetActingStatus.replace("_Employee_", strUserName))
strActingStatus = getActingStatus.get(0).get(0).toString()

WebUI.comment (strActor)
WebUI.comment (strActingStatus)
if (strActingStatus == null || strActingStatus == '') {
	if (strActor == 'ILN') {
		KeywordUtil.markFailedAndStop("Skenario Gagal!")
	} else {
		WebUI.comment('Skenario berhasil!')
	}
} else {
	if (strActor == 'ILN') {
		WebUI.comment('Skenario berhasil!')
	} else {
		KeywordUtil.markFailedAndStop("Skenario Gagal!")
	}
}






