import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
import groovy.json.JsonSlurper
import com.keyword.REA as REA

//LPS OOF ke EUS (tergantung yang generate voucher)
//Skenario EUS dapat melakukan approve VS11 dan 17
UI.SetGlobal('endpoint', 'Finance')

UI.SetGlobal('endpointlogin', 'Finance2')

String strPINo = ''
String strUser = 'EUS'
if (UI.verifyStaging()) {
	
    GlobalVariable.VoucherID = ''
	GlobalVariable.Action = ''
	
} else {
	WebUI.println('bukan staging')
    strPINo = 'PIS21A00543'
	GlobalVariable.Action = 'approve'	
}

WebUI.println(GlobalVariable.VoucherID)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'RVVT'
	, ('username') : strUser], FailureHandling.STOP_ON_FAILURE)

def submitWorklist = WS.sendRequest(findTestObject('Worklist/SubmitPIApproval', [('PINo') : strPINo]))

jsonSlurper = new JsonSlurper()

submitWorklist = jsonSlurper.parseText(submitWorklist.getResponseText())

WebUI.println(submitWorklist)

getDataActorOfLogID = REA.getAllDataDatabase('172.16.94.145', 'a2isfinanceDB', qGetActor.replace("_ReffNo_", strPINo))
strActor = getDataActorOfLogID.get(0).get(0).toString()

getActingStatus = REA.getAllDataDatabase('172.16.94.70', 'BeyondDB_Apr2020', qGetActingStatus.replace("_Employee_", strUser))
strActingStatus = getActingStatus.get(0).get(0).toString()

if (submitWorklist.data.Messages == '1 of 1 PI(s) has been updated') {
	if (strActingStatus == null || strActingStatus == '') {
		if (strActor == 'LPS') {
			KeywordUtil.markFailedAndStop("Skenario Gagal!")
		} else {
			WebUI.comment('Skenario berhasil!')
		}
	} else {
		if (strActor == 'LPS') {
			WebUI.comment('Skenario berhasil!')
		} else {
			KeywordUtil.markFailedAndStop("Skenario Gagal!")
		}
	}
} else {
	KeywordUtil.markFailedAndStop("Failed SubmitWorklist voucher with message : "+ submitWorklist.data.message)
}


