import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
import groovy.json.JsonSlurper
import com.keyword.REA as REA

//ILN OOF ke SRN
//Skenario EUS melakukan approve VS2 dan approval VS3 nextnya adalah ILN maka VS 3 akan auto approve
UI.SetGlobal('endpoint', 'Finance')

UI.SetGlobal('endpointlogin', 'Finance2')

String strActor
String strActingStatus
String strUserName  = 'SRN' //untuk login awal

if (UI.verifyStaging()) {
	
    GlobalVariable.VoucherID = ''
	GlobalVariable.Action = ''
	
} else {
	WebUI.println('bukan staging')
    GlobalVariable.VoucherID = "[45157]"
	GlobalVariable.Action = 'approve'
	
}

WebUI.println(GlobalVariable.VoucherID)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'U1BN'
	, ('username') : strUserName], FailureHandling.STOP_ON_FAILURE)

def submitWorklist = WS.sendRequest(findTestObject('Worklist/SubmitWorklist'))

getDataActorOfLogID = REA.getAllDataDatabase('172.16.94.145', 'a2isfinanceDB', qGetActor.replace("_ReffNo_", GlobalVariable.VoucherNo))
strActor = getDataActorOfLogID.get(0).get(0).toString()

getActingStatus = REA.getAllDataDatabase('172.16.94.70', 'BeyondDB_Apr2020', qGetActingStatus.replace("_Employee_", strUserName))
strActingStatus = getActingStatus.get(0).get(0).toString()

jsonSlurper = new JsonSlurper()

submitWorklist = jsonSlurper.parseText(submitWorklist.getResponseText())

WebUI.println(submitWorklist)
if (submitWorklist.data.Messages == '2 of 1 voucher(s) has been updated') {
	if (strActor == 'ILN') {
		WebUI.comment('Skenario berhasil!')
	} else {
		KeywordUtil.markFailedAndStop("Skenario Gagal!")
	}
} else {
	KeywordUtil.markFailedAndStop("Failed SubmitWorklist voucher with message : "+ submitWorklist.data.message)
}


