import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil


//kondisi OOF ILN ke JUR
//Walaupun JUR sudah mendapatkan akses ILN dia tidak dapat melakukan approval VS3
//Result API 0 data
UI.SetGlobal('endpoint', 'Finance')

UI.SetGlobal('endpointlogin', 'Finance2')

String paramVoucher

if (UI.verifyStaging()) {
	
	paramVoucher = ''
	
} else {

	paramVoucher = 'JKT00FRC20018984'
	
}

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'SlVS'
	, ('username') : 'JUR'], FailureHandling.STOP_ON_FAILURE)

def getWorklist = WS.sendRequest(findTestObject('Worklist/GetWorklist', [('voucherNo') : paramVoucher]))

def jsonSlurper = new JsonSlurper()

respGetWorklist = jsonSlurper.parseText(getWorklist.getResponseText())
WebUI.println(respGetWorklist)
if (respGetWorklist.data.totalItems == 0) {
	WebUI.comment('Skenario berhasil!')
} else {
	WebUI.comment('Skenario gagal harap hubungi dev terkait!')
	KeywordUtil.markFailedAndStop('')
}