import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

UI.SetGlobal('endpoint', 'Finance')

UI.SetGlobal('endpointlogin', 'Finance2')

String paramNote

if (UI.verifyStaging()) {
	
    paramNote = '00036/CN/JKT00/03/21' //01482/CN/JKT00/06/11'
	
} else {

    paramNote = '00097/CN/JKT00/11/20'
	
}

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'SlVS', ('username') : 'JUR'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/CreateVoucher'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Update Flag Hold Note'), [('ParamNoteNo') : paramNote, ('scenario') : 'unhold'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SearchAndAddNote'), [('Scenario') : 'CreateVoucher'
        , ('DataNota') : 'HarcodeNota', ('TransactionTypeID') : '1'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/UploadAttachmentVoucher'), [('DocumentTypeCode') : 'INVVC'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/UploadAttachmentVoucher'), [('DocumentTypeCode') : 'FAKVC'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/ValidateWTVoucher'), [('VoucherTypeCode') : 'PY'
        , ('scenario') : 'FlowVoucher'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SubmitVoucher'), [('TransactionTypeID') : 1
        , ('PaymentMethodID') : 1, ('AABBankAccountID') : 62, ('AABBankAccountNo') : '0063069695', ('Remarks') : 'PY Tax pph21 automate'
        , ('PayerID') : 0, ('VoucherTypeCode') : 'PY'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Check DB FIN - Create Voucher'), [('Proses') : 'GetNoteData'
        , ('Skenario') : 'Tax'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/GetVoucher'), [('Skenario') : 'Create'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Worklist/Worklist OOF'), [('Skenario') : 'Create'], FailureHandling.STOP_ON_FAILURE)

while (GlobalVariable.Status != 'Settled'){
	
	println (GlobalVariable.Status)
	
	if (GlobalVariable.Status == 'Approved - Waiting Validation' && GlobalVariable.VoucherTypeCode == "PY"){
		
		GlobalVariable.Action ='validatedWithoutPI'
		
	} else if (GlobalVariable.Status == 'Validated - Waiting Validation' && GlobalVariable.VoucherTypeCode != "PY"){
	
		GlobalVariable.Action ='validated'
	
	} else if (GlobalVariable.Status == 'Validated - Waiting Settlement'){
	
		GlobalVariable.Action ='settled'
		
	} else if (GlobalVariable.Status == 'Waiting Approval'){
	
		GlobalVariable.Action ='approve'
		
	} else if (GlobalVariable.Status == 'Tax Approval'){
			
		WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : GlobalVariable.EncriptUsername, ('username') : GlobalVariable.NextUser],
					FailureHandling.STOP_ON_FAILURE)
			
		WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Worklist/SubmitTaxWorklist'), [('Action') : 'approvetax'],
					FailureHandling.STOP_ON_FAILURE)
		
	}
	
	if (GlobalVariable.Status == 'Approved - Waiting Validation' || GlobalVariable.Status == 'Waiting Approval' || GlobalVariable.Status == 'Validated - Waiting Settlement'){
		
		println (GlobalVariable.Action)
				
		WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : GlobalVariable.EncriptUsername, ('username') : GlobalVariable.NextUser],
				FailureHandling.STOP_ON_FAILURE)
	
		WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SubmitWorklist'), [('Skenario') : 'CreateVoucher'
				, ('Action') : GlobalVariable.Action], FailureHandling.STOP_ON_FAILURE)
		
	}
	
	GlobalVariable.Action = ''
	
	GlobalVariable.Status = ''
		
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Worklist/Worklist OOF'), [('Skenario') : 'Create'], FailureHandling.STOP_ON_FAILURE)
}

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Check DB FIN - Create Voucher'), [('Proses') : 'CreateVoucher'
        , ('Skenario') : 'Tax'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Check DB SEA'), [('Proses') : 'CreateVoucher'], FailureHandling.STOP_ON_FAILURE)

