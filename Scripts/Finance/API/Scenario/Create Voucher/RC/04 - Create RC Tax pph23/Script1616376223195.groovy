import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

UI.SetGlobal('endpoint', 'Finance')

UI.SetGlobal('endpointlogin', 'Finance2')

String paramNote

if (UI.verifyStaging()) {
	
    paramNote = '01003/DN/JKT00/01/21'
	
} else {

    paramNote = '00451/DN/JKT00/08/14","01729/DN/JKT00/08/19","04620/CN/JKT00/08/14'
	
}

//login JUR
WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'SlVS', ('username') : 'JUR'], 
    	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/Request/Create Edited Note/CreateVoucher'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Update Flag Hold Note'), [('ParamNoteNo') : paramNote
		, ('scenario') : 'unhold'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SearchAndAddNote'), [('Scenario') : 'CreateVoucher'
        , ('DataNota') : 'HarcodeNota', ('TransactionTypeID') : '1'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/ValidateWTVoucher'), [('VoucherTypeCode') : 'RC'
        , ('scenario') : 'FlowVoucher'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/ExcludeTaxNotes'), [('ExcludeWithholdingTax') : true
		, ('ExcludeVAT') : true], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/ValidateWTVoucher'), [('VoucherTypeCode') : 'RC'
		, ('scenario') : 'FlowVoucher'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SubmitVoucher'), [('TransactionTypeID') : 1
        , ('PaymentMethodID') : 1, ('AABBankAccountID') : 62, ('AABBankAccountNo') : '0063069695', ('Remarks') : 'RC Tax PPh23 automate'
        , ('PayerID') : 0, ('VoucherTypeCode') : 'RC'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Check DB Finance Voucher - Create'), [('process') : 'GetNoteData'
	, ('Kriteria') : 'Fullpayment' , ('Kondisi') : 'ExcludeTax'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/GetVoucher'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Worklist/Worklist OOF'), [('Skenario') : 'Create'], FailureHandling.STOP_ON_FAILURE)

while (GlobalVariable.Status != 'Settled'){
	
	println (GlobalVariable.Status)
	
	if (GlobalVariable.Status == 'Approved - Waiting Validation'){
		
		GlobalVariable.Action ='validated'
		
	} else if (GlobalVariable.Status == 'Validated - Waiting Settlement'){
	
		GlobalVariable.Action ='settled'
	}
	
	println (GlobalVariable.Action)
			
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : GlobalVariable.EncriptUsername, ('username') : GlobalVariable.NextUser],
			FailureHandling.STOP_ON_FAILURE)

	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Create Voucher/SubmitWorklist'), [('Skenario') : 'CreateVoucher'
			, ('Action') : GlobalVariable.Action], FailureHandling.STOP_ON_FAILURE)
	
	GlobalVariable.Action = ''
	
	GlobalVariable.Status = ''
		
	WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Worklist/Worklist OOF'), [('Skenario') : 'Create'], FailureHandling.STOP_ON_FAILURE)
}

WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Check DB Finance Voucher - Create'), [('process') : 'CreateVoucher'
		,('Kriteria') : 'Fullpayment' ,('Kondisi') : 'ExcludeTax'], FailureHandling.STOP_ON_FAILURE)
   
WebUI.callTestCase(findTestCase('Finance/API/Script/Check DB/Check DB SEA'), [('scenario') : 'RC'
	, ('process') : 'CreateVoucher' , ('Kriteria') : 'Full'], FailureHandling.STOP_ON_FAILURE)
