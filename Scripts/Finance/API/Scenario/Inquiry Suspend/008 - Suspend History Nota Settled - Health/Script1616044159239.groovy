import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI as UI
import com.keyword.GEN5


UI.SetGlobal('endpoint', 'Finance')

UI.SetGlobal('endpointlogin', 'Finance2')

ArrayList DataParam = new ArrayList()

if (UI.verifyStaging()) {
	
    DataParam = GEN5.getOneRowDatabase("172.16.93.175", "a2isFinanceDB", GlobalVariable.FIN_GetNoteDataForSuspendHistory.replace("and suspendresultcategoryid <> 0","--and suspendresultcategoryid <> 0"))
	
} else {

	DataParam = GEN5.getOneRowDatabase("172.16.94.145", "a2isFinanceDB", GlobalVariable.FIN_GetNoteDataForSuspendHistory.replace("and suspendresultcategoryid <> 0","--and suspendresultcategoryid <> 0"))
	
}

println (DataParam)

GlobalVariable.NoteNumber = DataParam[1]

GlobalVariable.VoucherNo = DataParam[2]

//WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Login/Login'), [('Username') : 'RFZJ', ('username') : 'EUS'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Inquiry Suspend/GetSuspendNoteHistory'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Finance/API/Script/API/Finance/Inquiry Suspend/GetSuspendSettlementHistory'), [:], FailureHandling.STOP_ON_FAILURE)




