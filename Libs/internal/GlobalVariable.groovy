package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object Token
     
    /**
     * <p></p>
     */
    public static Object WTVoucherID
     
    /**
     * <p></p>
     */
    public static Object NoteNumber
     
    /**
     * <p></p>
     */
    public static Object FinanceNoteID
     
    /**
     * <p></p>
     */
    public static Object WTVoucherDetailID
     
    /**
     * <p></p>
     */
    public static Object ARAPAmount
     
    /**
     * <p></p>
     */
    public static Object VoucherID
     
    /**
     * <p></p>
     */
    public static Object VoucherNo
     
    /**
     * <p></p>
     */
    public static Object PaymentDate
     
    /**
     * <p></p>
     */
    public static Object PINo
     
    /**
     * <p></p>
     */
    public static Object TokenPI
     
    /**
     * <p></p>
     */
    public static Object FinanceNoteIDCA
     
    /**
     * <p></p>
     */
    public static Object VoucherDetailIDCA
     
    /**
     * <p></p>
     */
    public static Object VoucherIDCA
     
    /**
     * <p></p>
     */
    public static Object WTVoucherIDCA
     
    /**
     * <p></p>
     */
    public static Object VoucherNoCA
     
    /**
     * <p></p>
     */
    public static Object VoucherStatusID
     
    /**
     * <p></p>
     */
    public static Object FIN_GetNotePremiumW2
     
    /**
     * <p></p>
     */
    public static Object FIN_GetNoteClaimW2
     
    /**
     * <p>Profile default : Param: GVFinanceNoteID</p>
     */
    public static Object FIN_UpdateHoldFlag
     
    /**
     * <p>Profile default : Param: GVnotenumber</p>
     */
    public static Object FIN_GetFinanceNoteID
     
    /**
     * <p></p>
     */
    public static Object ParamNoteNumber
     
    /**
     * <p></p>
     */
    public static Object ParamString
     
    /**
     * <p></p>
     */
    public static Object ParamList
     
    /**
     * <p></p>
     */
    public static Object PolicyNo
     
    /**
     * <p></p>
     */
    public static Object AABAccount
     
    /**
     * <p></p>
     */
    public static Object PayerId
     
    /**
     * <p></p>
     */
    public static Object GroupingNo
     
    /**
     * <p></p>
     */
    public static Object NoteNo
     
    /**
     * <p></p>
     */
    public static Object VoucherType
     
    /**
     * <p></p>
     */
    public static Object EntryUser
     
    /**
     * <p>Profile default : format: 2020&#47;Sep&#47;08</p>
     */
    public static Object RequestDateFrom
     
    /**
     * <p>Profile default : format: 2020&#47;Sep&#47;08</p>
     */
    public static Object RequestDateTo
     
    /**
     * <p></p>
     */
    public static Object PageNo
     
    /**
     * <p></p>
     */
    public static Object ClaimNo
     
    /**
     * <p></p>
     */
    public static Object VoucherStatus
     
    /**
     * <p></p>
     */
    public static Object IsVATExclude
     
    /**
     * <p></p>
     */
    public static Object IsWitholdingTaxExclude
     
    /**
     * <p></p>
     */
    public static Object RequestAmount
     
    /**
     * <p></p>
     */
    public static Object VATAmount
     
    /**
     * <p></p>
     */
    public static Object IsFullPayment
     
    /**
     * <p></p>
     */
    public static Object OriginalAmount
     
    /**
     * <p></p>
     */
    public static Object PPNAmount
     
    /**
     * <p></p>
     */
    public static Object calculateWitholdingTax
     
    /**
     * <p></p>
     */
    public static Object FPOverShortage
     
    /**
     * <p></p>
     */
    public static Object TransactionTypeID
     
    /**
     * <p></p>
     */
    public static Object PaymentMethodID
     
    /**
     * <p></p>
     */
    public static Object AABBankAccountID
     
    /**
     * <p></p>
     */
    public static Object AABBankAccountNo
     
    /**
     * <p></p>
     */
    public static Object Remarks
     
    /**
     * <p></p>
     */
    public static Object PayerID
     
    /**
     * <p></p>
     */
    public static Object VoucherTypeCode
     
    /**
     * <p></p>
     */
    public static Object Action
     
    /**
     * <p></p>
     */
    public static Object username
     
    /**
     * <p></p>
     */
    public static Object GetDataCompareToVD
     
    /**
     * <p></p>
     */
    public static Object AABBankAccount
     
    /**
     * <p></p>
     */
    public static Object VoucherTypeCodeBefore
     
    /**
     * <p></p>
     */
    public static Object ExcludeWithholdingTax
     
    /**
     * <p></p>
     */
    public static Object ExcludeVAT
     
    /**
     * <p></p>
     */
    public static Object BankCharges
     
    /**
     * <p></p>
     */
    public static Object Journal
     
    /**
     * <p></p>
     */
    public static Object FinanceNotesID
     
    /**
     * <p></p>
     */
    public static Object CdEntity
     
    /**
     * <p></p>
     */
    public static Object CustomerName
     
    /**
     * <p></p>
     */
    public static Object PayerName
     
    /**
     * <p></p>
     */
    public static Object JournalType
     
    /**
     * <p></p>
     */
    public static Object SubJournalType
     
    /**
     * <p></p>
     */
    public static Object NoteStatus
     
    /**
     * <p></p>
     */
    public static Object ProductionDateFrom
     
    /**
     * <p></p>
     */
    public static Object ProductionDateTo
     
    /**
     * <p></p>
     */
    public static Object ClientName
     
    /**
     * <p></p>
     */
    public static Object MemberName
     
    /**
     * <p></p>
     */
    public static Object EmployeeID
     
    /**
     * <p></p>
     */
    public static Object MemberNo
     
    /**
     * <p></p>
     */
    public static Object isHoldFlag
     
    /**
     * <p></p>
     */
    public static Object OtherReferenceType
     
    /**
     * <p></p>
     */
    public static Object OtherReferenceValue
     
    /**
     * <p></p>
     */
    public static Object DocumentDate
     
    /**
     * <p></p>
     */
    public static Object PaymentInstrumentNo
     
    /**
     * <p></p>
     */
    public static Object ParamGenerateVoucherBulkPayment
     
    /**
     * <p></p>
     */
    public static Object ParamGenerateVoucherSinglePayment
     
    /**
     * <p></p>
     */
    public static Object ParamSubmitVoucherRC
     
    /**
     * <p></p>
     */
    public static Object ParamSubmitVoucherCA
     
    /**
     * <p></p>
     */
    public static Object ParamSubmitVoucherW2
     
    /**
     * <p></p>
     */
    public static Object ParamSubmitVoucherPS
     
    /**
     * <p></p>
     */
    public static Object ParamSubmitVoucherRO
     
    /**
     * <p></p>
     */
    public static Object ParamSubmitVoucherPY
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNoteSPR
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNotePRM
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNoteCLM
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNoteCOM
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNoteEXS
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNoteOTH
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNoteSCA
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNoteSOT
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNoteCOMPersonal
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNoteCOMCompany
     
    /**
     * <p></p>
     */
    public static Object ParamFinanceNoteSPRSubmitFinanceNote
     
    /**
     * <p></p>
     */
    public static Object DocumentTypeCode
     
    /**
     * <p></p>
     */
    public static Object CustomerCode
     
    /**
     * <p></p>
     */
    public static Object EncriptUsername
     
    /**
     * <p></p>
     */
    public static Object Status
     
    /**
     * <p></p>
     */
    public static Object NextUser
     
    /**
     * <p></p>
     */
    public static Object ListVoucher
     
    /**
     * <p></p>
     */
    public static Object ExportType
     
    /**
     * <p></p>
     */
    public static Object SuspendResultCategoryID
     
    /**
     * <p></p>
     */
    public static Object InternalDistributionID
     
    /**
     * <p></p>
     */
    public static Object InternalDistributionCode
     
    /**
     * <p></p>
     */
    public static Object InternalDistributionDescription
     
    /**
     * <p></p>
     */
    public static Object SalesmanCode
     
    /**
     * <p></p>
     */
    public static Object SalesmanName
     
    /**
     * <p></p>
     */
    public static Object PayerCode
     
    /**
     * <p></p>
     */
    public static Object PICCode
     
    /**
     * <p></p>
     */
    public static Object PICName
     
    /**
     * <p></p>
     */
    public static Object UnitCode
     
    /**
     * <p></p>
     */
    public static Object UnitDescription
     
    /**
     * <p>Profile default : ex: outstanding, settled</p>
     */
    public static Object SuspendStatus
     
    /**
     * <p></p>
     */
    public static Object RequestDate
     
    /**
     * <p></p>
     */
    public static Object Overshortage
     
    /**
     * <p></p>
     */
    public static Object SwiftCode
     
    /**
     * <p></p>
     */
    public static Object ClearingCode
     
    /**
     * <p></p>
     */
    public static Object AccountNo
     
    /**
     * <p></p>
     */
    public static Object AccountHolder
     
    /**
     * <p></p>
     */
    public static Object RequestCode
     
    /**
     * <p></p>
     */
    public static Object Nominal
     
    /**
     * <p></p>
     */
    public static Object ParamList1
     
    /**
     * <p></p>
     */
    public static Object DataMemberNo
     
    /**
     * <p></p>
     */
    public static Object BookingNo
     
    /**
     * <p></p>
     */
    public static Object ParamAll
     
    /**
     * <p></p>
     */
    public static Object EConsulAutomaticVoucherQueueList
     
    /**
     * <p></p>
     */
    public static Object ParamSuspendNotes
     
    /**
     * <p></p>
     */
    public static Object EConsulAutomaticVoucherRO
     
    /**
     * <p></p>
     */
    public static Object ParamDetail
     
    /**
     * <p></p>
     */
    public static Object ParamPaymentGateway
     
    /**
     * <p></p>
     */
    public static Object FIN_DataSuspendCategoryEconsul
     
    /**
     * <p></p>
     */
    public static Object ParamAddWTVoucherDetailOthersEconsul
     
    /**
     * <p></p>
     */
    public static Object ReferenceFinanceNoteID
     
    /**
     * <p></p>
     */
    public static Object ReferenceNoteNumber
     
    /**
     * <p></p>
     */
    public static Object VoucherPS
     
    /**
     * <p></p>
     */
    public static Object VoucherPO
     
    /**
     * <p></p>
     */
    public static Object VoucherRO
     
    /**
     * <p></p>
     */
    public static Object FinanceGroupCdEntity
     
    /**
     * <p></p>
     */
    public static Object TaxGroupCdEntity
     
    /**
     * <p></p>
     */
    public static Object IterasiGetOrganizationEntityUserList
     
    /**
     * <p></p>
     */
    public static Object GroupCdEntity
     
    /**
     * <p></p>
     */
    public static Object FinOrganizationEntityUserList
     
    /**
     * <p></p>
     */
    public static Object TaxOrganizationEntityUserList
     
    /**
     * <p></p>
     */
    public static Object HeadOrganizationEntityUserList
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object FIN_GetFinanceNotesSSTCMT
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GetDataCompareToFinanceNotesCMT
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GetDataCompareToFinanceNotesSSTPPH
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GetDataCompareToFinanceNotesSSTPPN
     
    /**
     * <p>Profile default query : GVNoteNumber,GVCashAmount</p>
     */
    public static Object FIN_GetDataTaxCompareToVoucherDetail
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GetFinanceNoteAdditional
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GetVoucher
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GetVoucherDetail
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_GetFinanceNotes
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_GetDataCompareToFinanceSettlements
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_GetDataCompareToVoucherDetail
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GetFinanceSettlements
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GetVoucherSettlementBackHistory
     
    /**
     * <p></p>
     */
    public static Object FIN_GetDataCompareToVoucherSettlementBackHistory
     
    /**
     * <p></p>
     */
    public static Object FIN_GetDataCompareToFinanceNotesSuspend
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object FIN_GetFinanceNotesSuspend
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object FIN_GetSuspendMonitoring
     
    /**
     * <p>Profile default query : GVSuspendResultCategoryCode</p>
     */
    public static Object FIN_GetDataCompareToSuspendMonitoring
     
    /**
     * <p>Profile default query : GVAABAccountID</p>
     */
    public static Object FIN_GetDataCompareToVoucher
     
    /**
     * <p></p>
     */
    public static Object FIN_GetNoteData
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_GetBaseNoteDataToCalculateTax
     
    /**
     * <p>Profile default query : GVJournalTypeCode, GVSubJournalTypeCode, GVTaxTypeCode, 'GVNominal',GVSuspendResultCategoryCode </p>
     */
    public static Object FIN_GetDataCompareToVoucherDetailOthers
     
    /**
     * <p></p>
     */
    public static Object FIN_GetInquiryVoucher
     
    /**
     * <p>Profile default query : GVNoteNumber</p>
     */
    public static Object FIN_GetDataCompareToWTVoucherDetail
     
    /**
     * <p>Profile default query : GVWTVoucherID</p>
     */
    public static Object FIN_GetWTVoucherDetail
     
    /**
     * <p>Profile default query : GVWTVoucherID</p>
     */
    public static Object FIN_GetWTVoucher
     
    /**
     * <p>Profile default query : GVlogid</p>
     */
    public static Object BEY_GetCdEntity
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object SEA_GetFSL
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object SEA_GetnVoucher
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object SEA_GetpVoucher
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object SEA_GetfVoucher
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object SEA_GetDataCompareToFSL
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object SEA_GetDataCompareTonVoucher
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object SEA_GetDataCompareTopVoucher
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object SEA_GetDataCompareTofVoucher
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object SEA_UpdatePostingFinanceVoucherQueue
     
    /**
     * <p>Profile default query : GVNotenumber</p>
     */
    public static Object SEA_GetVoucherRefSettled
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object SEA_GetPostingFinanceVoucherQueue
     
    /**
     * <p></p>
     */
    public static Object FIN_GetInquiryVoucher2
     
    /**
     * <p>Profile default query : where </p>
     */
    public static Object FIN_GetDataInquiryNote
     
    /**
     * <p>Profile default query : 9</p>
     */
    public static Object FIN_GetParamInquiryNote
     
    /**
     * <p>Profile default query : 9</p>
     */
    public static Object FIN_GetParamInquiryVoucher
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object FIN_GetNextUser
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object BEY_GetInitialDataCompareToFinanceNotes
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object BEY_GetInitialDataCompareToFinanceNoteInfos
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object BEY_GetInitialDataCompareToFinanceNoteAdditional
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object MIS_GetDataCIF
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object BEY_GetDataStatusNota
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object BEY_GetInitialDataCompareToFinanceSettlements
     
    /**
     * <p>Profile default query : GVVoucherNo, 9</p>
     */
    public static Object BEY_GetInitialDataCompareToVoucher
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object BEY_GetInitialDataCompareToVoucherDetail
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object BEY_GetInitialDataCompareToVoucherDetailOffshore
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object BEY_GetInitialDataCompareToVoucherDetailProgressiveTax
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object BEY_GetInitialDataCompareToCustomer
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object BEY_GetInitialDataCompareToCustomerPersonal
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object BEY_GetInitialDataCompareToCustomerCompany
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object BEY_GetInitialDataCompareToCustomerAccount
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object BEY_GetInitialDataCompareToCustomerUsage
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object BEY_GetInitialDataCompareToCustomerPIC
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object BEY_GetInitialDataCompareToCustomerAddress
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object FIN_SSISDataFIN_Voucher
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_SSISDataFIN_VoucherDetail
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_SSISDataFIN_VoucherDetailOffshore
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_SSISDataFIN_FinanceNotes
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_SSISDataFIN_FinanceNoteInfos
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_SSISDataFIN_FinanceNoteAdditional
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_SSISDailyData_FinanceNotes
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_SSISDailyData_FinanceNoteInfos
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_SSISDailyData_FinanceNoteAdditional
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDailyData_Customer
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDailyData_CustomerPersonal
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDailyData_CustomerCompany
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDailyData_CustomerAccount
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDailyData_CustomerUsage
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDailyData_CustomerPIC
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDailyData_CustomerAddress
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object FIN_SSISDailyData_Voucher
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_SSISDailyData_VoucherDetail
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_SSISDailyData_VoucherDetailOffshore
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_SSISDailyData_VoucherDetailProgressiveTax
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDataFIN_Customer
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDataFIN_CustomerPersonal
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDataFIN_CustomerCompany
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDataFIN_CustomerAccount
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDataFIN_CustomerUsage
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDataFIN_CustomerPIC
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISDataFIN_CustomerAddress
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_SSISDataFIN_VoucherDetailProgressiveTax
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_SSISInitialData_FinanceNotes
     
    /**
     * <p>Profile default query : GVNotenumber</p>
     */
    public static Object FIN_SSISInitialData_FinanceNoteInfos
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_SSISInitialData_FinanceNoteAdditional
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISInitialData_Customer
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISInitialData_CustomerPersonal
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISInitialData_CustomerCompany
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISInitialData_CustomerAccount
     
    /**
     * <p></p>
     */
    public static Object FIN_SSISInitialData_CustomerUsage
     
    /**
     * <p></p>
     */
    public static Object FIN_SSISInitialData_CustomerPIC
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_SSISInitialData_CustomerAddress
     
    /**
     * <p></p>
     */
    public static Object FIN_SSISInitialData_Voucher
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_SSISInitialData_VoucherDetail
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_SSISInitialData_VoucherDetailOffshore
     
    /**
     * <p></p>
     */
    public static Object FIN_SSISInitialData_VoucherDetailProgressiveTax
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_ProcessDateSSISNote
     
    /**
     * <p>Profile default query : GVNoteNumber</p>
     */
    public static Object BEY_GetDataSSISVoucherNo
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object BEY_GetDataSSISNoteNo
     
    /**
     * <p>Profile default query : GVCustomerCode</p>
     */
    public static Object FIN_GetCustomerID
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object FIN_SSISInitialData_FinanceSettlements
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GetNotePayerID_VD
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object FIN_SSISDataFIN_FinanceSettlements
     
    /**
     * <p>Profile default query : GVCustomerID</p>
     */
    public static Object FIN_GetCustomerCode
     
    /**
     * <p></p>
     */
    public static Object FIN_SSISDailyData_FinanceSettlements
     
    /**
     * <p>Profile default query : GVlogID</p>
     */
    public static Object BEY_UserWorklist
     
    /**
     * <p>Profile default query : GVnotenumber,GVvoucherno</p>
     */
    public static Object FIN_GetDataCompareToSuspendNote
     
    /**
     * <p></p>
     */
    public static Object FIN_GetNoteDataForSuspendHistory
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_GetDataCompareToSuspendSettlementHistory
     
    /**
     * <p>Profile default query : GVinternaldistributionID</p>
     */
    public static Object FIN_GetInternalDistribution
     
    /**
     * <p>Profile default query : GVunitcode</p>
     */
    public static Object FIN_GetUnit
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_GetDataCompareToUD_SuspendMonitoring
     
    /**
     * <p></p>
     */
    public static Object FIN_GetNoteDataForInquirySuspend
     
    /**
     * <p>Profile default query : FNA.rowstatus ='0'</p>
     */
    public static Object FIN_GetDataCompareToInquirySuspend
     
    /**
     * <p></p>
     */
    public static Object FIN_GetVoucherDataForInquiryVoucher
     
    /**
     * <p></p>
     */
    public static Object FIN_GetDataCompareToVDPS
     
    /**
     * <p></p>
     */
    public static Object FIN_GetDataCompareToFSPS
     
    /**
     * <p></p>
     */
    public static Object SEA_GetValidMember
     
    /**
     * <p>Profile default query : GVMemberNo,GVBookDate,GVClaimNo</p>
     */
    public static Object SEA_CekEligibilityMember
     
    /**
     * <p>Profile default query : GVNominal,GVBookingNo,GVClaimNo,GVSeq,GVDeskripsi,GVBookDate,GVType</p>
     */
    public static Object SEA_InsertToEconsultation_claimdetail
     
    /**
     * <p>Profile default query : GVbookingno</p>
     */
    public static Object SEA_GetDataEconsultation_claimdetail
     
    /**
     * <p>Profile default query : GVseqno,GVNominalAccepted,GVNominalUnpaid,GVbookingno</p>
     */
    public static Object SEA_UpdateUnpaidEconsul
     
    /**
     * <p>Profile default query : GVBookingNo,GVClaimNo,GVMemberNo,GVBookDate,GVAction,GVTransactionReference,GVPaymentTypeCode,GVNominal,GVFee,GVDeskripsi,GVSeq</p>
     */
    public static Object SEA_InsertToEConsultationAutomaticVoucherQueue
     
    /**
     * <p>Profile default query : GVTransactionReference</p>
     */
    public static Object SEA_GetDataEConsultationAutomaticVoucherQueue
     
    /**
     * <p>Profile default query : GVName</p>
     */
    public static Object SEA_GetPaymentType
     
    /**
     * <p>Profile default query : GVNotenumber</p>
     */
    public static Object FIN_GetVoucherFromNote
     
    /**
     * <p>Profile default query : GVVoucherNo,GVNotenumber</p>
     */
    public static Object FIN_DeleteUpdateFIN
     
    /**
     * <p>Profile default query : GVVoucherNo,GVNotenumber</p>
     */
    public static Object SEA_DeleteUpdateSEA
     
    /**
     * <p></p>
     */
    public static Object FIN_GetRequestPSEconsul
     
    /**
     * <p>Profile default query : GVnotenumber</p>
     */
    public static Object FIN_GetRemarkROEconsul
     
    /**
     * <p>Profile default query : GVnoteno</p>
     */
    public static Object SEA_GetAmountDataVoucher
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object SEA_DeleteVoucherRequest
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GeneratePSEconsul
     
    /**
     * <p>Profile default query : GVreferenceno</p>
     */
    public static Object FIN_GetStatusPSEconsul
     
    /**
     * <p></p>
     */
    public static Object FIN_GetVoucherForInquiry
     
    /**
     * <p></p>
     */
    public static Object FIN_GetBankForSearch
     
    /**
     * <p></p>
     */
    public static Object FIN_GetAABAccountForSearch
     
    /**
     * <p>Profile default query : param</p>
     */
    public static Object FIN_GetAABAccount
     
    /**
     * <p></p>
     */
    public static Object FIN_GetInternalDistributionForSearch
     
    /**
     * <p></p>
     */
    public static Object FIN_GetParamSubmitAABAccount
     
    /**
     * <p></p>
     */
    public static Object FIN_DeleteAABAccount
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_UpdateRowStatus
     
    /**
     * <p>Profile default query : GVvoucherno,GVTaskApprovalID,GVID</p>
     */
    public static Object FIN_UpdateVoucherROEconsul
     
    /**
     * <p>Profile default query : GVvoucherno</p>
     */
    public static Object FIN_GetLatestApprovalHistory
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object FIN_GetSuspendCategoryEconsul
     
    /**
     * <p>Profile default query : GVcorecode</p>
     */
    public static Object FIN_GetMappingAABAccountSEA
     
    /**
     * <p>Profile default query : GVpaymentmethodecode</p>
     */
    public static Object FIN_PaymentMethod
     
    /**
     * <p>Profile default query : GVVoucherNo</p>
     */
    public static Object FIN_GetSuspendEconsul
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            Token = selectedVariables['Token']
            WTVoucherID = selectedVariables['WTVoucherID']
            NoteNumber = selectedVariables['NoteNumber']
            FinanceNoteID = selectedVariables['FinanceNoteID']
            WTVoucherDetailID = selectedVariables['WTVoucherDetailID']
            ARAPAmount = selectedVariables['ARAPAmount']
            VoucherID = selectedVariables['VoucherID']
            VoucherNo = selectedVariables['VoucherNo']
            PaymentDate = selectedVariables['PaymentDate']
            PINo = selectedVariables['PINo']
            TokenPI = selectedVariables['TokenPI']
            FinanceNoteIDCA = selectedVariables['FinanceNoteIDCA']
            VoucherDetailIDCA = selectedVariables['VoucherDetailIDCA']
            VoucherIDCA = selectedVariables['VoucherIDCA']
            WTVoucherIDCA = selectedVariables['WTVoucherIDCA']
            VoucherNoCA = selectedVariables['VoucherNoCA']
            VoucherStatusID = selectedVariables['VoucherStatusID']
            FIN_GetNotePremiumW2 = selectedVariables['FIN_GetNotePremiumW2']
            FIN_GetNoteClaimW2 = selectedVariables['FIN_GetNoteClaimW2']
            FIN_UpdateHoldFlag = selectedVariables['FIN_UpdateHoldFlag']
            FIN_GetFinanceNoteID = selectedVariables['FIN_GetFinanceNoteID']
            ParamNoteNumber = selectedVariables['ParamNoteNumber']
            ParamString = selectedVariables['ParamString']
            ParamList = selectedVariables['ParamList']
            PolicyNo = selectedVariables['PolicyNo']
            AABAccount = selectedVariables['AABAccount']
            PayerId = selectedVariables['PayerId']
            GroupingNo = selectedVariables['GroupingNo']
            NoteNo = selectedVariables['NoteNo']
            VoucherType = selectedVariables['VoucherType']
            EntryUser = selectedVariables['EntryUser']
            RequestDateFrom = selectedVariables['RequestDateFrom']
            RequestDateTo = selectedVariables['RequestDateTo']
            PageNo = selectedVariables['PageNo']
            ClaimNo = selectedVariables['ClaimNo']
            VoucherStatus = selectedVariables['VoucherStatus']
            IsVATExclude = selectedVariables['IsVATExclude']
            IsWitholdingTaxExclude = selectedVariables['IsWitholdingTaxExclude']
            RequestAmount = selectedVariables['RequestAmount']
            VATAmount = selectedVariables['VATAmount']
            IsFullPayment = selectedVariables['IsFullPayment']
            OriginalAmount = selectedVariables['OriginalAmount']
            PPNAmount = selectedVariables['PPNAmount']
            calculateWitholdingTax = selectedVariables['calculateWitholdingTax']
            FPOverShortage = selectedVariables['FPOverShortage']
            TransactionTypeID = selectedVariables['TransactionTypeID']
            PaymentMethodID = selectedVariables['PaymentMethodID']
            AABBankAccountID = selectedVariables['AABBankAccountID']
            AABBankAccountNo = selectedVariables['AABBankAccountNo']
            Remarks = selectedVariables['Remarks']
            PayerID = selectedVariables['PayerID']
            VoucherTypeCode = selectedVariables['VoucherTypeCode']
            Action = selectedVariables['Action']
            username = selectedVariables['username']
            GetDataCompareToVD = selectedVariables['GetDataCompareToVD']
            AABBankAccount = selectedVariables['AABBankAccount']
            VoucherTypeCodeBefore = selectedVariables['VoucherTypeCodeBefore']
            ExcludeWithholdingTax = selectedVariables['ExcludeWithholdingTax']
            ExcludeVAT = selectedVariables['ExcludeVAT']
            BankCharges = selectedVariables['BankCharges']
            Journal = selectedVariables['Journal']
            FinanceNotesID = selectedVariables['FinanceNotesID']
            CdEntity = selectedVariables['CdEntity']
            CustomerName = selectedVariables['CustomerName']
            PayerName = selectedVariables['PayerName']
            JournalType = selectedVariables['JournalType']
            SubJournalType = selectedVariables['SubJournalType']
            NoteStatus = selectedVariables['NoteStatus']
            ProductionDateFrom = selectedVariables['ProductionDateFrom']
            ProductionDateTo = selectedVariables['ProductionDateTo']
            ClientName = selectedVariables['ClientName']
            MemberName = selectedVariables['MemberName']
            EmployeeID = selectedVariables['EmployeeID']
            MemberNo = selectedVariables['MemberNo']
            isHoldFlag = selectedVariables['isHoldFlag']
            OtherReferenceType = selectedVariables['OtherReferenceType']
            OtherReferenceValue = selectedVariables['OtherReferenceValue']
            DocumentDate = selectedVariables['DocumentDate']
            PaymentInstrumentNo = selectedVariables['PaymentInstrumentNo']
            ParamGenerateVoucherBulkPayment = selectedVariables['ParamGenerateVoucherBulkPayment']
            ParamGenerateVoucherSinglePayment = selectedVariables['ParamGenerateVoucherSinglePayment']
            ParamSubmitVoucherRC = selectedVariables['ParamSubmitVoucherRC']
            ParamSubmitVoucherCA = selectedVariables['ParamSubmitVoucherCA']
            ParamSubmitVoucherW2 = selectedVariables['ParamSubmitVoucherW2']
            ParamSubmitVoucherPS = selectedVariables['ParamSubmitVoucherPS']
            ParamSubmitVoucherRO = selectedVariables['ParamSubmitVoucherRO']
            ParamSubmitVoucherPY = selectedVariables['ParamSubmitVoucherPY']
            ParamFinanceNoteSPR = selectedVariables['ParamFinanceNoteSPR']
            ParamFinanceNotePRM = selectedVariables['ParamFinanceNotePRM']
            ParamFinanceNoteCLM = selectedVariables['ParamFinanceNoteCLM']
            ParamFinanceNoteCOM = selectedVariables['ParamFinanceNoteCOM']
            ParamFinanceNoteEXS = selectedVariables['ParamFinanceNoteEXS']
            ParamFinanceNoteOTH = selectedVariables['ParamFinanceNoteOTH']
            ParamFinanceNoteSCA = selectedVariables['ParamFinanceNoteSCA']
            ParamFinanceNoteSOT = selectedVariables['ParamFinanceNoteSOT']
            ParamFinanceNoteCOMPersonal = selectedVariables['ParamFinanceNoteCOMPersonal']
            ParamFinanceNoteCOMCompany = selectedVariables['ParamFinanceNoteCOMCompany']
            ParamFinanceNoteSPRSubmitFinanceNote = selectedVariables['ParamFinanceNoteSPRSubmitFinanceNote']
            DocumentTypeCode = selectedVariables['DocumentTypeCode']
            CustomerCode = selectedVariables['CustomerCode']
            EncriptUsername = selectedVariables['EncriptUsername']
            Status = selectedVariables['Status']
            NextUser = selectedVariables['NextUser']
            ListVoucher = selectedVariables['ListVoucher']
            ExportType = selectedVariables['ExportType']
            SuspendResultCategoryID = selectedVariables['SuspendResultCategoryID']
            InternalDistributionID = selectedVariables['InternalDistributionID']
            InternalDistributionCode = selectedVariables['InternalDistributionCode']
            InternalDistributionDescription = selectedVariables['InternalDistributionDescription']
            SalesmanCode = selectedVariables['SalesmanCode']
            SalesmanName = selectedVariables['SalesmanName']
            PayerCode = selectedVariables['PayerCode']
            PICCode = selectedVariables['PICCode']
            PICName = selectedVariables['PICName']
            UnitCode = selectedVariables['UnitCode']
            UnitDescription = selectedVariables['UnitDescription']
            SuspendStatus = selectedVariables['SuspendStatus']
            RequestDate = selectedVariables['RequestDate']
            Overshortage = selectedVariables['Overshortage']
            SwiftCode = selectedVariables['SwiftCode']
            ClearingCode = selectedVariables['ClearingCode']
            AccountNo = selectedVariables['AccountNo']
            AccountHolder = selectedVariables['AccountHolder']
            RequestCode = selectedVariables['RequestCode']
            Nominal = selectedVariables['Nominal']
            ParamList1 = selectedVariables['ParamList1']
            DataMemberNo = selectedVariables['DataMemberNo']
            BookingNo = selectedVariables['BookingNo']
            ParamAll = selectedVariables['ParamAll']
            EConsulAutomaticVoucherQueueList = selectedVariables['EConsulAutomaticVoucherQueueList']
            ParamSuspendNotes = selectedVariables['ParamSuspendNotes']
            EConsulAutomaticVoucherRO = selectedVariables['EConsulAutomaticVoucherRO']
            ParamDetail = selectedVariables['ParamDetail']
            ParamPaymentGateway = selectedVariables['ParamPaymentGateway']
            FIN_DataSuspendCategoryEconsul = selectedVariables['FIN_DataSuspendCategoryEconsul']
            ParamAddWTVoucherDetailOthersEconsul = selectedVariables['ParamAddWTVoucherDetailOthersEconsul']
            ReferenceFinanceNoteID = selectedVariables['ReferenceFinanceNoteID']
            ReferenceNoteNumber = selectedVariables['ReferenceNoteNumber']
            VoucherPS = selectedVariables['VoucherPS']
            VoucherPO = selectedVariables['VoucherPO']
            VoucherRO = selectedVariables['VoucherRO']
            FinanceGroupCdEntity = selectedVariables['FinanceGroupCdEntity']
            TaxGroupCdEntity = selectedVariables['TaxGroupCdEntity']
            IterasiGetOrganizationEntityUserList = selectedVariables['IterasiGetOrganizationEntityUserList']
            GroupCdEntity = selectedVariables['GroupCdEntity']
            FinOrganizationEntityUserList = selectedVariables['FinOrganizationEntityUserList']
            TaxOrganizationEntityUserList = selectedVariables['TaxOrganizationEntityUserList']
            HeadOrganizationEntityUserList = selectedVariables['HeadOrganizationEntityUserList']
            FIN_GetFinanceNotesSSTCMT = selectedVariables['FIN_GetFinanceNotesSSTCMT']
            FIN_GetDataCompareToFinanceNotesCMT = selectedVariables['FIN_GetDataCompareToFinanceNotesCMT']
            FIN_GetDataCompareToFinanceNotesSSTPPH = selectedVariables['FIN_GetDataCompareToFinanceNotesSSTPPH']
            FIN_GetDataCompareToFinanceNotesSSTPPN = selectedVariables['FIN_GetDataCompareToFinanceNotesSSTPPN']
            FIN_GetDataTaxCompareToVoucherDetail = selectedVariables['FIN_GetDataTaxCompareToVoucherDetail']
            FIN_GetFinanceNoteAdditional = selectedVariables['FIN_GetFinanceNoteAdditional']
            FIN_GetVoucher = selectedVariables['FIN_GetVoucher']
            FIN_GetVoucherDetail = selectedVariables['FIN_GetVoucherDetail']
            FIN_GetFinanceNotes = selectedVariables['FIN_GetFinanceNotes']
            FIN_GetDataCompareToFinanceSettlements = selectedVariables['FIN_GetDataCompareToFinanceSettlements']
            FIN_GetDataCompareToVoucherDetail = selectedVariables['FIN_GetDataCompareToVoucherDetail']
            FIN_GetFinanceSettlements = selectedVariables['FIN_GetFinanceSettlements']
            FIN_GetVoucherSettlementBackHistory = selectedVariables['FIN_GetVoucherSettlementBackHistory']
            FIN_GetDataCompareToVoucherSettlementBackHistory = selectedVariables['FIN_GetDataCompareToVoucherSettlementBackHistory']
            FIN_GetDataCompareToFinanceNotesSuspend = selectedVariables['FIN_GetDataCompareToFinanceNotesSuspend']
            FIN_GetFinanceNotesSuspend = selectedVariables['FIN_GetFinanceNotesSuspend']
            FIN_GetSuspendMonitoring = selectedVariables['FIN_GetSuspendMonitoring']
            FIN_GetDataCompareToSuspendMonitoring = selectedVariables['FIN_GetDataCompareToSuspendMonitoring']
            FIN_GetDataCompareToVoucher = selectedVariables['FIN_GetDataCompareToVoucher']
            FIN_GetNoteData = selectedVariables['FIN_GetNoteData']
            FIN_GetBaseNoteDataToCalculateTax = selectedVariables['FIN_GetBaseNoteDataToCalculateTax']
            FIN_GetDataCompareToVoucherDetailOthers = selectedVariables['FIN_GetDataCompareToVoucherDetailOthers']
            FIN_GetInquiryVoucher = selectedVariables['FIN_GetInquiryVoucher']
            FIN_GetDataCompareToWTVoucherDetail = selectedVariables['FIN_GetDataCompareToWTVoucherDetail']
            FIN_GetWTVoucherDetail = selectedVariables['FIN_GetWTVoucherDetail']
            FIN_GetWTVoucher = selectedVariables['FIN_GetWTVoucher']
            BEY_GetCdEntity = selectedVariables['BEY_GetCdEntity']
            SEA_GetFSL = selectedVariables['SEA_GetFSL']
            SEA_GetnVoucher = selectedVariables['SEA_GetnVoucher']
            SEA_GetpVoucher = selectedVariables['SEA_GetpVoucher']
            SEA_GetfVoucher = selectedVariables['SEA_GetfVoucher']
            SEA_GetDataCompareToFSL = selectedVariables['SEA_GetDataCompareToFSL']
            SEA_GetDataCompareTonVoucher = selectedVariables['SEA_GetDataCompareTonVoucher']
            SEA_GetDataCompareTopVoucher = selectedVariables['SEA_GetDataCompareTopVoucher']
            SEA_GetDataCompareTofVoucher = selectedVariables['SEA_GetDataCompareTofVoucher']
            SEA_UpdatePostingFinanceVoucherQueue = selectedVariables['SEA_UpdatePostingFinanceVoucherQueue']
            SEA_GetVoucherRefSettled = selectedVariables['SEA_GetVoucherRefSettled']
            SEA_GetPostingFinanceVoucherQueue = selectedVariables['SEA_GetPostingFinanceVoucherQueue']
            FIN_GetInquiryVoucher2 = selectedVariables['FIN_GetInquiryVoucher2']
            FIN_GetDataInquiryNote = selectedVariables['FIN_GetDataInquiryNote']
            FIN_GetParamInquiryNote = selectedVariables['FIN_GetParamInquiryNote']
            FIN_GetParamInquiryVoucher = selectedVariables['FIN_GetParamInquiryVoucher']
            FIN_GetNextUser = selectedVariables['FIN_GetNextUser']
            BEY_GetInitialDataCompareToFinanceNotes = selectedVariables['BEY_GetInitialDataCompareToFinanceNotes']
            BEY_GetInitialDataCompareToFinanceNoteInfos = selectedVariables['BEY_GetInitialDataCompareToFinanceNoteInfos']
            BEY_GetInitialDataCompareToFinanceNoteAdditional = selectedVariables['BEY_GetInitialDataCompareToFinanceNoteAdditional']
            MIS_GetDataCIF = selectedVariables['MIS_GetDataCIF']
            BEY_GetDataStatusNota = selectedVariables['BEY_GetDataStatusNota']
            BEY_GetInitialDataCompareToFinanceSettlements = selectedVariables['BEY_GetInitialDataCompareToFinanceSettlements']
            BEY_GetInitialDataCompareToVoucher = selectedVariables['BEY_GetInitialDataCompareToVoucher']
            BEY_GetInitialDataCompareToVoucherDetail = selectedVariables['BEY_GetInitialDataCompareToVoucherDetail']
            BEY_GetInitialDataCompareToVoucherDetailOffshore = selectedVariables['BEY_GetInitialDataCompareToVoucherDetailOffshore']
            BEY_GetInitialDataCompareToVoucherDetailProgressiveTax = selectedVariables['BEY_GetInitialDataCompareToVoucherDetailProgressiveTax']
            BEY_GetInitialDataCompareToCustomer = selectedVariables['BEY_GetInitialDataCompareToCustomer']
            BEY_GetInitialDataCompareToCustomerPersonal = selectedVariables['BEY_GetInitialDataCompareToCustomerPersonal']
            BEY_GetInitialDataCompareToCustomerCompany = selectedVariables['BEY_GetInitialDataCompareToCustomerCompany']
            BEY_GetInitialDataCompareToCustomerAccount = selectedVariables['BEY_GetInitialDataCompareToCustomerAccount']
            BEY_GetInitialDataCompareToCustomerUsage = selectedVariables['BEY_GetInitialDataCompareToCustomerUsage']
            BEY_GetInitialDataCompareToCustomerPIC = selectedVariables['BEY_GetInitialDataCompareToCustomerPIC']
            BEY_GetInitialDataCompareToCustomerAddress = selectedVariables['BEY_GetInitialDataCompareToCustomerAddress']
            FIN_SSISDataFIN_Voucher = selectedVariables['FIN_SSISDataFIN_Voucher']
            FIN_SSISDataFIN_VoucherDetail = selectedVariables['FIN_SSISDataFIN_VoucherDetail']
            FIN_SSISDataFIN_VoucherDetailOffshore = selectedVariables['FIN_SSISDataFIN_VoucherDetailOffshore']
            FIN_SSISDataFIN_FinanceNotes = selectedVariables['FIN_SSISDataFIN_FinanceNotes']
            FIN_SSISDataFIN_FinanceNoteInfos = selectedVariables['FIN_SSISDataFIN_FinanceNoteInfos']
            FIN_SSISDataFIN_FinanceNoteAdditional = selectedVariables['FIN_SSISDataFIN_FinanceNoteAdditional']
            FIN_SSISDailyData_FinanceNotes = selectedVariables['FIN_SSISDailyData_FinanceNotes']
            FIN_SSISDailyData_FinanceNoteInfos = selectedVariables['FIN_SSISDailyData_FinanceNoteInfos']
            FIN_SSISDailyData_FinanceNoteAdditional = selectedVariables['FIN_SSISDailyData_FinanceNoteAdditional']
            FIN_SSISDailyData_Customer = selectedVariables['FIN_SSISDailyData_Customer']
            FIN_SSISDailyData_CustomerPersonal = selectedVariables['FIN_SSISDailyData_CustomerPersonal']
            FIN_SSISDailyData_CustomerCompany = selectedVariables['FIN_SSISDailyData_CustomerCompany']
            FIN_SSISDailyData_CustomerAccount = selectedVariables['FIN_SSISDailyData_CustomerAccount']
            FIN_SSISDailyData_CustomerUsage = selectedVariables['FIN_SSISDailyData_CustomerUsage']
            FIN_SSISDailyData_CustomerPIC = selectedVariables['FIN_SSISDailyData_CustomerPIC']
            FIN_SSISDailyData_CustomerAddress = selectedVariables['FIN_SSISDailyData_CustomerAddress']
            FIN_SSISDailyData_Voucher = selectedVariables['FIN_SSISDailyData_Voucher']
            FIN_SSISDailyData_VoucherDetail = selectedVariables['FIN_SSISDailyData_VoucherDetail']
            FIN_SSISDailyData_VoucherDetailOffshore = selectedVariables['FIN_SSISDailyData_VoucherDetailOffshore']
            FIN_SSISDailyData_VoucherDetailProgressiveTax = selectedVariables['FIN_SSISDailyData_VoucherDetailProgressiveTax']
            FIN_SSISDataFIN_Customer = selectedVariables['FIN_SSISDataFIN_Customer']
            FIN_SSISDataFIN_CustomerPersonal = selectedVariables['FIN_SSISDataFIN_CustomerPersonal']
            FIN_SSISDataFIN_CustomerCompany = selectedVariables['FIN_SSISDataFIN_CustomerCompany']
            FIN_SSISDataFIN_CustomerAccount = selectedVariables['FIN_SSISDataFIN_CustomerAccount']
            FIN_SSISDataFIN_CustomerUsage = selectedVariables['FIN_SSISDataFIN_CustomerUsage']
            FIN_SSISDataFIN_CustomerPIC = selectedVariables['FIN_SSISDataFIN_CustomerPIC']
            FIN_SSISDataFIN_CustomerAddress = selectedVariables['FIN_SSISDataFIN_CustomerAddress']
            FIN_SSISDataFIN_VoucherDetailProgressiveTax = selectedVariables['FIN_SSISDataFIN_VoucherDetailProgressiveTax']
            FIN_SSISInitialData_FinanceNotes = selectedVariables['FIN_SSISInitialData_FinanceNotes']
            FIN_SSISInitialData_FinanceNoteInfos = selectedVariables['FIN_SSISInitialData_FinanceNoteInfos']
            FIN_SSISInitialData_FinanceNoteAdditional = selectedVariables['FIN_SSISInitialData_FinanceNoteAdditional']
            FIN_SSISInitialData_Customer = selectedVariables['FIN_SSISInitialData_Customer']
            FIN_SSISInitialData_CustomerPersonal = selectedVariables['FIN_SSISInitialData_CustomerPersonal']
            FIN_SSISInitialData_CustomerCompany = selectedVariables['FIN_SSISInitialData_CustomerCompany']
            FIN_SSISInitialData_CustomerAccount = selectedVariables['FIN_SSISInitialData_CustomerAccount']
            FIN_SSISInitialData_CustomerUsage = selectedVariables['FIN_SSISInitialData_CustomerUsage']
            FIN_SSISInitialData_CustomerPIC = selectedVariables['FIN_SSISInitialData_CustomerPIC']
            FIN_SSISInitialData_CustomerAddress = selectedVariables['FIN_SSISInitialData_CustomerAddress']
            FIN_SSISInitialData_Voucher = selectedVariables['FIN_SSISInitialData_Voucher']
            FIN_SSISInitialData_VoucherDetail = selectedVariables['FIN_SSISInitialData_VoucherDetail']
            FIN_SSISInitialData_VoucherDetailOffshore = selectedVariables['FIN_SSISInitialData_VoucherDetailOffshore']
            FIN_SSISInitialData_VoucherDetailProgressiveTax = selectedVariables['FIN_SSISInitialData_VoucherDetailProgressiveTax']
            FIN_ProcessDateSSISNote = selectedVariables['FIN_ProcessDateSSISNote']
            BEY_GetDataSSISVoucherNo = selectedVariables['BEY_GetDataSSISVoucherNo']
            BEY_GetDataSSISNoteNo = selectedVariables['BEY_GetDataSSISNoteNo']
            FIN_GetCustomerID = selectedVariables['FIN_GetCustomerID']
            FIN_SSISInitialData_FinanceSettlements = selectedVariables['FIN_SSISInitialData_FinanceSettlements']
            FIN_GetNotePayerID_VD = selectedVariables['FIN_GetNotePayerID_VD']
            FIN_SSISDataFIN_FinanceSettlements = selectedVariables['FIN_SSISDataFIN_FinanceSettlements']
            FIN_GetCustomerCode = selectedVariables['FIN_GetCustomerCode']
            FIN_SSISDailyData_FinanceSettlements = selectedVariables['FIN_SSISDailyData_FinanceSettlements']
            BEY_UserWorklist = selectedVariables['BEY_UserWorklist']
            FIN_GetDataCompareToSuspendNote = selectedVariables['FIN_GetDataCompareToSuspendNote']
            FIN_GetNoteDataForSuspendHistory = selectedVariables['FIN_GetNoteDataForSuspendHistory']
            FIN_GetDataCompareToSuspendSettlementHistory = selectedVariables['FIN_GetDataCompareToSuspendSettlementHistory']
            FIN_GetInternalDistribution = selectedVariables['FIN_GetInternalDistribution']
            FIN_GetUnit = selectedVariables['FIN_GetUnit']
            FIN_GetDataCompareToUD_SuspendMonitoring = selectedVariables['FIN_GetDataCompareToUD_SuspendMonitoring']
            FIN_GetNoteDataForInquirySuspend = selectedVariables['FIN_GetNoteDataForInquirySuspend']
            FIN_GetDataCompareToInquirySuspend = selectedVariables['FIN_GetDataCompareToInquirySuspend']
            FIN_GetVoucherDataForInquiryVoucher = selectedVariables['FIN_GetVoucherDataForInquiryVoucher']
            FIN_GetDataCompareToVDPS = selectedVariables['FIN_GetDataCompareToVDPS']
            FIN_GetDataCompareToFSPS = selectedVariables['FIN_GetDataCompareToFSPS']
            SEA_GetValidMember = selectedVariables['SEA_GetValidMember']
            SEA_CekEligibilityMember = selectedVariables['SEA_CekEligibilityMember']
            SEA_InsertToEconsultation_claimdetail = selectedVariables['SEA_InsertToEconsultation_claimdetail']
            SEA_GetDataEconsultation_claimdetail = selectedVariables['SEA_GetDataEconsultation_claimdetail']
            SEA_UpdateUnpaidEconsul = selectedVariables['SEA_UpdateUnpaidEconsul']
            SEA_InsertToEConsultationAutomaticVoucherQueue = selectedVariables['SEA_InsertToEConsultationAutomaticVoucherQueue']
            SEA_GetDataEConsultationAutomaticVoucherQueue = selectedVariables['SEA_GetDataEConsultationAutomaticVoucherQueue']
            SEA_GetPaymentType = selectedVariables['SEA_GetPaymentType']
            FIN_GetVoucherFromNote = selectedVariables['FIN_GetVoucherFromNote']
            FIN_DeleteUpdateFIN = selectedVariables['FIN_DeleteUpdateFIN']
            SEA_DeleteUpdateSEA = selectedVariables['SEA_DeleteUpdateSEA']
            FIN_GetRequestPSEconsul = selectedVariables['FIN_GetRequestPSEconsul']
            FIN_GetRemarkROEconsul = selectedVariables['FIN_GetRemarkROEconsul']
            SEA_GetAmountDataVoucher = selectedVariables['SEA_GetAmountDataVoucher']
            SEA_DeleteVoucherRequest = selectedVariables['SEA_DeleteVoucherRequest']
            FIN_GeneratePSEconsul = selectedVariables['FIN_GeneratePSEconsul']
            FIN_GetStatusPSEconsul = selectedVariables['FIN_GetStatusPSEconsul']
            FIN_GetVoucherForInquiry = selectedVariables['FIN_GetVoucherForInquiry']
            FIN_GetBankForSearch = selectedVariables['FIN_GetBankForSearch']
            FIN_GetAABAccountForSearch = selectedVariables['FIN_GetAABAccountForSearch']
            FIN_GetAABAccount = selectedVariables['FIN_GetAABAccount']
            FIN_GetInternalDistributionForSearch = selectedVariables['FIN_GetInternalDistributionForSearch']
            FIN_GetParamSubmitAABAccount = selectedVariables['FIN_GetParamSubmitAABAccount']
            FIN_DeleteAABAccount = selectedVariables['FIN_DeleteAABAccount']
            FIN_UpdateRowStatus = selectedVariables['FIN_UpdateRowStatus']
            FIN_UpdateVoucherROEconsul = selectedVariables['FIN_UpdateVoucherROEconsul']
            FIN_GetLatestApprovalHistory = selectedVariables['FIN_GetLatestApprovalHistory']
            FIN_GetSuspendCategoryEconsul = selectedVariables['FIN_GetSuspendCategoryEconsul']
            FIN_GetMappingAABAccountSEA = selectedVariables['FIN_GetMappingAABAccountSEA']
            FIN_PaymentMethod = selectedVariables['FIN_PaymentMethod']
            FIN_GetSuspendEconsul = selectedVariables['FIN_GetSuspendEconsul']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
