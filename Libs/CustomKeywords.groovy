
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import java.util.ArrayList

import com.kms.katalon.core.testobject.TestObject

import com.kms.katalon.core.testobject.ResponseObject


def static "com.keyword.General.convertdate"(
    	String Date	) {
    (new com.keyword.General()).convertdate(
        	Date)
}

def static "com.keyword.UI.verifyStaging"() {
    (new com.keyword.UI()).verifyStaging()
}

def static "com.keyword.UI.connectDB"(
    	String IP	
     , 	String dbname	) {
    (new com.keyword.UI()).connectDB(
        	IP
         , 	dbname)
}

def static "com.keyword.UI.executeQuery"(
    	String queryString	) {
    (new com.keyword.UI()).executeQuery(
        	queryString)
}

def static "com.keyword.UI.CallableState"(
    	String queryString	) {
    (new com.keyword.UI()).CallableState(
        	queryString)
}

def static "com.keyword.UI.execute"(
    	String queryString	) {
    (new com.keyword.UI()).execute(
        	queryString)
}

def static "com.keyword.UI.countdbColumn"(
    	String queryTable	) {
    (new com.keyword.UI()).countdbColumn(
        	queryTable)
}

def static "com.keyword.UI.countdbRow"(
    	String queryTable	) {
    (new com.keyword.UI()).countdbRow(
        	queryTable)
}

def static "com.keyword.UI.SetGlobal"(
    	String varName	
     , 	String App	) {
    (new com.keyword.UI()).SetGlobal(
        	varName
         , 	App)
}

def static "com.keyword.UI.getValueDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new com.keyword.UI()).getValueDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	ColumnName)
}

def static "com.keyword.UI.updateValueDatabase"(
    	String IP	
     , 	String dbname	
     , 	String updateQuery	) {
    (new com.keyword.UI()).updateValueDatabase(
        	IP
         , 	dbname
         , 	updateQuery)
}

def static "com.keyword.UI.getOneRowDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).getOneRowDatabase(
        	IP
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getOneColumnDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new com.keyword.UI()).getOneColumnDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	ColumnName)
}

def static "com.keyword.UI.getAllDataDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).getAllDataDatabase(
        	IP
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getSpecificDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	int row	
     , 	int column	) {
    (new com.keyword.UI()).getSpecificDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	row
         , 	column)
}

def static "com.keyword.UI.compareRowDBtoArray"(
    	String url	
     , 	String dbname	
     , 	String queryTable	
     , 	ArrayList listData	) {
    (new com.keyword.UI()).compareRowDBtoArray(
        	url
         , 	dbname
         , 	queryTable
         , 	listData)
}

def static "com.keyword.UI.CompareFieldtoDatabase"(
    	ArrayList ObjRep	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).CompareFieldtoDatabase(
        	ObjRep
         , 	url
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getFieldsValue"(
    	ArrayList ObjRep	) {
    (new com.keyword.UI()).getFieldsValue(
        	ObjRep)
}

def static "com.keyword.UI.closeDatabaseConnection"() {
    (new com.keyword.UI()).closeDatabaseConnection()
}

def static "com.keyword.UI.newTestObject"(
    	String locator	) {
    (new com.keyword.UI()).newTestObject(
        	locator)
}

def static "com.keyword.UI.AccessURL"(
    	String App	) {
    (new com.keyword.UI()).AccessURL(
        	App)
}

def static "com.keyword.UI.AccessBrowser"(
    	String URL	) {
    (new com.keyword.UI()).AccessBrowser(
        	URL)
}

def static "com.keyword.UI.Sleep"(
    	int timeOut	) {
    (new com.keyword.UI()).Sleep(
        	timeOut)
}

def static "com.keyword.UI.Write"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).Write(
        	xpath
         , 	text)
}

def static "com.keyword.UI.WaitElement"(
    	TestObject xpath	) {
    (new com.keyword.UI()).WaitElement(
        	xpath)
}

def static "com.keyword.UI.Click"(
    	TestObject xpath	) {
    (new com.keyword.UI()).Click(
        	xpath)
}

def static "com.keyword.UI.DoubleClick"(
    	TestObject xpath	) {
    (new com.keyword.UI()).DoubleClick(
        	xpath)
}

def static "com.keyword.UI.DragAndDrop"(
    	TestObject sourceXpath	
     , 	TestObject destinationXpath	) {
    (new com.keyword.UI()).DragAndDrop(
        	sourceXpath
         , 	destinationXpath)
}

def static "com.keyword.UI.Back"() {
    (new com.keyword.UI()).Back()
}

def static "com.keyword.UI.HoverItem"(
    	TestObject xpath	) {
    (new com.keyword.UI()).HoverItem(
        	xpath)
}

def static "com.keyword.UI.DeleteWrite"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).DeleteWrite(
        	xpath
         , 	text)
}

def static "com.keyword.UI.SkipWrite"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).SkipWrite(
        	xpath
         , 	text)
}

def static "com.keyword.UI.ComboBoxSearch"(
    	TestObject Combo1	
     , 	TestObject Combo2	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBoxSearch(
        	Combo1
         , 	Combo2
         , 	Value)
}

def static "com.keyword.UI.ComboBoxSearchSkip"(
    	TestObject comboOpen	
     , 	TestObject comboSearch	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBoxSearchSkip(
        	comboOpen
         , 	comboSearch
         , 	Value)
}

def static "com.keyword.UI.ComboBox"(
    	TestObject Combo	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBox(
        	Combo
         , 	Value)
}

def static "com.keyword.UI.MultiSelectComboBox"(
    	TestObject Combo	
     , 	String Value1	
     , 	String Value2	
     , 	String Value3	
     , 	String Value4	) {
    (new com.keyword.UI()).MultiSelectComboBox(
        	Combo
         , 	Value1
         , 	Value2
         , 	Value3
         , 	Value4)
}

def static "com.keyword.UI.CheckDisableandWrite"(
    	TestObject Xpath	
     , 	String text	) {
    (new com.keyword.UI()).CheckDisableandWrite(
        	Xpath
         , 	text)
}

def static "com.keyword.UI.RunningPhoneNumber"(
    	TestObject xpath	) {
    (new com.keyword.UI()).RunningPhoneNumber(
        	xpath)
}

def static "com.keyword.UI.UploadFile"(
    	String fileLocation	
     , 	String pictureName	) {
    (new com.keyword.UI()).UploadFile(
        	fileLocation
         , 	pictureName)
}

def static "com.keyword.UI.UploadFile2"(
    	String pictureName	) {
    (new com.keyword.UI()).UploadFile2(
        	pictureName)
}

def static "com.keyword.UI.RunScheduler"(
    	String path	) {
    (new com.keyword.UI()).RunScheduler(
        	path)
}

def static "com.keyword.UI.WriteAllRowsXls"(
    	String path	
     , 	int row	
     , 	ArrayList value	) {
    (new com.keyword.UI()).WriteAllRowsXls(
        	path
         , 	row
         , 	value)
}

def static "com.keyword.UI.WriteSingleCellXls"(
    	String path	
     , 	int row	
     , 	int column	
     , 	Object value	) {
    (new com.keyword.UI()).WriteSingleCellXls(
        	path
         , 	row
         , 	column
         , 	value)
}

def static "com.keyword.UI.AccessURLwithPlugin"(
    	String url	
     , 	String Plugin	) {
    (new com.keyword.UI()).AccessURLwithPlugin(
        	url
         , 	Plugin)
}

def static "com.keyword.UI.readQRCode"() {
    (new com.keyword.UI()).readQRCode()
}

def static "com.keyword.UI.getDateToday"(
    	String format	) {
    (new com.keyword.UI()).getDateToday(
        	format)
}

def static "com.keyword.UI.ScreenShot"(
    	String FileName	) {
    (new com.keyword.UI()).ScreenShot(
        	FileName)
}

def static "com.keyword.UI.GlobalVar"(
    	String name	
     , 	Object value	) {
    (new com.keyword.UI()).GlobalVar(
        	name
         , 	value)
}

def static "com.keyword.UI.Note"(
    	Object variable	) {
    (new com.keyword.UI()).Note(
        	variable)
}

def static "db.dbQuery.connectDB"(
    	String url	
     , 	String dbname	
     , 	String username	
     , 	String password	) {
    (new db.dbQuery()).connectDB(
        	url
         , 	dbname
         , 	username
         , 	password)
}

def static "db.dbQuery.executeQuery"(
    	String queryString	) {
    (new db.dbQuery()).executeQuery(
        	queryString)
}

def static "com.keyword.API.verifyStatusCode"(
    	TestObject request	
     , 	int expectedStatusCode	) {
    (new com.keyword.API()).verifyStatusCode(
        	request
         , 	expectedStatusCode)
}

def static "com.keyword.API.addBasicAuthorizationProperty"(
    	TestObject request	
     , 	String username	
     , 	String password	) {
    (new com.keyword.API()).addBasicAuthorizationProperty(
        	request
         , 	username
         , 	password)
}

def static "com.keyword.API.GetFullResponse"(
    	String EndPoint	
     , 	String bodyParam	
     , 	String contentType	
     , 	String Authorization	
     , 	String Cookie	) {
    (new com.keyword.API()).GetFullResponse(
        	EndPoint
         , 	bodyParam
         , 	contentType
         , 	Authorization
         , 	Cookie)
}

def static "com.keyword.API.getResponseData"(
    	ResponseObject Request	) {
    (new com.keyword.API()).getResponseData(
        	Request)
}

def static "com.keyword.GEN5.SideMenu"(
    	String ParentMenu	
     , 	String ChildMenu	) {
    (new com.keyword.GEN5()).SideMenu(
        	ParentMenu
         , 	ChildMenu)
}

def static "com.keyword.GEN5.DatePicker"(
    	String DateNow	
     , 	TestObject DatePickerDiv	) {
    (new com.keyword.GEN5()).DatePicker(
        	DateNow
         , 	DatePickerDiv)
}

def static "com.keyword.GEN5.getAllColumnValue"(
    	TestObject tableXpath	
     , 	String gridColumn	) {
    (new com.keyword.GEN5()).getAllColumnValue(
        	tableXpath
         , 	gridColumn)
}

def static "com.keyword.GEN5.getAllRowsValue"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	) {
    (new com.keyword.GEN5()).getAllRowsValue(
        	tableXpath
         , 	columnHeader
         , 	RowsValue)
}

def static "com.keyword.GEN5.getAllDataTable"(
    	TestObject tableXpath	) {
    (new com.keyword.GEN5()).getAllDataTable(
        	tableXpath)
}

def static "com.keyword.GEN5.getAllDataTableMultiPage"(
    	TestObject tableXpath	
     , 	TestObject ButtonNext	) {
    (new com.keyword.GEN5()).getAllDataTableMultiPage(
        	tableXpath
         , 	ButtonNext)
}

def static "com.keyword.GEN5.CompareRowsValue"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	
     , 	ArrayList RowsCompare	) {
    (new com.keyword.GEN5()).CompareRowsValue(
        	tableXpath
         , 	columnHeader
         , 	RowsValue
         , 	RowsCompare)
}

def static "com.keyword.GEN5.CompareColumnsValue"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	ArrayList CompareColumn	) {
    (new com.keyword.GEN5()).CompareColumnsValue(
        	tableXpath
         , 	gridColumn
         , 	CompareColumn)
}

def static "com.keyword.GEN5.ClickExpectedRow"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String columnValue	) {
    (new com.keyword.GEN5()).ClickExpectedRow(
        	tableXpath
         , 	gridColumn
         , 	columnValue)
}

def static "com.keyword.GEN5.ClickExpectedRowWithNext"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String columnValue	
     , 	TestObject ButtonNext	) {
    (new com.keyword.GEN5()).ClickExpectedRowWithNext(
        	tableXpath
         , 	gridColumn
         , 	columnValue
         , 	ButtonNext)
}

def static "com.keyword.GEN5.ProcessingCommand"() {
    (new com.keyword.GEN5()).ProcessingCommand()
}

def static "com.keyword.GEN5.CompareColumnToDatabase"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	
     , 	String getColumn	) {
    (new com.keyword.GEN5()).CompareColumnToDatabase(
        	tableXpath
         , 	gridColumn
         , 	url
         , 	dbname
         , 	queryTable
         , 	getColumn)
}

def static "com.keyword.GEN5.CompareRowToDatabase"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.GEN5()).CompareRowToDatabase(
        	tableXpath
         , 	columnHeader
         , 	RowsValue
         , 	url
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.GEN5.InsertIntoDataHealth"(
    	String type	
     , 	String AppName	
     , 	String value	) {
    (new com.keyword.GEN5()).InsertIntoDataHealth(
        	type
         , 	AppName
         , 	value)
}

def static "com.keyword.GEN5.getDataFromDataHealth"(
    	String Parameter	
     , 	String ParamValue	
     , 	String Value	) {
    (new com.keyword.GEN5()).getDataFromDataHealth(
        	Parameter
         , 	ParamValue
         , 	Value)
}

def static "com.keyword.GEN5.getPopUpText"(
    	TestObject TextOnPopup	) {
    (new com.keyword.GEN5()).getPopUpText(
        	TextOnPopup)
}

def static "com.keyword.GEN5.tickAllCheckboxInTable"(
    	TestObject Object	) {
    (new com.keyword.GEN5()).tickAllCheckboxInTable(
        	Object)
}

def static "db.UI.verifyStaging"() {
    (new db.UI()).verifyStaging()
}

def static "db.UI.connectDB"(
    	String IP	
     , 	String dbname	) {
    (new db.UI()).connectDB(
        	IP
         , 	dbname)
}

def static "db.UI.executeQuery"(
    	String queryString	) {
    (new db.UI()).executeQuery(
        	queryString)
}

def static "db.UI.execute"(
    	String queryString	) {
    (new db.UI()).execute(
        	queryString)
}

def static "db.UI.countdbColumn"(
    	String queryTable	) {
    (new db.UI()).countdbColumn(
        	queryTable)
}

def static "db.UI.countdbRow"(
    	String queryTable	) {
    (new db.UI()).countdbRow(
        	queryTable)
}

def static "db.UI.getValueDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new db.UI()).getValueDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	ColumnName)
}

def static "db.UI.updateValueDatabase"(
    	String IP	
     , 	String dbname	
     , 	String updateQuery	) {
    (new db.UI()).updateValueDatabase(
        	IP
         , 	dbname
         , 	updateQuery)
}

def static "db.UI.getOneRowDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new db.UI()).getOneRowDatabase(
        	IP
         , 	dbname
         , 	queryTable)
}

def static "db.UI.getOneColumnDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new db.UI()).getOneColumnDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	ColumnName)
}

def static "db.UI.getAllDataDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new db.UI()).getAllDataDatabase(
        	IP
         , 	dbname
         , 	queryTable)
}

def static "db.UI.getSpecificDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	int row	
     , 	int column	) {
    (new db.UI()).getSpecificDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	row
         , 	column)
}

def static "db.UI.compareRowDBtoArray"(
    	String url	
     , 	String dbname	
     , 	String queryTable	
     , 	ArrayList listData	) {
    (new db.UI()).compareRowDBtoArray(
        	url
         , 	dbname
         , 	queryTable
         , 	listData)
}

def static "db.UI.CompareFieldtoDatabase"(
    	ArrayList ObjRep	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	) {
    (new db.UI()).CompareFieldtoDatabase(
        	ObjRep
         , 	url
         , 	dbname
         , 	queryTable)
}

def static "db.UI.getFieldsValue"(
    	ArrayList ObjRep	) {
    (new db.UI()).getFieldsValue(
        	ObjRep)
}

def static "db.UI.closeDatabaseConnection"() {
    (new db.UI()).closeDatabaseConnection()
}

def static "db.UI.newTestObject"(
    	String locator	) {
    (new db.UI()).newTestObject(
        	locator)
}

def static "db.UI.AccessURL"(
    	String App	) {
    (new db.UI()).AccessURL(
        	App)
}

def static "db.UI.AccessBrowser"(
    	String URL	) {
    (new db.UI()).AccessBrowser(
        	URL)
}

def static "db.UI.Sleep"(
    	int timeOut	) {
    (new db.UI()).Sleep(
        	timeOut)
}

def static "db.UI.Write"(
    	TestObject xpath	
     , 	String text	) {
    (new db.UI()).Write(
        	xpath
         , 	text)
}

def static "db.UI.WaitElement"(
    	TestObject xpath	) {
    (new db.UI()).WaitElement(
        	xpath)
}

def static "db.UI.Click"(
    	TestObject xpath	) {
    (new db.UI()).Click(
        	xpath)
}

def static "db.UI.DoubleClick"(
    	TestObject xpath	) {
    (new db.UI()).DoubleClick(
        	xpath)
}

def static "db.UI.DragAndDrop"(
    	TestObject sourceXpath	
     , 	TestObject destinationXpath	) {
    (new db.UI()).DragAndDrop(
        	sourceXpath
         , 	destinationXpath)
}

def static "db.UI.Back"() {
    (new db.UI()).Back()
}

def static "db.UI.HoverItem"(
    	TestObject xpath	) {
    (new db.UI()).HoverItem(
        	xpath)
}

def static "db.UI.DeleteWrite"(
    	TestObject xpath	
     , 	String text	) {
    (new db.UI()).DeleteWrite(
        	xpath
         , 	text)
}

def static "db.UI.SkipWrite"(
    	TestObject xpath	
     , 	String text	) {
    (new db.UI()).SkipWrite(
        	xpath
         , 	text)
}

def static "db.UI.ComboBoxSearch"(
    	TestObject Combo1	
     , 	TestObject Combo2	
     , 	String Value	) {
    (new db.UI()).ComboBoxSearch(
        	Combo1
         , 	Combo2
         , 	Value)
}

def static "db.UI.ComboBoxSearchSkip"(
    	TestObject comboOpen	
     , 	TestObject comboSearch	
     , 	String Value	) {
    (new db.UI()).ComboBoxSearchSkip(
        	comboOpen
         , 	comboSearch
         , 	Value)
}

def static "db.UI.ComboBox"(
    	TestObject Combo	
     , 	String Value	) {
    (new db.UI()).ComboBox(
        	Combo
         , 	Value)
}

def static "db.UI.MultiSelectComboBox"(
    	TestObject Combo	
     , 	String Value1	
     , 	String Value2	
     , 	String Value3	
     , 	String Value4	) {
    (new db.UI()).MultiSelectComboBox(
        	Combo
         , 	Value1
         , 	Value2
         , 	Value3
         , 	Value4)
}

def static "db.UI.CheckDisableandWrite"(
    	TestObject Xpath	
     , 	String text	) {
    (new db.UI()).CheckDisableandWrite(
        	Xpath
         , 	text)
}

def static "db.UI.RunningPhoneNumber"(
    	TestObject xpath	) {
    (new db.UI()).RunningPhoneNumber(
        	xpath)
}

def static "db.UI.UploadFile"(
    	String fileLocation	
     , 	String pictureName	) {
    (new db.UI()).UploadFile(
        	fileLocation
         , 	pictureName)
}

def static "db.UI.UploadFile2"(
    	String pictureName	) {
    (new db.UI()).UploadFile2(
        	pictureName)
}

def static "db.UI.RunScheduler"(
    	String path	) {
    (new db.UI()).RunScheduler(
        	path)
}

def static "db.UI.WriteAllRowsXls"(
    	String path	
     , 	int row	
     , 	ArrayList value	) {
    (new db.UI()).WriteAllRowsXls(
        	path
         , 	row
         , 	value)
}

def static "db.UI.WriteSingleCellXls"(
    	String path	
     , 	int row	
     , 	int column	
     , 	Object value	) {
    (new db.UI()).WriteSingleCellXls(
        	path
         , 	row
         , 	column
         , 	value)
}

def static "db.UI.AccessURLwithPlugin"(
    	String url	
     , 	String Plugin	) {
    (new db.UI()).AccessURLwithPlugin(
        	url
         , 	Plugin)
}

def static "db.UI.readQRCode"() {
    (new db.UI()).readQRCode()
}

def static "com.keyword.REA.ComboBoxReact"(
    	TestObject Combo	
     , 	String Value	) {
    (new com.keyword.REA()).ComboBoxReact(
        	Combo
         , 	Value)
}

def static "com.keyword.REA.getArrayIcon"(
    	TestObject Object	) {
    (new com.keyword.REA()).getArrayIcon(
        	Object)
}

def static "com.keyword.REA.getArrayButton"(
    	TestObject Object	
     , 	String type	
     , 	String Class	) {
    (new com.keyword.REA()).getArrayButton(
        	Object
         , 	type
         , 	Class)
}

def static "com.keyword.REA.getColumnDataTableUI"(
    	TestObject Table	
     , 	int column	
     , 	boolean displayEmpty	) {
    (new com.keyword.REA()).getColumnDataTableUI(
        	Table
         , 	column
         , 	displayEmpty)
}

def static "com.keyword.REA.datePicker"(
    	String date	
     , 	TestObject FieldDate	) {
    (new com.keyword.REA()).datePicker(
        	date
         , 	FieldDate)
}

def static "com.keyword.REA.datePicker2"(
    	String date	) {
    (new com.keyword.REA()).datePicker2(
        	date)
}

def static "com.keyword.REA.convertDate"(
    	String Date	
     , 	boolean time	) {
    (new com.keyword.REA()).convertDate(
        	Date
         , 	time)
}

def static "com.keyword.REA.getAllText"(
    	TestObject Cover	
     , 	String tagName	) {
    (new com.keyword.REA()).getAllText(
        	Cover
         , 	tagName)
}

def static "com.keyword.REA.getAllColumnValue"(
    	TestObject tableXpath	
     , 	String gridColumn	) {
    (new com.keyword.REA()).getAllColumnValue(
        	tableXpath
         , 	gridColumn)
}

def static "com.keyword.REA.getAllRowsValue"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	) {
    (new com.keyword.REA()).getAllRowsValue(
        	tableXpath
         , 	columnHeader
         , 	RowsValue)
}

def static "com.keyword.REA.CompareRowsValue"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	
     , 	ArrayList RowsCompare	) {
    (new com.keyword.REA()).CompareRowsValue(
        	tableXpath
         , 	columnHeader
         , 	RowsValue
         , 	RowsCompare)
}

def static "com.keyword.REA.CompareColumnsValue"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	ArrayList CompareColumn	) {
    (new com.keyword.REA()).CompareColumnsValue(
        	tableXpath
         , 	gridColumn
         , 	CompareColumn)
}

def static "com.keyword.REA.ClickExpectedRow"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String columnValue	) {
    (new com.keyword.REA()).ClickExpectedRow(
        	tableXpath
         , 	gridColumn
         , 	columnValue)
}

def static "com.keyword.REA.ClickExpectedRowWithNext"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String columnValue	
     , 	TestObject ButtonNext	) {
    (new com.keyword.REA()).ClickExpectedRowWithNext(
        	tableXpath
         , 	gridColumn
         , 	columnValue
         , 	ButtonNext)
}

def static "com.keyword.REA.CompareColumnToDatabase"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	
     , 	String getColumn	) {
    (new com.keyword.REA()).CompareColumnToDatabase(
        	tableXpath
         , 	gridColumn
         , 	url
         , 	dbname
         , 	queryTable
         , 	getColumn)
}

def static "com.keyword.REA.CompareRowToDatabase"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.REA()).CompareRowToDatabase(
        	tableXpath
         , 	columnHeader
         , 	RowsValue
         , 	url
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.REA.getAllDataTable"(
    	TestObject tableXpath	) {
    (new com.keyword.REA()).getAllDataTable(
        	tableXpath)
}

def static "com.keyword.REA.getAllDataTableMultiPage"(
    	TestObject tableXpath	
     , 	TestObject ButtonNext	) {
    (new com.keyword.REA()).getAllDataTableMultiPage(
        	tableXpath
         , 	ButtonNext)
}

def static "com.keyword.REA.compareAllTabletoDatabase"(
    	TestObject tableXpath	
     , 	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.REA()).compareAllTabletoDatabase(
        	tableXpath
         , 	IP
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.REA.compareAllDatabasetoArray"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	ArrayList ArrayName	) {
    (new com.keyword.REA()).compareAllDatabasetoArray(
        	IP
         , 	dbname
         , 	queryTable
         , 	ArrayName)
}
