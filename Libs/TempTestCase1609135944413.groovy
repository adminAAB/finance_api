import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\eum\\AppData\\Local\\Temp\\Katalon\\Test Cases\\Finance\\API\\Scenario\\Penurunan Data\\Nota\\Misc Retail - Realtime\\Sce 392 Nota PRM partial dg nota lain (tambah account)\\20201228_131224\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCase('Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Misc Retail - Realtime/Sce 392 Nota PRM partial dg nota lain (tambah account)', new TestCaseBinding('Test Cases/Finance/API/Scenario/Penurunan Data/Nota/Misc Retail - Realtime/Sce 392 Nota PRM partial dg nota lain (tambah account)',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
