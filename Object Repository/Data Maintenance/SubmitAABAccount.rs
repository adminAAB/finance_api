<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>SubmitAABAccount</name>
   <tag></tag>
   <elementGuidId>c929a0b5-54bf-4234-9e8d-ec9ea4c986ba</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;AABAccountCode\&quot;:\&quot;${AABAccountCode}\&quot;,\&quot;AABAccountDescription\&quot;:\&quot;${AABAccountDescription}\&quot;,\&quot;COACode\&quot;:\&quot;${COACode}\&quot;,\&quot;AccountNo\&quot;:\&quot;${AccountNo}\&quot;,\&quot;AccountName\&quot;:\&quot;${AccountName}\&quot;,\&quot;CurrencyID\&quot;:${CurrencyID},\&quot;InternalDistributionID\&quot;:${InternalDistributionID},\&quot;IsDC\&quot;:\&quot;${IsDC}\&quot;,\&quot;BankID\&quot;:${BankID},\&quot;BankBranchID\&quot;:${BankBranchID},\&quot;CompanyID\&quot;:\&quot;${CompanyID}\&quot;,\&quot;TypeOfSyariahTransaction\&quot;:null,\&quot;ActiveStatusID\&quot;:${ActiveStatusID},\&quot;Action\&quot;:\&quot;${Action}\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/DataMaintenanceAPI/SubmitAABAccount</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
