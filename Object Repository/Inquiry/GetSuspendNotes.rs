<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GetSuspendNotes</name>
   <tag></tag>
   <elementGuidId>880e026e-be02-4264-bfcb-a37c9b72d1e0</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;JournalTypeCode\&quot;:\&quot;${GlobalVariable.JournalType}\&quot;,\&quot;SubJournalTypeCode\&quot;:\&quot;${GlobalVariable.SubJournalType}\&quot;,\&quot;InternalDistID\&quot;:0,\&quot;DirectorateID\&quot;:0,\&quot;AABAccountID\&quot;:0,\&quot;UnitCode\&quot;:\&quot;\&quot;,\&quot;SuspendStatus\&quot;:\&quot;${GlobalVariable.SuspendStatus}\&quot;,\&quot;SuspendResultCategoryID\&quot;:0,\&quot;TaxTypeCode\&quot;:\&quot;\&quot;,\&quot;EntryUsr\&quot;:\&quot;\&quot;,\&quot;StartPeriod\&quot;:\&quot;\&quot;,\&quot;EndPeriod\&quot;:\&quot;\&quot;,\&quot;NoteNumber\&quot;:\&quot;${GlobalVariable.NoteNumber}\&quot;,\&quot;VoucherNo\&quot;:\&quot;${GlobalVariable.VoucherNo}\&quot;,\&quot;PageIndex\&quot;:1,\&quot;PageSize\&quot;:10,\&quot;SortBy\&quot;:\&quot;\&quot;,\&quot;SortDir\&quot;:\&quot;\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/FinanceNotesAPI/GetSuspendNotes</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
