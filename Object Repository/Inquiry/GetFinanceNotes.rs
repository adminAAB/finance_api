<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GetFinanceNotes</name>
   <tag></tag>
   <elementGuidId>3a8ba881-36b0-43f7-8a05-bddf841931c6</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;PageNo\&quot;:${GlobalVariable.PageNo},\&quot;PageSize\&quot;:10,\&quot;SortBy\&quot;:\&quot;\&quot;,\&quot;SortDir\&quot;:\&quot;\&quot;,\&quot;PolicyNo\&quot;:\&quot;${GlobalVariable.PolicyNo}\&quot;,\&quot;ClaimNo\&quot;:\&quot;${GlobalVariable.ClaimNo}\&quot;,\&quot;NoteNo\&quot;:\&quot;${GlobalVariable.NoteNo}\&quot;,\&quot;GroupNo\&quot;:\&quot;${GlobalVariable.GroupingNo}\&quot;,\&quot;CustomerName\&quot;:\&quot;${GlobalVariable.CustomerName}\&quot;,\&quot;PayerName\&quot;:\&quot;${GlobalVariable.PayerName}\&quot;,\&quot;JournalType\&quot;:\&quot;${GlobalVariable.JournalType}\&quot;,\&quot;SubJournalType\&quot;:\&quot;${GlobalVariable.SubJournalType}\&quot;,\&quot;NoteStatus\&quot;:\&quot;${GlobalVariable.NoteStatus}\&quot;,\&quot;ProductionDateFrom\&quot;:\&quot;${GlobalVariable.ProductionDateFrom}\&quot;,\&quot;ProductionDateTo\&quot;:\&quot;${GlobalVariable.ProductionDateTo}\&quot;,\&quot;ClientName\&quot;:\&quot;${GlobalVariable.ClientName}\&quot;,\&quot;MemberName\&quot;:\&quot;${GlobalVariable.MemberName}\&quot;,\&quot;EmployeeID\&quot;:\&quot;${GlobalVariable.EmployeeID}\&quot;,\&quot;MemberNo\&quot;:\&quot;${GlobalVariable.MemberNo}\&quot;,\&quot;isHoldFlag\&quot;:\&quot;${GlobalVariable.isHoldFlag}\&quot;,\&quot;OtherReferenceType\&quot;:\&quot;${GlobalVariable.OtherReferenceType}\&quot;,\&quot;OtherReferenceValue\&quot;:\&quot;${GlobalVariable.OtherReferenceValue}\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/FinanceNotesAPI/GetFinanceNotes</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
