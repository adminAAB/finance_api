<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>UpdateDetailsSuspend</name>
   <tag></tag>
   <elementGuidId>5d3f7d1e-2e63-407a-b223-ca64ea7894f8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;NoteNumber\&quot;:[\&quot;${GlobalVariable.NoteNumber}\&quot;],\&quot;SuspendResultCategoryID\&quot;:${GlobalVariable.SuspendResultCategoryID},\&quot;InternalDistributionID\&quot;:${GlobalVariable.InternalDistributionID},\&quot;InternalDistributionCode\&quot;:\&quot;${GlobalVariable.InternalDistributionCode}\&quot;,\&quot;InternalDistributionDescription\&quot;:\&quot;${GlobalVariable.InternalDistributionDescription}\&quot;,\&quot;SalesmanCode\&quot;:\&quot;${GlobalVariable.SalesmanCode}\&quot;,\&quot;SalesmanName\&quot;:\&quot;${GlobalVariable.SalesmanName}\&quot;,\&quot;PayerCode\&quot;:\&quot;${GlobalVariable.PayerCode}\&quot;,\&quot;PayerName\&quot;:\&quot;${GlobalVariable.PayerName}\&quot;,\&quot;PICCode\&quot;:\&quot;${GlobalVariable.PICCode}\&quot;,\&quot;PICName\&quot;:\&quot;${GlobalVariable.PICName}\&quot;,\&quot;UnitCode\&quot;:\&quot;${GlobalVariable.UnitCode}\&quot;,\&quot;UnitDescription\&quot;:\&quot;${GlobalVariable.UnitDescription}\&quot;,\&quot;NoteRemarks\&quot;:\&quot;${GlobalVariable.Remarks}\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/FinanceNotesAPI/UpdateDetailsSuspend</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
