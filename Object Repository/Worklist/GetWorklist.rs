<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GetWorklist</name>
   <tag></tag>
   <elementGuidId>f024fbc3-0710-4d91-ac3c-04ae31c1f9d8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;VoucherNo\&quot;:\&quot;${voucherNo}\&quot;,\&quot;PINo\&quot;:\&quot;\&quot;,\&quot;ApprovalStage\&quot;:\&quot;\&quot;,\&quot;UserID\&quot;:\&quot;\&quot;,\&quot;AABAccountID\&quot;:0,\&quot;CompanyID\&quot;:0,\&quot;DirectorateCode\&quot;:\&quot;\&quot;,\&quot;SubmitDateFrom\&quot;:\&quot;\&quot;,\&quot;SubmitDateTo\&quot;:\&quot;\&quot;,\&quot;PageNo\&quot;:1,\&quot;PageSize\&quot;:10,\&quot;SortBy\&quot;:\&quot;\&quot;,\&quot;SortDir\&quot;:\&quot;\&quot;,\&quot;ListApprovalStage\&quot;:\&quot;\&quot;,\&quot;listOOFUserLogin\&quot;:\&quot;DPU\&quot;,\&quot;listCdEntityUserLogin\&quot;:\&quot;FIN332\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <katalonVersion>7.8.0</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/WorklistAPI/GetWorklist</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
