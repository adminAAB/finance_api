<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>SubmitEditNote</name>
   <tag></tag>
   <elementGuidId>75bfca5d-8574-46cf-92ec-f941f44c31c0</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;FinanceNoteID\&quot;:${GlobalVariable.FinanceNotesID},\&quot;WTVoucherDetailID\&quot;:${GlobalVariable.WTVoucherDetailID},\&quot;WTVoucherID\&quot;:${GlobalVariable.WTVoucherID},\&quot;ARAPAmount\&quot;:${GlobalVariable.ARAPAmount},\&quot;NoteRemarks\&quot;:\&quot;\&quot;,\&quot;ROCRequestAmount\&quot;:${GlobalVariable.RequestAmount},\&quot;BCDRate\&quot;:1,\&quot;BCNRate\&quot;:1,\&quot;BCCardChargesType\&quot;:\&quot;\&quot;,\&quot;BCCardCharges\&quot;:0,\&quot;BCBankCharges\&quot;:${GlobalVariable.BankCharges},\&quot;FPOverShortage\&quot;:${GlobalVariable.FPOverShortage},\&quot;IsFullPayment\&quot;:${GlobalVariable.IsFullPayment},\&quot;SDAmount\&quot;:0,\&quot;SDJournalType\&quot;:\&quot;\&quot;,\&quot;SDSubJournalType\&quot;:0,\&quot;SDSuspendCategory\&quot;:0,\&quot;IsSuspend\&quot;:false,\&quot;voucherTypeCode\&quot;:\&quot;RC\&quot;,\&quot;IsVATExclude\&quot;:${GlobalVariable.IsVATExclude},\&quot;IsWitholdingTaxExclude\&quot;:${GlobalVariable.IsWitholdingTaxExclude},\&quot;VATAmount\&quot;:${GlobalVariable.VATAmount},\&quot;WithholdingTaxAmount\&quot;:${GlobalVariable.calculateWitholdingTax},\&quot;PayerID\&quot;:0,\&quot;InternalDistributionCode\&quot;:\&quot;JKT000\&quot;,\&quot;TaxTypeCode\&quot;:\&quot;\&quot;,\&quot;PartnerCode\&quot;:\&quot;\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/VoucherAPI/SubmitEditNote</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
