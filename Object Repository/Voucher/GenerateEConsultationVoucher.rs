<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GenerateEConsultationVoucher</name>
   <tag></tag>
   <elementGuidId>a5edc209-18f3-4423-8dd7-98e027f723ed</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;ObjectParam\&quot;:\&quot;{\\\&quot;ID\\\&quot;:${ID},\\\&quot;RequestCode\\\&quot;:\\\&quot;${GlobalVariable.RequestCode}\\\&quot;,\\\&quot;PaymentMethodCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyCode\\\&quot;:\\\&quot;BT001\\\&quot;,\\\&quot;AABAccountNo\\\&quot;:null,\\\&quot;AABAccountCode\\\&quot;:\\\&quot;GRDMDKA006-IDR\\\&quot;,\\\&quot;Remark\\\&quot;:\\\&quot;E-Cons - Penyelesaian Suspend - JKT00FRO2100D759 - EUM-202103031455-1\\\&quot;,\\\&quot;IsPrintDetailInCashBank\\\&quot;:false,\\\&quot;SecondaryPayerBankCode\\\&quot;:null,\\\&quot;SecondaryPayerBankBranch\\\&quot;:null,\\\&quot;SecondaryPayerBankSwiftCode\\\&quot;:null,\\\&quot;SecondaryPayerAccountNo\\\&quot;:null,\\\&quot;SecondaryPayerAccountHolder\\\&quot;:null,\\\&quot;SecondaryBankAddress\\\&quot;:null,\\\&quot;SecondaryInternationalBankAccountNumber\\\&quot;:null,\\\&quot;SecondarySortCode\\\&quot;:null,\\\&quot;SecondaryClearingCode\\\&quot;:null,\\\&quot;SecondaryCorrespondenceBank\\\&quot;:null,\\\&quot;RequestDate\\\&quot;:\\\&quot;${GlobalVariable.RequestDate}T11:32:35.14Z\\\&quot;,\\\&quot;VoucherTypeCode\\\&quot;:\\\&quot;PS\\\&quot;,\\\&quot;EntryUserInternalDistributionCode\\\&quot;:\\\&quot;JKT00\\\&quot;,\\\&quot;EntryUserInternalDistributionID\\\&quot;:0,\\\&quot;InternalDistributionCode\\\&quot;:\\\&quot;JKT00\\\&quot;,\\\&quot;StatementAccountDate\\\&quot;:null,\\\&quot;StatementAccountRemarks\\\&quot;:null,\\\&quot;ChannelCode\\\&quot;:\\\&quot;DRC\\\&quot;,\\\&quot;EntryUserCode\\\&quot;:\\\&quot;ais\\\&quot;,\\\&quot;DirectorateCode\\\&quot;:\\\&quot;HLT600\\\&quot;,\\\&quot;VoucherStatusCode\\\&quot;:\\\&quot;VS1\\\&quot;,\\\&quot;CurrencyCode\\\&quot;:\\\&quot;IDR\\\&quot;,\\\&quot;FinanceNoteID\\\&quot;:[{\\\&quot;FinanceNoteID\\\&quot;:${FinanceNoteID1},\\\&quot;NoteNo\\\&quot;:\\\&quot;${Notenumber1}\\\&quot;,\\\&quot;RequestAmount\\\&quot;:${RequestAmount1},\\\&quot;Overshortage\\\&quot;:${Overshortage1},\\\&quot;CustomerCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;SwiftCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;ClearingCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;AccountNo\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;AccountHolder\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;Remark\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;IsFullPayment\\\&quot;:1,\\\&quot;TransactionReference\\\&quot;:\\\&quot;\\\&quot;},{\\\&quot;FinanceNoteID\\\&quot;:${FinanceNoteID2},\\\&quot;NoteNo\\\&quot;:\\\&quot;${Notenumber2}\\\&quot;,\\\&quot;RequestAmount\\\&quot;:${RequestAmount2},\\\&quot;Overshortage\\\&quot;:${Overshortage2},\\\&quot;CustomerCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;SwiftCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;ClearingCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;AccountNo\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;AccountHolder\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;Remark\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;IsFullPayment\\\&quot;:1,\\\&quot;TransactionReference\\\&quot;:\\\&quot;\\\&quot;}]}\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/VoucherAPI/GenerateEConsultationVoucher</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
