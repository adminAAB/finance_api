<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>SubmitVoucher</name>
   <tag></tag>
   <elementGuidId>dff1f5d9-cffb-4b85-a437-69c42e4a5a7d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;VoucherNo\&quot;:\&quot;\&quot;,\&quot;WTVoucherID\&quot;:${GlobalVariable.WTVoucherID},\&quot;TransactionTypeID\&quot;:${GlobalVariable.TransactionTypeID},\&quot;PaymentMethodID\&quot;:${GlobalVariable.PaymentMethodID},\&quot;CompanyID\&quot;:1,\&quot;AABBankAccountID\&quot;:${GlobalVariable.AABBankAccountID},\&quot;AABBankAccountNo\&quot;:\&quot;${GlobalVariable.AABBankAccountNo}\&quot;,\&quot;StatementAccountDate\&quot;:\&quot;\&quot;,\&quot;StatementAccountRemarks\&quot;:\&quot;\&quot;,\&quot;Remarks\&quot;:\&quot;${GlobalVariable.Remarks}\&quot;,\&quot;PrintDetail\&quot;:false,\&quot;PayerID\&quot;:${GlobalVariable.PayerID},\&quot;PayerAccountID\&quot;:0,\&quot;PayerAccountNo\&quot;:\&quot;\&quot;,\&quot;PayerAccountHolder\&quot;:\&quot;\&quot;,\&quot;PayerAccountCurrencyCode\&quot;:\&quot;\&quot;,\&quot;SecondaryBankID\&quot;:0,\&quot;SecondaryBankBranch\&quot;:\&quot;\&quot;,\&quot;SecondarySwiftCode\&quot;:\&quot;\&quot;,\&quot;SecondaryAccountNo\&quot;:\&quot;\&quot;,\&quot;SecondaryAccountName\&quot;:\&quot;\&quot;,\&quot;SecondaryBankAddress\&quot;:\&quot;\&quot;,\&quot;SecondaryIBAN\&quot;:\&quot;\&quot;,\&quot;SecondaryBankName\&quot;:\&quot;\&quot;,\&quot;VoucherTypeCode\&quot;:\&quot;${GlobalVariable.VoucherTypeCode}\&quot;,\&quot;InternalDistributionCode\&quot;:\&quot;JKT000\&quot;,\&quot;IsContainCOMNotes\&quot;:false}\n&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/VoucherAPI/SubmitVoucher</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
