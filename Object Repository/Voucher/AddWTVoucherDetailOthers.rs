<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>AddWTVoucherDetailOthers</name>
   <tag></tag>
   <elementGuidId>e3266881-893c-4777-8fa3-b25d083f9707</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;WTVoucherDetailID\&quot;:0,\&quot;WTVoucherID\&quot;:${GlobalVariable.WTVoucherID},\&quot;CurrencyCode\&quot;:\&quot;IDR\&quot;,\&quot;JournalTypeCode\&quot;:\&quot;${JournalTypeCode}\&quot;,\&quot;SubJournalTypeCode\&quot;:\&quot;${SubJournalTypeCode}\&quot;,\&quot;SuspendResultCategoryCode\&quot;:\&quot;${SuspendResultCategoryCode}\&quot;,\&quot;TaxTypeCode\&quot;:\&quot;\&quot;,\&quot;Remark\&quot;:\&quot;Remark suspend automation\&quot;,\&quot;AABAccountID\&quot;:69,\&quot;AABAccountNo\&quot;:\&quot;0701139003\&quot;,\&quot;Amount\&quot;:\&quot;${Amount}\&quot;,\&quot;ReferenceFinanceNoteID\&quot;:${GlobalVariable.ReferenceFinanceNoteID},\&quot;ReferenceNoteNumber\&quot;:\&quot;${GlobalVariable.ReferenceNoteNumber}\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/VoucherAPI/AddWTVoucherDetailOthers</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
