<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>SearchAndAddNote</name>
   <tag></tag>
   <elementGuidId>ad3a7cea-ed55-4902-9d98-cde06b187c05</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;WTVoucherID\&quot;:${GlobalVariable.WTVoucherID},\&quot;PayerName\&quot;:\&quot;\&quot;,\&quot;Currency\&quot;:\&quot;\&quot;,\&quot;JournalType\&quot;:\&quot;\&quot;,\&quot;SubJournalType\&quot;:\&quot;\&quot;,\&quot;Directorate\&quot;:\&quot;\&quot;,\&quot;IsExistInUnsettledVoucher\&quot;:false,\&quot;ReferenceType\&quot;:\&quot;NoteNo\&quot;,\&quot;ReferenceTypeValue\&quot;:[\&quot;${NoteNumber}\&quot;],\&quot;ProductionDateFrom\&quot;:\&quot;\&quot;,\&quot;ProductionDateTo\&quot;:\&quot;\&quot;,\&quot;OnProcessVoucherNo\&quot;:\&quot;\&quot;,\&quot;Company\&quot;:1,\&quot;TransactionType\&quot;:\&quot;${TransactionTypeID}\&quot;,\&quot;VoucherNoAddNote\&quot;:\&quot;\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI//VoucherAPI/SearchAndAddNote</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
