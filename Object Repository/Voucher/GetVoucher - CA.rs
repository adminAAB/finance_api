<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GetVoucher - CA</name>
   <tag></tag>
   <elementGuidId>071df96e-0452-4f78-9c04-f66542092349</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;PageNo\&quot;:1,\&quot;PageSize\&quot;:10,\&quot;SortBy\&quot;:\&quot;\&quot;,\&quot;SortDir\&quot;:\&quot;\&quot;,\&quot;VoucherNo\&quot;:\&quot;${GlobalVariable.VoucherNoCA}\&quot;,\&quot;PolicyNo\&quot;:\&quot;\&quot;,\&quot;NoteNo\&quot;:\&quot;\&quot;,\&quot;ClaimNo\&quot;:\&quot;\&quot;,\&quot;GroupingNo\&quot;:\&quot;\&quot;,\&quot;PaymentInstrumentNo\&quot;:\&quot;\&quot;,\&quot;AABAccount\&quot;:0,\&quot;PayerName\&quot;:\&quot;\&quot;,\&quot;VoucherType\&quot;:0,\&quot;VoucherStatus\&quot;:0,\&quot;RequestDateFrom\&quot;:\&quot;\&quot;,\&quot;RequestDateTo\&quot;:\&quot;\&quot;,\&quot;EntryUser\&quot;:\&quot;\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/VoucherAPI/GetVoucher</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
