<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GetVoucher</name>
   <tag></tag>
   <elementGuidId>f7ae93ff-9323-4f15-a622-ddcb55c972a9</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;PageNo\&quot;:${GlobalVariable.PageNo},\&quot;PageSize\&quot;:10,\&quot;SortBy\&quot;:\&quot;\&quot;,\&quot;SortDir\&quot;:\&quot;\&quot;,\&quot;VoucherNo\&quot;:\&quot;${GlobalVariable.VoucherNo}\&quot;,\&quot;PolicyNo\&quot;:\&quot;${GlobalVariable.PolicyNo}\&quot;,\&quot;NoteNo\&quot;:\&quot;${GlobalVariable.NoteNo}\&quot;,\&quot;ClaimNo\&quot;:\&quot;${GlobalVariable.ClaimNo}\&quot;,\&quot;GroupingNo\&quot;:\&quot;${GlobalVariable.GroupingNo}\&quot;,\&quot;PaymentInstrumentNo\&quot;:\&quot;${GlobalVariable.PINo}\&quot;,\&quot;AABAccount\&quot;:${GlobalVariable.AABAccount},\&quot;PayerName\&quot;:\&quot;${GlobalVariable.PayerId}\&quot;,\&quot;VoucherType\&quot;:${GlobalVariable.VoucherType},\&quot;VoucherStatus\&quot;:${GlobalVariable.VoucherStatus},\&quot;RequestDateFrom\&quot;:\&quot;${GlobalVariable.RequestDateFrom}\&quot;,\&quot;RequestDateTo\&quot;:\&quot;${GlobalVariable.RequestDateTo}\&quot;,\&quot;EntryUser\&quot;:\&quot;${GlobalVariable.EntryUser}\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/VoucherAPI/GetVoucher</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
