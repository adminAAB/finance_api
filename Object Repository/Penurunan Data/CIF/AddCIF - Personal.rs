<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>AddCIF - Personal</name>
   <tag></tag>
   <elementGuidId>4eac1a15-5c73-41f5-97dc-67cc2f0735f7</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;ObjectParam\&quot;:\&quot;{\\\&quot;ID\\\&quot;:625,\\\&quot;CustomerCode\\\&quot;:\\\&quot;${CustomerCode}\\\&quot;,\\\&quot;CustomerName\\\&quot;:\\\&quot;${CustomerName}\\\&quot;,\\\&quot;CustomerGroupCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CustomerTypeCode\\\&quot;:\\\&quot;${CustomerTypeCode}\\\&quot;,\\\&quot;CustomerStatusTypeCode\\\&quot;:\\\&quot;${CustomerStatusTypeCode}\\\&quot;,\\\&quot;CustomerActivationStatusTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CustomerCategoryCode\\\&quot;:\\\&quot;${CustomerCategoryCode}\\\&quot;,\\\&quot;EmailAddress\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;IDCardTypeCode\\\&quot;:\\\&quot;${IDCardTypeCode}\\\&quot;,\\\&quot;IDCardNumber\\\&quot;:\\\&quot;${IDCardNumber}\\\&quot;,\\\&quot;NameOnNPWP\\\&quot;:\\\&quot;${NameOnNPWP}\\\&quot;,\\\&quot;NPWPNumber\\\&quot;:\\\&quot;${NPWPNumber}\\\&quot;,\\\&quot;NPWPAddress\\\&quot;:\\\&quot;${NPWPAddress}\\\&quot;,\\\&quot;IsPKP\\\&quot;:\\\&quot;${IsPKP}\\\&quot;,\\\&quot;PKPNumber\\\&quot;:\\\&quot;${PKPNumber}\\\&quot;,\\\&quot;PKPDate\\\&quot;:\\\&quot;1900-01-01T00:00:00\\\&quot;,\\\&quot;CustomerUsageTypeCode\\\&quot;:[\\\&quot;${CustomerUsageTypeCode}\\\&quot;],\\\&quot;CustomerAddress\\\&quot;:[{\\\&quot;AddressTypeCode\\\&quot;:\\\&quot;${AddressTypeCode}\\\&quot;,\\\&quot;Address\\\&quot;:\\\&quot;${Address}\\\&quot;,\\\&quot;PostalCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;Phone1\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;Phone2\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;Fax1\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;Fax2\\\&quot;:\\\&quot;\\\&quot;}],\\\&quot;CustomerAccountTypeCode\\\&quot;:\\\&quot;${CustomerAccountTypeCode}\\\&quot;,\\\&quot;AccountNo\\\&quot;:\\\&quot;${AccountNo}\\\&quot;,\\\&quot;SwiftCode\\\&quot;:\\\&quot;${SwiftCode}\\\&quot;,\\\&quot;ClearingCode\\\&quot;:\\\&quot;${ClearingCode}\\\&quot;,\\\&quot;BankBranchDescription\\\&quot;:\\\&quot;${BankBranchDescription}\\\&quot;,\\\&quot;AccountHolder\\\&quot;:\\\&quot;${AccountHolder}\\\&quot;,\\\&quot;SortCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CorrespondenceBank\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;InternationalBankAccountNumber\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CurrencyCode\\\&quot;:\\\&quot;IDR\\\&quot;,\\\&quot;CreditCardTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;BirthDate\\\&quot;:\\\&quot;1900-01-01T00:00:00\\\&quot;,\\\&quot;GenderTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;EducationTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;MaritalStatusCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;ReligionTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;HomeStatusCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;NationalityCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;OccupationCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyName\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;JobTitleCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;OccupiedSince\\\&quot;:\\\&quot;0\\\&quot;,\\\&quot;JobAreaCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;SocialMediaInfo\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;LegalName\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;EstablishSince\\\&quot;:\\\&quot;0\\\&quot;,\\\&quot;CustomerPICName\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CustomerPICEmail\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CustomerPICFax\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CustomerPICHomePhone\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CustomerPICMobilePhone\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CustomerPICAddress\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CustomerPICTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyAssetTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyIndustrialTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyStatusTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyOwnershipTypeCode\\\&quot;:\\\&quot;NASTR\\\&quot;,\\\&quot;CompanyCustomerGroupTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyAnnualCoyRevenueTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyNoOfEmployeeTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyPsychographicsTypeCode\\\&quot;:\\\&quot;\\\&quot;,\\\&quot;CompanyProductMarketCode\\\&quot;:\\\&quot;\\\&quot;}\&quot;}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.Token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.endpoint}FinanceAPI/VoucherAPI/SubmitVoucher</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
